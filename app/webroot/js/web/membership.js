$(function(){
    $("#cc-form").submit(function(e){
        var stop = false;
        $(".text-danger").remove();
        if($(":radio[name='membership']:checked").length == 0){
            $("#membership-container").prepend("<h4 class='text-danger text-center'>You must choose an option</h4>");
            stop = true;
        }
        if($(":radio[name='membership']:checked").val()==$("#prev-mem").val()){
            $("#membership-container").prepend("<h4 class='text-danger text-center'>You already have this membership</h4>");
            stop = true;
        }
        if($("#card-name").val()==''){
            $("#card-name").after("<em class='text-danger text-center'>Insert a valid card name</em>");
            stop = true;
        }
        if($("#card-number").val()==''){
            $("#card-number").after("<em class='text-danger text-center'>Insert a valid card number</em>");
            stop = true;
        }
        if($("#card-cvv").val()==''){
            $("#card-cvv").after("<em class='text-danger text-center'>Insert a valid cvv</em>");
            stop = true;
        }
        item =  "Unity Zohar '"+$(":radio[name='membership']:checked").data("name")+"' membership";
        amount =  $(":radio[name='membership']:checked").data("price");
        $("#m_id").val($(":radio[name='membership']:checked").val());
        $("#concept").val(item);
        $("#amount").val(amount);
        if(stop){
            $("html, body").animate({ scrollTop: $("#membership-container").offset().top-20 }, "slow");
            e.preventDefault();
        }else{

            bootbox.confirm("Sure you want to buy this membership?", function(result) {
                if(result){
                    $("#modal-content, #modal-background").show();
                    var data_ = $("#cc-form").serialize();
                    $.ajax({
                        type:"POST",
                        dataType: 'json',
                        data : data_,
                        url:  Admin.basePath+"users/create_membership_agreement.json",
                        success: function(result) {
                            $("#modal-content, #modal-background").hide();
                            if(result.result.created === 0){
                                bootbox.dialog({
                                    message: result.result.error,
                                    title: "Payment failure",
                                    buttons: {
                                        main: {
                                            label: "Ok",
                                            className: "btn-primary"
                                        }
                                    }
                                });
                            }else{
                                bootbox.dialog({
                                    message: "Thank you, your payment has been processed successfully<br />A confirmation email has been sent",
                                    title: "Payment accepted",
                                    buttons: {
                                        main: {
                                            label: "Ok",
                                            className: "btn-primary",
                                            callback: function() {
                                                document.location.href = Admin.basePath+"users/appaccount"
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        }
        return false;
    });

    $("#cancel-membership").on("click",function(e){
        e.preventDefault();
        href = $(this).attr("href");
        bootbox.confirm("Sure you want to cancel your membership?", function(result) {
            if(result){
                $("#label-message").html("Your membership is being cancel");
                $("#modal-content, #modal-background").show();
                document.location.href = href;
            }
        });
    });

    $(":radio[name='membership']").change(function(){
        var lvl = $(this).parent().contents()
            .filter(function() {
                return this.nodeType === 3; //Node.TEXT_NODE
            });
        $("#membership-card span.level em").html($.trim(lvl.text()));
    });
});
