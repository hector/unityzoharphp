var markers =[];
var map;
limit = limit || 200;
var start = 0;
var markerCluster;
function initialize() {
    var mapOptions = {
        zoom: 2,
        center: new google.maps.LatLng(20,1),
        minZoom: 2
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),mapOptions);
    markerCluster = new MarkerClusterer(map);
    markerCluster.setGridSize(30);
    getMarkers();

}

function getMarkers() {
    locs = [];
    $.ajax({
        url: Admin.basePath + "pages/get_visitors/zohar/"+limit+".json?start="+start,
        success: function (result) {
            if(result.visitors.length == 0){
                return false;
            }
            for (i = 0; i < result.visitors.length; i++) {
                loc = [
                    result.visitors[i].ZoharCycle.city,
                    result.visitors[i].ZoharCycle.lat,
                    result.visitors[i].ZoharCycle.lng
                ];
                locs.push(loc);
            }
            setMarkers(locs);
            if(Admin.params.action == "unity_readers") {
                start += 200;
                setTimeout(function(){
                    getMarkers();
                },1000);

            }
        }
    });
}
function setMarkers(locations) {
    for (var i = 0; i < locations.length; i++)
    {
        var beach = locations[i];
        var coords = new google.maps.LatLng(beach[1], beach[2]);
        var marker = new google.maps.Marker({
            position: coords,
            map: map,
            title: beach[0],
            zIndex: 1
        });
        markers.push(marker);
    }
   // var markerCluster = new MarkerClusterer(map, markers);
    //markerCluster.setGridSize(30);

    markerCluster.clearMarkers();
    markerCluster.redraw();

    markerCluster.addMarkers(markers);
}