$(function(){
    $("#meditation-button").on("click",function(){
        $("#meditation-text").slideToggle('fast',function(){
            if($("#meditation-text").is(":hidden")){
                $("#more-meditation").html("(+)");
            }else{
                $("#more-meditation").html("(-)");
            }
        });


    });

    if($.cookie('show-aramic') == undefined || $.cookie('show-aramic')=='off'){
        $.cookie('show-aramic', 'off', { expires: 30 , path: '/' });
        $(".aramaic-text").hide();
    }else if($.cookie('show-aramic')=='on'){
        $(".aramaic-text").show();
        $(".hebrew-text").hide();

        var text = $("#change-text").text();
        var new_text = $("#change-text").data("change-text");
        $("#change-text").text(new_text);
        $("#change-text").data("change-text",text);

    }

    if($.cookie('show-trans') == undefined || $.cookie('show-trans')=='off'){
        $.cookie('show-trans', 'off', { expires: 30 , path: '/' });
        $(".zohar-paragraphs .transliteration").hide();
    }else if($.cookie('show-trans')=='on'){
        var text = $("#transliteration-button").text();
        var new_text = $("#transliteration-button").data("change-text");
        $("#transliteration-button").text(new_text);
        $("#transliteration-button").data("change-text",text);

        if($.cookie('show-aramic')=='on'){
            $(".transliterated-text-aramaic").show();
            $(".transliterated-text").hide();
        }else{
            $(".transliterated-text-aramaic").hide();
            $(".transliterated-text").show();
        }
    }


    if($.cookie('size')){
        $(".zohar-paragraphs .hebrew-text, .zohar-paragraphs .hebrew-text2,.zohar-paragraphs .aramaic-text, .zohar-paragraphs .transliterated-text, .zohar-paragraphs .transliterated-text-aramaic ")
            .removeClass("size-14")
            .addClass("size-"+ $.cookie('size'));
    }

    $(".plus-btn").on("click",function(){
        var $content = $(".zohar-paragraphs .hebrew-text, .zohar-paragraphs .hebrew-text2, .zohar-paragraphs .aramaic-text, .zohar-paragraphs .transliterated-text, .zohar-paragraphs .transliterated-text-aramaic ");
        var clases = $content.attr('class').split(/\s+/);
        $.each(clases, function(index, item) {
            if(item.indexOf('size') >= 0){
                var sz = parseInt(item.split("-")[1]);
                if(sz >= 20) return false;
                sz++;
                $content.removeClass(item);
                $content.addClass("size-"+sz);
                $.cookie('size',sz);
                return false;
            }
        });
    });

    $(".minus-btn").on("click",function(){
        var $content = $(".zohar-paragraphs .hebrew-text,.zohar-paragraphs .hebrew-text2, .zohar-paragraphs .aramaic-text, .zohar-paragraphs .transliterated-text, .zohar-paragraphs .transliterated-text-aramaic");
        var clases = $content.attr('class').split(/\s+/);
        $.each(clases, function(index, item) {
            console.log(item);
            if(item.indexOf('size') >= 0){
                var sz = parseInt(item.split("-")[1]);
                if(sz <= 14) return false;
                sz--;
                $content.removeClass(item);
                $content.addClass("size-"+sz);
                $.cookie('size',sz);
                return false;
            }
        });
    });

    var delay = 7000;
    if(Admin.params.action =='unity_books'){
        var delay = 1;
    }
    $('[data-toggle="tooltip"]').tooltip();

    $(".next-zohar").attr("disabled","disabled");
    $("#change-text").on("click",function(e){
        e.preventDefault();
        if($.cookie('show-aramic')=='on'){
            $.cookie('show-aramic', 'off', { expires: 30 , path: '/' });
            $(".aramaic-text").hide();
            $(".hebrew-text").show();
            if($.cookie('show-trans')=='on'){
                $(".transliterated-text-aramaic").hide();
                $(".transliterated-text").show();
            }
        }else{
            $.cookie('show-aramic', 'on', { expires: 30, path: '/'  });
            $(".aramaic-text").show();
            $(".hebrew-text").hide();
            if($.cookie('show-trans')=='on'){
                $(".transliterated-text-aramaic").show();
                $(".transliterated-text").hide();
            }
        }
        var text = $(this).text();
        var new_text = $(this).data("change-text");
        $(this).text(new_text);
        $(this).data("change-text",text);
    });
    $("#transliteration-button").on("click",function(e){

        if($.cookie('show-trans')=='on'){
            $.cookie('show-trans', 'off', { expires: 30, path: '/'  });
            $(".zohar-paragraphs .transliteration").hide();
        }else{
            $.cookie('show-trans', 'on', { expires: 30 , path: '/' });
            if($.cookie('show-aramic')=='on'){
                $(".transliterated-text-aramaic").show();
                $(".transliterated-text").hide();
            }else{
                $(".transliterated-text-aramaic").hide();
                $(".transliterated-text").show();
            }

        }
        e.preventDefault();

        var text = $(this).text();
        var new_text = $(this).data("change-text");
        $(this).text(new_text);
        $(this).data("change-text",text);
    });
    setTimeout(function(){
        $(".next-zohar").removeAttr("disabled");
        $('[data-toggle="tooltip"]').tooltip('destroy');
    },delay);


    $("select#parasha").on("change",function(){
        document.location.href = Admin.basePath+"zohar-books/"+$(this).val();
    });


    if($("#paragraph-slider").length>0){
        var slider = new Slider("#paragraph-slider", {
            tooltip: 'always'
        }).on("slideStop",function(val){
                console.log(val.value);

                document.location.href = document.location.href.replace(/(\/\d*)?$/, "/" + (val.value - 1).toString());
            })
    }

});
