var interval;
$(function(){
    $(":radio.radio-donation:first").prop("checked",true);

    $('.panel-heading a').on('click',function(e){

        if($(this).parents('.panel').children('.panel-collapse').hasClass('in')){
            e.stopPropagation();
            e.preventDefault();
        }
        if($(this).parents('.panel-heading').attr("id")!="headingOne"){
            $("#free-panel,#no-free-panel-title").hide();
            $(".not-free").removeClass("hide");
        }else{
            $(".not-free").addClass("hide");
            $("#free-panel,#no-free-panel-title").show();
            $(".not-free").find(":radio").removeAttr("checked");
            $(".not-free + label").find(":radio").attr("checked","checked");
            $(".not-free + label").find(":radio").prop('checked', true);
        }
    });
    $('#accordion').find(".panel-collapse:not(.in) input,button").attr("disabled","disabled");

    $('#accordion').on('shown.bs.collapse', function (e) {
        $(e.target).find("input,button").removeAttr("disabled");
    });
    $('#accordion').on('hidden.bs.collapse', function (e) {
        $(e.target).find("input,button").attr("disabled","disabled");
    });

    $("#cc-form").submit(function(e){
        e.preventDefault();
        $(".text-danger").remove();
        stop = false;
        pass = true;
        if($.trim($("input.first-name:not(:disabled)").val())==""){
            $("input.first-name:not(:disabled)").after("<em class='text-danger'>This field cannot be empty<br /></em>");
            pass = false;
        }
        if($.trim($("input.parent-name:not(:disabled)").val())==""){
            $("input.parent-name:not(:disabled)").after("<em class='text-danger'>This field cannot be empty<br /></em>");
            pass = false;
        }

        if(!pass){
            $("html, body").animate({ scrollTop: $("#bio-donation-container").offset().top-20 }, "slow");
            return;
        }


        if($("#card-name").val()==''){
            $("#card-name").after("<em class='text-danger text-center'>Insert a valid card name</em>");
            stop = true;
        }
        if($("#card-number").val()==''){
            $("#card-number").after("<em class='text-danger text-center'>Insert a valid card number</em>");
            stop = true;
        }
        if($("#card-cvv").val()==''){
            $("#card-cvv").after("<em class='text-danger text-center'>Insert a valid cvv</em>");
            stop = true;
        }
        item =  $(":radio.radio-donation:checked").parent().html().replace(/(<([^>]+)>)/ig,"").replace(/(\r\n|\n|\r)/gm,"").replace(/\s+/g," ");
        amount =  $(":radio.radio-donation:checked").data("price");
        $("#concept").val(item);
        $("#amount").val(amount);
        if(stop){
            $("html, body").animate({ scrollTop: $("#payment-data").offset().top-20 }, "slow");
            return;
        }else{
                    $("#modal-content, #modal-background").show();
                    data_ = $("#cc-form").serialize();
                    //console.log(data_);
                    $.ajax({
                        type:"POST",
                        dataType: 'json',
                        data : data_,
                        url:  Admin.basePath+"candles/save_web/",
                        success: function(result) {
                            $("#modal-content, #modal-background").hide();
                            if(result.saved === 0){
                                bootbox.dialog({
                                    message: result.error,
                                    title: "Payment failure",
                                    buttons: {
                                        main: {
                                            label: "Ok",
                                            className: "btn-primary"
                                        }
                                    }
                                });
                            }else{
                                bootbox.dialog({
                                    message: "Thank you, your payment has been processed successfully and your candle has been created<br />A confirmation email has been sent",
                                    title: "Candle Created",
                                    buttons: {
                                        main: {
                                            label: "Ok",
                                            className: "btn-primary",
                                            callback: function() {
                                                document.location.href = Admin.basePath+"candles";
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
        }
        return false;
    });


    $("#free-candle").on("click",function(e){
        e.preventDefault();
        $("#cc-form").find(".text-danger").remove();
        var pass = true;
        if($.trim($("input.first-name:not(:disabled)").val())==""){
            $("input.first-name:not(:disabled)").after("<em class='text-danger'>This field cannot be empty<br /></em>");
            pass = false;
        }
        if($.trim($("input.parent-name:not(:disabled)").val())==""){
            $("input.parent-name:not(:disabled)").after("<em class='text-danger'>This field cannot be empty<br /></em>");
            pass = false;
        }

        if(!pass){
            $("html, body").animate({ scrollTop: $("#bio-donation-container").offset().top-20 }, "slow");
            return;
        }
        if(!pass){
            ;
        }else{
            $("#modal-content, #modal-background").show();
            data_ = $("#cc-form").serialize();
            $.ajax({
                type:"POST",
                dataType: 'json',
                data : data_,
                url:  Admin.basePath+"candles/free_candle/",
                success: function(result) {
                    $("#modal-content, #modal-background").hide();
                    if(result.saved === 0){
                        bootbox.dialog({
                            message: result.error,
                            title: "Candle failure",
                            buttons: {
                                main: {
                                    label: "Ok",
                                    className: "btn-primary"
                                }
                            }
                        });
                    }else{
                        bootbox.dialog({
                            message: "Your free candle has been created",
                            title: "Candle Created",
                            buttons: {
                                main: {
                                    label: "Ok",
                                    className: "btn-primary",
                                    callback: function() {
                                        document.location.href = Admin.basePath+"candles";
                                    }
                                }
                            }
                        });
                    }
                }
            });
        }
    });
 });

