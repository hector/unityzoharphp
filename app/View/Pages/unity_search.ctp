<?php echo $this->element("web-header");?>
    <div id="unity-container">
        <div class="container">
            <h2 class="text-center">Search by: <em><?php echo @$this->data['search'] ?></em></h2>
        </div>
    </div>
    <div class="main-content clearfix">
        <div class="container top15 bottom15">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?php echo $this->Form->create(null, [
                        'url' => Router::url("/zohar-books/search",true),
                        'type' => 'get'
                    ]); ?>
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" value="<?php echo @$this->data['search'] ?>" placeholder="Search..."/>
                            <span class="input-group-btn">
                                <button type='submit' class="btn btn-flat"><i class="fa fa-search"></i></button>
                            </span>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
            <div class="row top15 bottom15">
                <div class="col-md-8 col-md-offset-2 text-left">
                    <div class="btn-group" role="group">
                        <a href="#" id="change-text" class="pull-left btn btn-unity" data-change-text="<?php echo __("Show Hebrew text") ?>"><?php echo __("Show Aramaic text") ?></a>
                        &nbsp;<a href="#" id="transliteration-button" class="pull-left btn btn-default" data-change-text="<?php echo __("Hide Transliteration") ?>"><?php echo __("Show Transliteration") ?></a>
                    </div>
                </div>

                <div class="col-md-3 text-right">
                </div>
            </div>
            <?php foreach ($zohars as $key =>$zohar) {?>
                <div class="row zohar-paragraphs">
                    <div class="col-md-8 col-md-offset-2  zohar-text">
                        <p style="direction: ltr">
                            <?php if(is_file(WWW_ROOT."audio".DS.$zohar["Parasha"]["audio_name"]."-".sprintf("%03d", $zohar['Zohar']["paragraph_num"]).".mp3")){?>
                                <a class="audio {mp3:'<?php echo Router::url("/audio/",true).$zohar["Parasha"]["audio_name"]."-".sprintf("%03d", $zohar['Zohar']["paragraph_num"]).".mp3" ?>'}" href="<?php echo Router::url("/audio/",true).$zohar["Parasha"]["audio_name"]."-".sprintf("%03d", $zohar['Zohar']["paragraph_num"]).".mp3" ?>"><?php echo $zohar['Zohar']["paragraph_num"] ?></a>
                            <?php } ?>
                        </p>
                        <p class="hebrew-text">
                            <span class="text-primary" data-id="<?php echo $zohar['Zohar']["id"] ?>">
                                [
                                <?php echo $zohar['Parasha']["name"]?>
                                <?php echo /*$zohar['Zohar']["id"].":".*/$zohar['Zohar']["paragraph_num"]?>

                                <?php  if(!empty($zohar["Translation"]) || !empty($zohar['Zohar']["notes"])){?>
                                    <a href="#" class="btn-translation" data-id="<?php echo $zohar['Zohar']["id"] ?>"><i class="fa fa-plus-circle"></i></a>
                                <?php }?>
                                ]
                            </span>
                            <?php echo $zohar['Zohar']["text_hebrew"] ?>
                        </p>
                        <p class="aramaic-text">
                            <span class="text-primary" data-id="<?php echo $zohar['Zohar']["id"] ?>">
                                [

                                <?php echo $zohar['Parasha']["name"]?>
                                <?php echo /*$zohar['Zohar']["id"].":".*/$zohar['Zohar']["paragraph_num"]?>

                                <?php  if(!empty($zohar["Translation"]) || !empty($zohar['Zohar']["notes"])){?>
                                    <a href="#" class="btn-translation" data-id="<?php echo $zohar['Zohar']["id"] ?>"><i class="fa fa-plus-circle"></i></a>
                                <?php }?>
                                ]</span>
                            <?php echo $zohar['Zohar']["text_aramaic"] ?>
                        </p>
                        <p class="transliterated-text transliteration">
                            <?php echo $zohar['Zohar']["transliterated_hebrew"] ?>
                        </p>
                        <p class="transliterated-text-aramaic transliteration">
                            <?php echo $zohar['Zohar']["transliterated_aramaic"] ?>
                        </p>
                    </div>
                </div>
                <?php
                if(!empty($zohar["Translation"]) || !empty($zohar['Zohar']["notes"]))
                {?>
                    <div class="col-md-8 col-md-offset-2">
                        <div class="translatios-tabs hide well well-sm"  data-id="<?php echo $zohar['Zohar']["id"] ?>">
                            <h4 class="text-center">Translations &amp; Notes</h4>
                            <ul class="nav nav-tabs" role="tablist">
                                <?php foreach ($zohar["Translation"] as $translation) {?>
                                    <li role="presentation">
                                        <a href="#lang_<?php echo $translation["id"]?>"  role="tab" data-toggle="tab"><?php echo $languages[$translation["language_id"]] ?></a>
                                    </li>
                                <?php }?>
                                <?php if(!empty($zohar['Zohar']["notes"])){?>
                                    <li role="presentation">
                                        <a href="#notes_<?php echo $zohar["Zohar"]["id"]?>" role="tab" data-toggle="tab"><?php echo __("Notes") ?></a>
                                    </li>
                                <?php }?>
                                <?php /*
                            <li role="comments">
                                <a href="#comments_<?php echo $zohar["Zohar"]["id"]?>" role="tab" data-toggle="tab"
                                   onclick="loadDisqus(jQuery(this), <?php echo$zohar['Zohar']["id"] ?>, '<?php echo Router::url("/zohar-books/",true)."?zohar=".$zohar['Zohar']["id"]; ?>')">
                                    <?php echo __("Comments") ?>
                                </a>
                            </li>
                            */?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($zohar["Translation"] as $translation) {?>
                                    <div role="tabpanel" class="tab-pane" id="lang_<?php echo $translation["id"]?>">
                                        <div class="padded">
                                            <?php echo  $translation["text"]?>
                                        </div>
                                        <div class="padded">
                                            <?php echo  $translation["notes"]?>
                                        </div>
                                    </div>
                                <?php }?>
                                <?php if(!empty($zohar['Zohar']["notes"])){?>
                                    <div role="tabpanel" class="tab-pane" id="notes_<?php echo $zohar["Zohar"]["id"]?>">
                                        <div class="padded zohar-notes">
                                            <?php echo $zohar['Zohar']["notes"]?>
                                        </div>
                                    </div>
                                <?php }?>
                                <!--                                <div role="tabpanel" class="tab-pane" id="comments_--><?php //echo $zohar["Zohar"]["id"]?><!--">-->
                                <!--                                    <div class="padded">-->
                                <!--                                        <div id="comment---><?php //echo $zohar["Zohar"]["id"]?><!--">-->
                                <!---->
                                <!--                                        </div>-->
                                <!--                                    </div>-->
                                <!--                                </div>-->
                            </div>
                        </div>
                    </div>
                <?php  }?>
            <?php  }?>
            <div class="row top30">
                <div class="col-lg-12">
                    <?php echo $this->element("pray",[],['cache'=>['config'=>'day']]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="pagination center-block pagination-large text-center">
                        <ul class="pagination">
                            <?php
                            echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                            echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                            echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                            ?>
                        </ul>
                    </div>
                    <div class="center-block text-center">
                        <p class="muted"> <?php echo $this->Paginator->counter(
                                __('Page').' {:page}/{:pages}, '.__('Records').' {:current}/{:count}, '.__('from').' {:start} '.__('To').' {:end}');?>
                        </p>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>
<script>
    $(function(){
        $(".btn-translation").on("click",function(e){
            e.preventDefault();
            id = $(this).data("id");
            $(".btn-translation i").removeClass("fa-minus-circle").addClass("fa-plus-circle");
            $(this).find("i").toggleClass("fa-minus-circle");
            $(".translatios-tabs[data-id='"+id+"']").toggleClass("hide");
            $(".translatios-tabs").not("[data-id='"+id+"']").addClass("hide");
            if(!$(".translatios-tabs[data-id='"+id+"']").is(":visible")){
                $(this).find("i").toggleClass("fa-minus-circle");
            }

        });
        $('.nav-tabs').each(function(){
            $(this).find("a:first").tab('show');
        });

        $(".audio").mb_miniPlayer({
            width:300,
            id3:false,
            addShadow:false,
            pauseOnWindowBlur: false,
            downloadPage:null,
            playAlone:false,
            inLine:true,
            skin:'gray',
            onEnd: playNext,
            onPlay:stopOthers,
            onMute:function(player){
                setTimeout(function(){
                    jQuery("[isPlaying='true']").not(".jp-state-playing").find(".map_play").trigger(jQuery.mbMiniPlayer.eventEnd);
                },250);

            }
        });
        function playNext(player) {
            var players = $(".audio");
            document.playerIDX = (player.idx <= players.length - 1 ? player.idx : 10000);
            var $newplayer = $(players).eq(document.playerIDX);
            var ofs = $("#"+$newplayer.attr("id")).siblings('.mbMiniPlayer').offset().top;
            $('html, body').animate({
                scrollTop: ofs
            }, 500);

            console.log();
            $newplayer.mb_miniPlayer_play();
        }
        function stopOthers(player) {
            $(player).jPlayer("pauseOthers",0);
        }
    });
</script>
<?php echo $this->element("web-pre-footer")?>
<?php
echo $this->Html->script(['web/bootstrap-slider','audio/jquery.mb.miniPlayer.min.js?a=123'], array('inline' => false));
echo $this->Html->css(['web/bootstrap-slider','web/miniplayer'], array('inline' => false));
?>