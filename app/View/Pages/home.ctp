<?php echo $this->element("web-header")?>
    <div id="unity-container">
        <div class="container">
            <h2 class="text-center">Unity Zohar</h2>
        </div>
    </div>
    <div class="main-content clearfix">
        <div class="container top15 bottom15">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $this->Session->flash("middle"); ?>
                </div>
            </div>
        </div>
        <div class="container top15 bottom15">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $text["UnityText"]["text"]; ?>
                </div>
            </div>
            <div class="row tiles-container top15">
                <div class="col-md-3">
                    <div class="tile tile-1">
                        <h2 class="text-uppercase text-center">Unity</h2>
                    </div>
                    <div class="tile-footer">
                        <h3 class="text-center text-uppercase"><a href="<?php echo Router::url("/",true)?>unity-zohar"">Unity Zohar +</a></h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="tile tile-2">
                        <h2 class="text-uppercase text-center">Zohar</h2>
                    </div>
                    <div class="tile-footer">
                        <h3 class="text-center text-uppercase"><a href="<?php echo Router::url("/",true)?>unity-pinchas"">Unity Pinchas +</a></h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="tile tile-3">
                        <h2 class="text-uppercase text-center">Global</h2>
                    </div>
                    <div class="tile-footer">
                        <h3 class="text-center text-uppercase"><a href="<?php echo Router::url("/unity-tikunim",true)?>">Unity Tikunim +</a></h3>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="tile tile-3">
                        <h2 class="text-uppercase text-center">Project</h2>
                    </div>
                    <div class="tile-footer">
                        <h3 class="text-center text-uppercase"><a href="<?php echo Router::url("/unity-psalms",true)?>">Unity Psalms +</a></h3>
                    </div>
                </div>
            </div>
            <div class="row top30">
                <div class="col-md-8">
                    <div class="bible-container">
                        <h3 class="membership">ZOHAR by books Study Membership</h3>
                        <div class="membership-content">
                            <h4 class="text-center">Get full access to the study pages.</h4>
                            <h5 class="text-center bottom17">
                                The entire  Zohar | Search | Comments | Translations
                            </h5>
                        </div>
                        <div>
                        <a href="<?php echo Router::url("/",true)?>zohar-books" class="enter">Enter</a>
<!--                            --><?php //if(!AuthComponent::user('id')){?>
<!--                                <a href="--><?php //echo Router::url("/",true)?><!--users/membership" class="enter">Enter</a>-->
<!--                            --><?php //}elseif(AuthComponent::user('id') && AuthComponent::user('membership_id')=='0'){ ?>
<!--                                <a href="--><?php //echo Router::url("/",true)?><!--users/membership" class="enter">Enter</a>-->
<!--                            --><?php //}elseif(AuthComponent::user('id') && AuthComponent::user('membership_id')!='0'){?>
<!--                                <a href="--><?php //echo Router::url("/",true)?><!--zohar-books" class="enter">Membership</a>-->
<!--                            --><?php //}else{?>
<!--                                <a href="--><?php //echo Router::url("/",true)?><!--users/membership" class="enter">Enter</a>-->
<!--                            --><?php //} ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-4" style="line-height: 24px">
                    <p class="text-justify">
                    For the first time since its revelation over 700 years ago, the complete sacred text of the Holy Zohar is available for all human beings to connect to-in unity.
                    </p>
                    <p class="text-justify">
                    UnityZohar.com was created as a free soul connection tool-for you, for us. We live on the same planet, breath the same air, and drink the same water. No matter how you look at it, we are one. We can achieve the Ultimate Global Unity by going above all physical limitations and join in one consciousness with all the people around the world that are already part of the Unity Zohar Project.
                    </p>
                    <p class="text-justify">
                    You are not joining a club or a teacher or organization. There are no gathering places to meet and no fees to pay It is all about ONE consciousness of love and unity. It is a spiritual place that connects your consciousness in unity with the rest of the world using the light of the Zohar.
                    </p>
                </div>
            </div>
            <div class="row top15 bottom15">
                <h3 class="text-center">Most recent Unity Zohar readers</h3>
                <a href="<?php echo Router::url("/zohar-readers",true) ?>">
                <div id="map-canvas" style="background-image: url(<?php echo Router::url("/images/map.jpg",true) ?>);cursor: pointer">
                </div>
                </a>
                <div class="col-md-12 top10 bottom10 text-center">
                    <a href="<?php echo Router::url("/zohar-readers",true) ?>">View all Zohar readers</a>
                </div>
<!--                <div class="col-md-6">-->
<!--                    <p class="bg-info btn-lg">-->
<!--                        Zohar's time lecture: --><?php //echo $zohar_time ?>
<!--                    </p>-->
<!--                </div>-->
<!--                <div class="col-md-6 text-left">-->
<!--                    <p class="text-right bg-info btn-lg">-->
<!--                    Pinchas' time lecture: </label>--><?php //echo $pinchas_time ?>
<!--                    </p>-->
<!--                </div>-->

            </div>
        </div>
<?php echo $this->element("web-pre-footer")?>
<script>
        var limit = 150;
</script>
<?php //echo $this->Html->script('web/marked', array('inline' => true));?>
<?php //echo $this->Html->script('web/maps', array('inline' => true));?>
<script>

    $("#map-canvas").on("click",function(){
        //document.location =
    });
/*
    function loadScript(src,callback){

        var script = document.createElement("script");
        script.type = "text/javascript";
        if(callback)script.onload=callback;
        document.getElementsByTagName("head")[0].appendChild(script);
        script.src = src;
    }
    loadScript('https://maps.googleapis.com/maps/api/js?v=3&callback=initialize&key=AIzaSyByeQ_1J8q58Ud23fDcRnadC-XrORLNAWQ',
        function(){});*/
</script>
<?php //echo $this->Html->script('https://google-maps-utility-library-v3.googlecode.com/svn/tags/markerclusterer/1.0/src/markerclusterer.js', array('inline' => true));?>

<?php //echo $this->Html->script('web/marked', array('inline' => true));?>
