<?php echo $this->element("web-header")?>
<div id="unity-container">
    <div class="container">
        <h2 class="text-center page-title">How does it work</h2>
    </div>
</div>
<div class="container-fluid" style="background: #FFF" >
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-push-2 text-justify top30 bottom30" >
                <?php echo $text ?>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element("web-pre-footer")?>
