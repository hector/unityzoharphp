<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="page-header">API
        <small>
            <i class="fa fa-angle-double-right"></i>
            Methods
        </small></h1>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-lg-12">
            <h3>Available formats</h3>
            <div class="code-highlight">
                <pre>
                    <code class="html">
                        The API allows you to obtain the required data in two different formats.
                        <em>JSON</em> | <em>XML</em>
                    </code>
                </pre>
            </div>
            <h3>Get Zohar Books</h3>
            <div class="code-highlight">
                <pre>
                    <code class="html">
                        <?php echo Router::url("/",true)."api/parashot/get.FORMAT" ?><br />
                        <?php echo Router::url("/",true)."api/parashot/get/id.FORMAT" ?><br />
                        <?php echo Router::url("/",true)."api/parashot/get/slug.FORMAT" ?><br />
                    </code>
                </pre>
            </div>
            <h3>Get Zohar text</h3>
            <div class="code-highlight">
                <pre>
                    <code class="html">
                        <?php echo Router::url("/",true)."api/zohar/get.FORMAT" ?><br />
                        Accepted Params:
                        <em>parasha_id</em> : Parasha book id
                        <em>paragraph</em> : Number of the required paragraph
                        <em>paragraph_from</em> : Number of paragraph from which results would be shown
                        <em>limit</em> : results limit | all results have a max of 50 records

                    </code>
                </pre>
            </div>



            <h3>Get Bible Books</h3>
            <div class="code-highlight">
                <pre>
                    <code class="html">
                        <?php echo Router::url("/",true)."api/bible_books/get.FORMAT" ?><br />
                        <?php echo Router::url("/",true)."api/bible_books/get/id.FORMAT" ?><br />
                        <?php echo Router::url("/",true)."api/bible_books/get/slug.FORMAT" ?><br />
                    </code>
                </pre>
            </div>

            <h3>Get Bible text</h3>
            <div class="code-highlight">
                <pre>
                    <code class="html">
                        <?php echo Router::url("/",true)."api/bible/get.FORMAT" ?><br />
                        Accepted Params:
                        <em>book_id</em> : Bible book id
                        <em>chapter</em> : Number of the required chapter
                        <em>verse</em> : Number of the required Verse
                        <em>verse_from</em> : Number of verse from which results would be shown
                        <em>limit</em> : results limit | all results have a max of 50 records

                    </code>
                </pre>
            </div>


<!--            <h3>Access to API methods sample code</h3>-->
<!--            <div class="code-highlight">-->
<!--        <pre>-->
<!--            <code class="html">-->
<!--                &lt;?php-->
<!--                $secret = "SECRECT";-->
<!--                $appkey = "API_KEY";-->
<!--                $url = "--><?php //echo Router::url("/",true)?><!--api/date/converter.json?date=2014-08-06";-->
<!--                $time = time();-->
<!--                $signature = hash_hmac( 'sha256', $time,$secret);-->
<!--                $headr = array();-->
<!--                $headr[] = 'Content-length: 0';-->
<!--                $headr[] = 'Content-type: application/json';-->
<!--                $headr[] = 'Authorization: '.$signature;-->
<!--                $headr[] = 'apikey: '.$appkey;-->
<!--                $headr[] = 'timestamp: '.$time;-->
<!---->
<!--                $ch = curl_init($url);-->
<!--                curl_setopt($ch, CURLOPT_HTTPHEADER,$headr);-->
<!--                curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);-->
<!--                echo $ret = curl_exec($ch);-->
<!--                curl_close($ch);-->
<!--                ?&gt;-->
<!--            </code>-->
<!--        </pre>-->
            </div>

        </div>
        <!-- /.col-lg-12 -->
    </div>

</section><!-- /.content -->


