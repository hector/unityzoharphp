<?php echo $this->element("web-header")?>
    <div id="unity-container">
        <div class="container">
            <h2 class="text-center"><?php echo __("Unity Psalms")?></h2>
        </div>
    </div>
    <div class="main-content clearfix">
        <div class="container top15 bottom15">
            <div class="row">
                <div class="date-container top10">
                    <label class="date">%<span id="today-date"><?php echo $percent." | ".__("Current reading cycle: ").$cycle["PsalmsCycle"]["cycle"]." | Members online: ".$readers ?>
                </span>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?php echo $this->element("meditation",[],['cache'=>['config'=>'day']]); ?>
                </div>
            </div>
            <div class="row top30">
                <div class="col-md-5 col-sm-12 col-md-offset-2">

                </div>
                <div class="col-md-3 col-sm-12 text-right">
                    <div class="btn-group font-sizes" role="group">
                        <button data-role="none" type="button" class="btn btn-default minus-btn"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
                        <span  data-role="none" class="btn btn-default no-btn"><i class="fa fa-font" aria-hidden="true"></i></span>
                        <button data-role="none" type="button" class="btn btn-default plus-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            <div class="row top10">
                <div class="col-md-5 col-xs-7 col-md-offset-2 col-sm-6 text-left bottom15">
                    <div class="btn-group" role="group">
<!--                        <a href="#" id="change-text" class="pull-left btn btn-unity" data-change-text="--><?php //echo __("Show Hebrew text") ?><!--">--><?php //echo __("Show Aramaic text") ?><!--</a>-->
<!--                        &nbsp;-->
                        <a href="#" id="transliteration-button2" class="pull-left btn btn-default" data-change-text="<?php echo __("Hide Transliteration") ?>"><?php echo __("Show Transliteration") ?></a>
                    </div>
                    </div>
                <div class="col-md-3 col-sm-6 col-xs-5 text-right bottom15">
                    <h4><strong><?php echo $book["BibleBook"]["name"] ?> | <span class="zohar-text-no-bg"><?php echo $book["BibleBook"]["name_hebrew"]  ?></span></strong></h4>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <?php foreach ($zohars as $key =>$zohar) {?>
                <div class="row zohar-paragraphs">
                    <div class="col-md-8 col-md-offset-2  zohar-text">
                        <p style="direction: ltr">
<!--                            --><?php //if(is_file(WWW_ROOT."audio".DS.$zohar["BibleBook"]["audio_name"]."-".sprintf("%03d", $zohar['Bible']["verse_num"]).".mp3")){?>
<!--                                <a class="audio {mp3:'--><?php //echo Router::url("/audio/",true).$zohar["BibleBook"]["audio_name"]."-".sprintf("%03d", $zohar['Bible']["verse_num"]).".mp3" ?><!--'}" href="--><?php //echo Router::url("/audio/",true).$zohar["BibleBook"]["audio_name"]."-".sprintf("%03d", $zohar['Zohar']["paragraph_num"]).".mp3" ?><!--">--><?php //echo $zohar['Zohar']["paragraph_num"] ?><!--</a>-->
<!--                            --><?php //} ?>
                        </p>
                        <p class="hebrew-text2  size-14">
                            <span class="text-primary paragraph-nums" data-id="<?php echo $zohar['Bible']["id"] ?>">
                                <?php echo $zohar['Bible']["chapter"]." - ".$zohar['Bible']["verse_number"]?>
                            </span>
                            <?php echo $zohar['Bible']["verse_text_hebrew"] ?>
                        </p>
<!--                        <p class="aramaic-text">-->
<!--                            <span class="text-primary paragraph-nums" data-id="--><?php //echo $zohar['Bible']["id"] ?><!--">-->
<!--                               --><?php //echo $zohar['Bible']["paragraph_num"]?>
<!--                            </span>-->
<!--                            --><?php //echo $zohar['Bible']["text_aramaic"] ?>
<!--                        </p>-->
                        <p class="transliterated-text transliteration  size-14">
                            <?php echo $zohar['Bible']["transliterated_hebrew"] ?>
                        </p>
<!--                        <p class="transliterated-text-aramaic transliteration">-->
<!--                            --><?php //echo $zohar['Bible']["transliterated_aramaic"] ?>
<!--                        </p>-->
                    </div>
                </div>
                <?php //aca iban las pestañas ?>
            <?php  }?>
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $this->element("pray",[],['cache'=>['config'=>'day']]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $this->element('candles_first',['candles'=>$first,'pre'=>true]/*['pre'=>true],[ "cache" => ['config'=>"minute15"]]*/); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-xs-7 col-md-offset-2 col-sm-6 bottom15">
                </div>
                <div class="col-md-3 col-sm-6 col-xs-5 text-right">
                    <div class="inline-block" data-toggle="tooltip" data-placement="left" title="Next Psalm is available after 7 seconds delay">
                        <a href="<?php echo $this->request->here ?>"  class=" btn btn-unity next-zohar"><?php echo __("Next Psalm") ?></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12 bottom15 top17">
                    <h3 class="text-center">
                        <a href="<?php echo Router::url("/candles",true) ?>" class="add-header text-uppercase">Add your name to the list</a>
                        <hr />
                    </h3>
                </div>
            </div>
            <?php echo $this->element('candles',['candles'=>$candles]/*['pre'=>true],[ "cache" => ['config'=>"minute15"]]*/); ?>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-center"><?php echo __("Most recent Psalms readers")?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $this->element("visitors_pinchas",[],[ "cache" => ['config'=>"minute5"]]);?>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $(".audio").mb_miniPlayer({
                width:300,
                id3:false,
                addShadow:false,
                pauseOnWindowBlur: false,
                downloadPage:null,
                inLine:true,
                playAlone:true,
                skin:'gray'
                // downloadPage:"map_download.php"
            });
        });
    </script>
<?php echo $this->element("web-pre-footer")?>
<?php
echo $this->Html->script(['audio/jquery.jplayer.min','audio/jquery.mb.miniPlayer.min'], array('inline' => false));
echo $this->Html->css(['web/miniplayer'], array('inline' => false));
?>

<script>
    $(function(){
        $("#transliteration-button2").on("click",function(e){

            if($.cookie('show-trans')=='on'){
                $.cookie('show-trans', 'off', { expires: 30, path: '/'  });
                $(".zohar-paragraphs .transliteration").hide();
            }else{
                $.cookie('show-trans', 'on', { expires: 30 , path: '/' });
                $(".transliterated-text").show();

            }
            e.preventDefault();

            var text = $(this).text();
            var new_text = $(this).data("change-text");
            $(this).text(new_text);
            $(this).data("change-text",text);
        });

        if($.cookie('show-trans')=='on'){
            $("#transliteration-button2").text("Hide Transliteration");
            $("#transliteration-button2").data("change-text","Show Transliteration");
        }else{
            $("#transliteration-button2").text("Show Transliteration");
            $("#transliteration-button2").data("change-text","Hide Transliteration");

        }
    });
</script>