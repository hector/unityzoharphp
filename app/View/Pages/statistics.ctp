<!-- Content Header (Page header) -->
<section class="content-header">
    <h1 class="page-header">Statistics
        <small>

        </small></h1>

</section>

<!-- Main content -->
<section class="content">

    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-orange">
                <div class="inner">
                    <h3>0</h3>
                    <p>Current Zohar cycle</p>
                </div>
                <div class="icon">
                    <i class="fa fa-refresh"></i>
                </div>
                <a href="#" class="small-box-footer">
                    <?php echo $percent ?>%
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3><?php echo $cyclec ?></h3>
                    <p>Current pinchas cycle</p>
                </div>
                <div class="icon">
                    <i class="fa fa-refresh"></i>
                </div>
                <a href="#" class="small-box-footer">
                    <?php echo $percentc ?>%
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-red">
                <div class="inner">
                    <h3><?php echo $zr + $pr ?></h3>
                    <p>Unique readers</p>
                </div>
                <div class="icon">
                    <i class="fa fa-user"></i>
                </div>
                <a href="#" class="small-box-footer">
                    <?php echo "Zohar: ".$zr ?> | <?php echo "Pinchas: ".$pr ?>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-xs-6">
            <div class="small-box bg-green">
                <div class="inner">
                    <h3><?php echo $cr ?></h3>
                    <p>Different countries</p>
                </div>
                <div class="icon">
                    <i class="fa fa-map-marker"></i>
                </div>
                <a href="#" class="small-box-footer">
                    &nbsp;
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <span class="text-muted"><em>This data have  10 minutes delay, <a href="<?php echo Router::url("/admin/?clear=1",true) ?>">refresh</a></em></span>
        </div>

    </div>

</section><!-- /.content -->


