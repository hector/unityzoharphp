<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">Dashboard
            <small>
                <i class="fa fa-angle-double-right"></i>
                Statistics
            </small></h1>
    </div>
    <!-- /.col-lg-12 -->
</div>
<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header text-center">Tzadiks</h2>

    </div>
</div>


<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-eye"></i>  Most Check Out Tzadiks
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="50%">Tzadik</th>
                            <th class="text-right">Last Visit</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($most as $row){?>
                        <tr>
                            <td><?php echo $row["Event"]["hits"] ?></td>
                            <td><?php echo $row["Event"]["name"] ?></td>
                            <td class="text-right"><span class="text-muted">
                                    <em>
                                    <?php echo $this->Time->timeAgoInWords(
                                    $row["Event"]["last_visit"],
                                    array(
                                        'format' => 'F jS, Y',
                                        'accuracy' => array('month' => 'month'),
                                        'end' => '+1 month')
                                ); ?>
                                    </em>
                                </span>
                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <div class="col-lg-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <i class="fa fa-star"></i>  Most Favorites Tzadiks
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="50%">Tzadik</th>
                            <th class="text-right">Last Add</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($favorite as $row){?>
                            <tr>
                                <td><?php echo $row[0]["total"] ?></td>
                                <td><?php echo $row["Event"]["name"] ?></td>
                                <td class="text-right"><span class="text-muted">
                                    <em>
                                        <?php echo $this->Time->timeAgoInWords(
                                            $row["EventUser"]["modified"],
                                            array(
                                                'format' => 'F jS, Y',
                                                'accuracy' => array('month' => 'month'),
                                                'end' => '+1 month')
                                        ); ?>
                                    </em>
                                </span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <i class="fa fa-map-marker"></i> Most Visited Tzadiks
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="50%">Tzadik</th>
                            <th class="text-right">Last Add</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($visited as $row){?>
                            <tr>
                                <td><?php echo $row[0]["total"] ?></td>
                                <td><?php echo $row["Event"]["name"] ?></td>
                                <td class="text-right"><span class="text-muted">
                                    <em>
                                        <?php echo $this->Time->timeAgoInWords(
                                            $row["EventUser"]["modified"],
                                            array(
                                                'format' => 'F jS, Y',
                                                'accuracy' => array('month' => 'month'),
                                                'end' => '+1 month')
                                        ); ?>
                                    </em>
                                </span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <div class="col-lg-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <i class="fa fa-fire"></i> Most Candles per Tzadiks
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="50%">Tzadik</th>
                            <th>Type</th>
                            <th class="text-right">Last Add</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($candles as $row){?>
                            <tr>
                                <td><?php echo $row[0]["total"] ?></td>
                                <td><?php echo $row["Event"]["name"] ?></td>
                                <td><?php echo ucfirst($row["Candle"]["sponsor_type"]) ?></td>
                                <td class="text-right"><span class="text-muted">
                                    <em>
                                        <?php echo $this->Time->timeAgoInWords(
                                            $row["Candle"]["modified"],
                                            array(
                                                'format' => 'F jS, Y',
                                                'accuracy' => array('month' => 'month'),
                                                'end' => '+1 month')
                                        ); ?>
                                    </em>
                                </span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <h2 class="page-header text-center">Users</h2>
    </div>
</div>
<div class="row">
    <div class="col-lg-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-user"></i> Most active users
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th width="50%">User</th>
                            <th>Platform</th>
                            <th class="text-right">Last connection</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($most_users as $row){?>
                            <tr>
                                <td></td>
                                <td><?php echo $row["User"]["nombres"]." ".$row["User"]["apellidos"] ?></td>
                                <td><?php echo ucfirst($row["UserToken"]["platform"]) ?></td>
                                <td class="text-right"><span class="text-muted">
                                    <em>
                                        <?php echo $this->Time->timeAgoInWords(
                                            $row["UserToken"]["created"],
                                            array(
                                                'format' => 'F jS, Y',
                                                'accuracy' => array('month' => 'month'),
                                                'end' => '+1 month')
                                        ); ?>
                                    </em>
                                </span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>