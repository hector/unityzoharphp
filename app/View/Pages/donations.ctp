<?php echo $this->element("web-header")?>
<div id="tzadkim-container">
    <div class="container">
        <h2 class="text-center page-title">Tzedaka</h2>
    </div>
</div>
<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-push-2 text-justify top30 bottom30">
                <div class="about-tzadikim ui-collapsible-content ui-body-inherit">
                    <p>Tzadikim is a free application sponsored with the donations of people around the world.</p>
                    <p>Our goal and focus is to provide you with tools to help you connect to the souls of the Tzadikim and have
                        their support in your prayers.</p>
                    <p>Your	donation	will	go	toward	supporting	the	expansion	of	this	app.	We	will	add	more	tzadikim
                        information, pictures	and	features.	This	app	is	one	of	the	Daily	Zohar	projects	and	our	goal	is	to	reach
                        and	provide	many	people	around	the	world	with	tools	to	enhance	their	spiritual	connections,	increasing
                        the	presence	of	the	Light	of	the	Creator	in	this	world.</p>
                    <p>We invite you to light a virtual candle on the page of the Tzadik’s you choose. Give a donation to support
                        and elevate your prayers with Light of sharing</p>

                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $this->element("web-pre-footer")?>
