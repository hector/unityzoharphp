<?php echo $this->element("web-header")?>
<div id="unity-container">
    <div class="container">
        <h2 class="text-center">Unity Zohar</h2>
    </div>
</div>
    <div class="main-content clearfix">
        <div class="container top15 bottom15">
            <div class="row">
                <div class="date-container top10">
                    <label class="date">%<span id="today-date"><?php echo $percent." | ".__("Current reading cycle: ").$cycle["ZoharCycle"]["cycle"]." | Members online: ". ($readers == 0 ?? 1) ?>
                </span>
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?php echo $this->element("meditation",[],['cache'=>['config'=>'day']]); ?>
                </div>
            </div>
            <div class="row top30">
                <div class="col-md-5 col-sm-12 col-md-offset-2">

                </div>
                <div class="col-md-3 col-sm-12 text-right">
                    <div class="btn-group font-sizes" role="group">
                        <button data-role="none" type="button" class="btn btn-default minus-btn"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
                        <span  data-role="none" class="btn btn-default no-btn"><i class="fa fa-font" aria-hidden="true"></i></span>
                        <button data-role="none" type="button" class="btn btn-default plus-btn"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
                    </div>
                </div>
            </div>
            <div class="row top10">

                <div class="col-md-5 col-xs-7 col-md-offset-2 col-sm-6 bottom15">
                    <div class="btn-group" role="group">
                        <a href="#" id="change-text" class="pull-left btn btn-unity" data-change-text="<?php echo __("Show Hebrew text") ?>"><?php echo __("Show Aramaic text") ?></a>
                        &nbsp;<a href="#" id="transliteration-button" class="pull-left btn btn-default" data-show-translation="off" data-change-text="<?php echo __("Hide Transliteration") ?>"><?php echo __("Show Transliteration") ?></a>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-xs-5 text-right bottom15">
                    <h4><strong><?php echo $parasha["Parasha"]["name"] ?> | <span class="zohar-text-no-bg"><?php echo $parasha["Parasha"]["name_hebrew"]  ?></span></strong></h4>
                </div>
                <div class="col-md-2">
                </div>
            </div>
            <?php foreach ($zohars as $key =>$zohar) {?>
                <div class="row zohar-paragraphs">
                    <div class="col-md-8 col-md-offset-2 zohar-text">
                        <p style="direction: ltr">
                            <?php if(is_file(WWW_ROOT."audio".DS.$zohar["Parasha"]["audio_name"]."-".sprintf("%03d", $zohar['Zohar']["paragraph_num"]).".mp3")){?>
                                <a class="audio {mp3:'<?php echo Router::url("/audio/",true).$zohar["Parasha"]["audio_name"]."-".sprintf("%03d", $zohar['Zohar']["paragraph_num"]).".mp3" ?>'}" href="<?php echo Router::url("/audio/",true).$zohar["Parasha"]["audio_name"]."-".sprintf("%03d", $zohar['Zohar']["paragraph_num"]).".mp3" ?>"><?php echo $zohar['Zohar']["paragraph_num"] ?></a>
                            <?php } ?>
                        </p>
                        <p class="hebrew-text size-14">
                            <span class="text-primary paragraph-nums" data-id="<?php echo $zohar['Zohar']["id"] ?>">
                                <?php echo $zohar['Zohar']["paragraph_num"]?>
                            </span>
                            <?php echo $zohar['Zohar']["text_hebrew"] ?>
                        </p>
                        <p class="aramaic-text size-14">
                           <span class="text-primary paragraph-nums" data-id="<?php echo $zohar['Zohar']["id"] ?>">
                                <?php echo $zohar['Zohar']["paragraph_num"]?>
                            </span>
                            <?php echo $zohar['Zohar']["text_aramaic"] ?>
                        </p>
                        <p class="transliterated-text transliteration size-14">
                            <?php echo $zohar['Zohar']["transliterated_hebrew"] ?>
                        </p>
                        <p class="transliterated-text-aramaic transliteration size-14">
                            <?php echo $zohar['Zohar']["transliterated_aramaic"] ?>
                        </p>
                    </div>
                </div>
            <?php  }?>
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $this->element("pray",[],['cache'=>['config'=>'day']]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <?php echo $this->element('candles_first',['candles'=>$first,'pre'=>true]/*['pre'=>true],[ "cache" => ['config'=>"minute15"]]*/); ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-5 col-xs-7 col-md-offset-2 col-sm-6 bottom15">
                </div>
                <div class="col-md-3 col-sm-6 col-xs-5 text-right">
                    <div class="inline-block" data-toggle="tooltip" data-placement="left" title="Next Zohar is available after 7 seconds delay">
                        <a href="<?php echo $this->request->here ?>" class="pull-right btn btn-unity next-zohar"><?php echo __("Next Zohar") ?></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-xs-12 bottom15 top17">
                    <h3 class="text-center">
                        <a href="<?php echo Router::url("/candles",true) ?>" class="add-header text-uppercase">Add your name to the list</a>
                        <hr />
                    </h3>
                </div>
            </div>
            <?php echo $this->element('candles',['candles'=>$candles]/*['pre'=>true],[ "cache" => ['config'=>"minute15"]]*/); ?>
            <div class="row">
                <div class="col-md-12">
                    <h4 class="text-center"><?php echo __("Most recent Zohar readers")?></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <?php echo $this->element("visitors_zohar",[],[ "cache" => ['config'=>"minute5"]]);?>
                </div>
            </div>
            <div class="row top10 bottom10">
                <div class="col-md-12 text-center">
                    <a href="<?php echo Router::url("/zohar-readers",true) ?>"><?php echo __("View them on Google maps")?></a>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(".audio").mb_miniPlayer({
            width:300,
            id3:false,
            addShadow:false,
            pauseOnWindowBlur: false,
            downloadPage:null,
            playAlone:false,
            inLine:true,
            skin:'gray',
            onEnd: playNext,
            onPlay:stopOthers,
            onMute:function(player){
                setTimeout(function(){
                    jQuery("[isPlaying='true']").not(".jp-state-playing").find(".map_play").trigger(jQuery.mbMiniPlayer.eventEnd);
                },250);

            }
        });
        function playNext(player) {
            var players = $(".audio");
            document.playerIDX = (player.idx <= players.length - 1 ? player.idx : 10000);
            var $newplayer = $(players).eq(document.playerIDX);
            var ofs = $("#"+$newplayer.attr("id")).siblings('.mbMiniPlayer').offset().top;
            $('html, body').animate({
                scrollTop: ofs
            }, 500);

            console.log();
            $newplayer.mb_miniPlayer_play();
        }
        function stopOthers(player) {
            $(player).jPlayer("pauseOthers",0);
        }
    </script>
<?php echo $this->element("web-pre-footer")?>
<?php
echo $this->Html->script(['audio/jquery.jplayer.min','audio/jquery.mb.miniPlayer.min'], array('inline' => false));
echo $this->Html->css(['web/miniplayer'], array('inline' => false));
?>