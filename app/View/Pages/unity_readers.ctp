<?php echo $this->element("web-header")?>
    <div id="unity-container">
        <div class="container">
            <h2 class="text-center">The Holy Zohar Readers Map</h2>
        </div>
    </div>
    <div class="main-content clearfix">
        <div class="container top15 bottom15">
            <div class="row top15 bottom15">
                <h3 class="text-center">We cover the globe with light of the Zohar<br /> <small>Due to the many thousands of locations on the map, it may take some time to load</small></h3>

                <div id="map-canvas"></div>
            </div>
        </div>
    </div>
<?php echo $this->element("web-pre-footer")?>
<script>
    var limit = 0;
</script>
<?php echo $this->Html->script('web/marked', array('inline' => true));?>
<?php echo $this->Html->script('web/maps', array('inline' => true));?>
<script>
    function loadScript(src,callback){

        var script = document.createElement("script");
        script.type = "text/javascript";
        if(callback)script.onload=callback;
        document.getElementsByTagName("head")[0].appendChild(script);
        script.src = src;
    }
    loadScript('https://maps.googleapis.com/maps/api/js?callback=initialize&key=AIzaSyByeQ_1J8q58Ud23fDcRnadC-XrORLNAWQ',
        function(){});
</script>
