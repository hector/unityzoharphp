<?php $hash = md5(time() . rand());
    echo $this->Html->script(['admin/bootbox.min'], array('inline' => false));
if (Configure::read("debug") > 0) {
    echo $this->Html->script('web/candles', array('inline' => false));
} else {
    echo $this->Html->script('web/candles.min', array('inline' => false));
}
?>
<?php echo $this->element("web-header") ?>
<div id="unity-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-title text-center">Healing List</h1>
            </div>
        </div>
    </div>
</div>
<div class="main-content bottom15">
    <div class="container">
        <?php echo $this->Form->create('User', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'id' =>'cc-form',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="row top15">
            <div class="col-lg-8 col-md-push-2">
                <div class="panel panel-default">
                    <div class="panel-heading text-center"><h4>Current active items</h4></div>
                    <ul class="list-group healing-list">
                        <?php foreach ($candles as $candle) { ?>
                            <li class="list-group-item <?php echo $candle["Candle"]["sponsor_type"] ?>">
                                <?php echo $candle["Candle"]["name"] . " " . $candle["Candle"]["gender"] . " " . $candle["Candle"]["parent_name"] ?>
                                &nbsp;
                                <em>
                                    (<?php echo date("M d, Y", strtotime('+' . $candle["Candle"]["donation"] . ' days', strtotime($candle["Candle"]["from"]))); ?>
                                    )</em>
                            </li>
                        <?php } ?>
                    </ul>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-push-2">
                    <div id="bio-donation-container" style="display:block;margin:0">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default panel-unity top30">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                           aria-expanded="true" aria-controls="collapseOne">
                                            <?php echo __("Healing"); ?>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                     aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">First Name</label>
                                            <input type="text" class="form-control first-name" name="data[Candle][name]" placeholder="First Name">
                                            <input type="hidden" name="data[Candle][sponsor_type]" id="sponsor_type" value="refua">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Gender</label><br/>
                                            <div class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default active">
                                                    <input type="radio" name="data[Candle][gender]" value="ben" checked="checked" /> Male
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="data[Candle][gender]" value="bat"> Female
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Mother's Name</label>
                                            <input type="text" class="form-control parent-name" name="data[Candle][parent_name]" placeholder="Mother name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default panel-unity">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            In bless memory (Z"L)
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">First Name</label>
                                            <input type="text" class="form-control first-name" name="data[Candle][name]" placeholder="First Name">
                                            <input type="hidden" name="data[Candle][sponsor_type]" id="sponsor_type" value="bless">
                                        </div>


                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Gender</label><br/>

                                            <div class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default active">
                                                    <input type="radio" name="data[Candle][gender]" value="ben" checked="checked" /> Male
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="data[Candle][gender]" value="bat"> Female
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Father's Name</label>
                                            <input type="text" class="form-control parent-name" name="data[Candle][parent_name]" placeholder="Father's name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default panel-unity">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Success in life
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">First Name</label>
                                            <input type="text" class="form-control first-name" name="data[Candle][name]" placeholder="First Name">
                                            <input type="hidden" name="data[Candle][sponsor_type]" id="sponsor_type" value="wish">
                                        </div>


                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Gender</label><br/>

                                            <div class="btn-group" data-toggle="buttons">
                                                <label class="btn btn-default active">
                                                    <input type="radio" name="data[Candle][gender]" value="ben" checked="checked" /> Male
                                                </label>
                                                <label class="btn btn-default">
                                                    <input type="radio" name="data[Candle][gender]" value="bat"> Female
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Father's Name</label>
                                            <input type="text" class="form-control parent-name" name="data[Candle][parent_name]" placeholder="Father's name">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
<!--                        <div class="panel panel-default" id="free-panel-title">-->
<!--                            <div class="panel-heading text-center"><h4>I would like to</h4></div>-->
<!--                        </div>-->
<!--                        <div class="panel panel-default" id="free-panel">-->
<!--                            <div class="list-group">-->
<!--                                <a href="#" class="list-group-item active" id="free-candle">-->
<!--                                    <strong>FREE</strong> Add names to the list for the next 10 days (only one Active Name FREE at a time)-->
<!--                                </a>-->
<!--                                <input type="hidden" id="donation-free" value="10">-->
<!--                            </div>-->
<!--                        </div>-->


                        <div class="panel panel-default">
                            <div class="panel panel-default" id="free-panel-title">
                                <div class="panel-heading text-center"><h4>I would like to</h4></div>
                            </div>
                            <div class="list-group">
                                <?php foreach ($prices as $price) {?>
                                    <label class="btn btn-default list-group-item" style="text-align: left">
                                        <input type="radio"
                                               name="data[Candle][donation]"
                                               data-price="<?php echo $price["Payment"]["price"] ?>"
                                               value="<?php echo $price["Payment"]["days"] ?>"
                                               class="radio-donation"
                                            />
                                        <span class="donation-price">$<?php echo $price["Payment"]["price"] ?></span> -
                                        Add names to the list for the next <span class="donation-days"><?php echo $price["Payment"]["days"] ?></span>
                                        days.<span> </span>
                                    </label>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-md-push-2">
                <fieldset id="payment-data">
                    <legend class="text-center">Payment data</legend>
                    <input type="hidden" id="concept" name="data[cc][text]"/>
                    <input type="hidden" id="amount" name="data[cc][amount]"/>

                    <!--<div class="control-group">
                        <label label-default="" class="control-label">Card Holder's Name</label>

                        <div class="controls">
                            <input type="text" class="form-control" placeholder="First and last name"
                                   name="data[cc][card_name]" id="card-name"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label label-default="" class="control-label">Card Type</label>

                        <div class="controls">
                            <div class="row">
                                <div class="col-md-12">
                                    <select class="form-control" name="data[cc][type]">
                                        <option value="visa">Visa</option>
                                        <option value="mastercard">Mastercard</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div class="control-group">
                        <label label-default="" class="control-label">Card Number</label>

                        <div class="controls">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" id="card-number" value="" class="form-control"
                                           autocomplete="off" name="data[cc][number]" maxlength="16"
                                           placeholder="Only numbers">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label label-default="" class="control-label">Card Expiry Date</label>

                        <div class="controls">
                            <div class="row">
                                <div class="col-md-9">
                                    <select class="form-control" name="data[cc][expire_month]">
                                        <option value="01">January</option>
                                        <option value="02">February</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control" name="data[cc][expire_year]">
                                        <?php
                                        for ($i = (int)date("Y"); $i < (int)date("Y", strtotime("+10 years")); $i++) {
                                            ?>
                                            <option value="<?php echo $i?>"><?php echo $i?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label label-default="" class="control-label">Card CVV</label>

                        <div class="controls">
                            <div class="row">
                                <div class="col-md-12">
                                    <input type="text" id="card-cvv" maxlength="4" class="form-control col-md-3"
                                           name="data[cc][cvv]">
                                </div>
                                <div class="col-md-8"></div>
                            </div>
                        </div>
                    </div>
                    <div class="control-group">
                        <label label-default="" class="control-label"></label>

                        <div class="controls">
                            <button type="submit" class="btn bottom17 btn-default btn-unity btn-block">Create Candle
                            </button>

                        </div>
                    </div>
                </fieldset>

            </div>
        </div>

        <?php echo $this->Form->end(); ?>
    </div>
    <!--/.row-fluid-->
</div>
</div>
<?php echo $this->element("web-pre-footer") ?>
<div id="modal-background"></div>
<div id="modal-content">
    <label class="btn btn-success btn-block" id="label-message">Creating Candle</label>
    <div class="top15 text-center">
        <p class="text-center">Please Wait</p>
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>

</div>
</div>