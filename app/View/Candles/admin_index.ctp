<section class="content-header">
    <h1>Candle <small><i class="fa fa-angle-double-right"></i> All</small></h1>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-12">
    <table class="table table-hover table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th><?php echo $this->Paginator->sort('User.full_name', ' User',array('class'=>'icon-sort'));?></th>
                <th><?php echo $this->Paginator->sort('Candle.name', ' Name',array('class'=>'icon-sort'));?></th>
                <th><?php echo $this->Paginator->sort('Candle.parent_name', ' Parent Name',array('class'=>'icon-sort'));?></th>
                <th>Time</th>
                <th><?php echo $this->Paginator->sort('Candle.from', ' From',array('class'=>'icon-sort'));?></th>
                <th>Payment Id</th>
                <th><?php echo $this->Paginator->sort('Candle.created', ' Created',array('class'=>'icon-sort'));?></th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($rows as $row) {?>
            <tr>
                <td><?php echo $row["Candle"]["id"]?></td>
                <td><?php echo $row["User"]["name"]." ".$row["User"]["lastname"]?></td>
                <td><?php echo $row["Candle"]["name"]?></td>
                <td><?php echo $row["Candle"]["parent_name"]?></td>
                <td><?php echo $row["Candle"]["donation"]?> days</td>
                <td><?php echo date("M j, Y",strtotime($row["Candle"]["from"])) ?></td>
                <td><?php echo ($row["Candle"]["payment_id"])?$row["Candle"]["payment_id"]:'Candle w/o a payment'?> </td>
                <td><?php echo $this->Time->timeAgoInWords(
                        $row["Candle"]["created"],
                        array(
                            'format' => 'M jS, Y',
                            'accuracy' => array('month' => 'month'),
                            'end' => '+1 month')
                    ); ?>
                </td>
                <td>
                    <?php echo $this->Html->link(
                        '<i class="fa-edit fa fa-1"></i>',
                        array(
                            'controller' => 'candles',
                            'action' => 'edit',
                            $row["Candle"]['id'],
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-xs btn-info','title'=>__("Edit"))
                    );?>
                </td>
            </tr>
            <?php } ?>
            </tbody>
        </table>

        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination center-block pagination-large text-center">
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
            <div class="center-block text-center">
                <p class="muted"> <?php echo $this->Paginator->counter(
                        __('Page').' {:page}/{:pages}, '.__('Records').' {:current}/{:count}, '.__('from').' {:start} '.__('To').' {:end}');?>
                </p>
                <p>
                <?php echo $this->Html->link(
                    '<i class="fa fa-pencil"></i> '.__("Add new"),
                    array(
                        'controller' => 'candles',
                        'action' => 'add',
                        'full_base' => true,
                        'admin'=>true
                    ),array('escape' => false,'class'=>'btn btn-primary')
                );?>
                </p>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    </section>