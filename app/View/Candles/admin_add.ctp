<?php
echo $this->Html->script('admin/chosen.jquery.min', array('inline' => false));
echo $this->Html->script('admin/moment', array('inline' => false));
echo $this->Html->script('admin/bootstrap-datetimepicker', array('inline' => false));
echo $this->Html->css('admin/chosen.min', array('inline' => false));
echo $this->Html->css('admin/bootstrap-datetimepicker.min', array('inline' => false));
?>
<section class="content-header">
    <h1>Candle <small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <?php echo $this->Form->create('Candle', array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'type' => 'file',
                'novalidate' => true,
                'inputDefaults' => array(
                    'label' => false,
                    'div' => false,
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
                )
            )); ?>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1"><?php echo __("User")?></label>
                <div class="controls col-sm-5">
                    <?php echo $this->Form->input('Candle.user_id',array('options'=>$users,'class' => 'form-control chosen-select'));?>
                </div>
                <div class="controls col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1"><?php echo __("Type")?></label>
                <div class="controls col-sm-5">
                    <?php echo $this->Form->input('Candle.sponsor_type',array('options'=>array("refua"=>'Refua','wish'=>'Wish','bless'=>'Elevation'),'class' => 'form-control'));?>
                </div>
                <div class="controls col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1"><?php echo __("Name")?></label>
                <div class="controls col-sm-5">
                    <?php echo $this->Form->input('Candle.name',array("type"=>"text",'class' => 'form-control'));?>
                </div>
                <div class="controls col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1"><?php echo __("Gender")?></label>
                <div class="controls col-sm-5">
                    <?php echo $this->Form->input('Candle.gender',array('options'=>array("ben"=>'Ben','bat'=>'Bat'),'class' => 'form-control'));?>
                </div>
                <div class="controls col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1"><?php echo __("Parent Name")?></label>
                <div class="controls col-sm-5">
                    <?php echo $this->Form->input('Candle.parent_name',array("type"=>"text",'class' => 'form-control'));?>
                </div>
                <div class="controls col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1"><?php echo __("Time")?></label>
                <div class="controls col-sm-5">
                    <div class="input-group">
                        <?php echo $this->Form->input('Candle.donation',array('type'=>'text','class' => 'form-control'));?>
                        <span class="input-group-addon">days</span>
                    </div>

                </div>
                <div class="controls col-sm-5">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label" for="form-field-1"><?php echo __("From")?></label>
                <div class="controls col-sm-5">
                    <div class='input-group date' id='datetimepicker3'>
                        <?php echo $this->Form->input('Candle.from',array('type'=>'text','class' => 'form-control','readonly'=>'readonly',"data-date-format"=>"YYYY-MM-DD"));?>
                        <span class="input-group-addon"><span class="fa fa-calendar fa-fw"></span>
                    </span>
                    </div>
                </div>
                <div class="controls col-sm-5">
                </div>
            </div>
            <div class="controls col-sm-9">
                <div class="text-center">
                    <?php echo $this->Form->submit(__('Save'),array('class'=>'btn btn-primary'));?>
                </div>
            </div>
            <?php echo $this->Form->end();?>

        </div>
    </div>
</section>
<script>
    $(function(){
        $(".chosen-select").chosen({allow_single_deselect: true,width: "100%"});
        $('#datetimepicker3').datetimepicker({
            pickTime: false,
            minDate: new Date()
        });
    });
</script>