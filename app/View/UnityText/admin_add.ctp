<?php
    echo $this->Html->script('admin/ckeditor/ckeditor', array('inline' => false));
?>
<section class="content-header">
    <h1>Unity Text<small><i class="fa fa-angle-double-right"></i> add</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('UnityText', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
            <div class="form-group">
                <label>Abbreviation</label>
                <?php echo $this->Form->input('UnityText.abbreviation',array("type"=>"text",'class' => 'form-control'));?>
            </div>
            <div class="form-group">
                <label>Location</label>
                <?php echo $this->Form->input('UnityText.location',['options'=>['none'=>'None','home'=>'Home'],'class' => 'form-control','placeholder'=>'']);?>
            </div>
            <div class="form-group">
                <label>Text</label>
                <?php echo $this->Form->input('UnityText.text',array('class' => 'form-control ckeditor','type'=>'textarea','placeholder'=>''));?>
            </div>
        <div class="form-group">
            <label>Notes</label>
            <?php echo $this->Form->input('UnityText.notes',array('class' => 'form-control ckeditor zohar-text-no-bg','type'=>'textarea','placeholder'=>'Short info'));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
    <script>
        CKEDITOR.stylesSet.add( 'my_styles', [
            { name: 'Hebrew text', element: 'span', attributes: { 'class': 'zohar-text-no-bg' } }
        ] );
        CKEDITOR.config.contentsCss = '<?php echo Router::url("/css/admin/ckeditor.css") ?>';
       CKEDITOR.config.stylesSet = ('my_styles');
        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    </script>
</section><!-- /.content -->