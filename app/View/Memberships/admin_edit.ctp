<section class="content-header">
    <h1>Membership <small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('Membership', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Membership</label>
            <?php echo $this->Form->input('Membership.name',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Description</label>
            <?php echo $this->Form->input('Membership.description',array("type"=>"textarea",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Cost</label>
            <?php echo $this->Form->input('Membership.cost',array('class' => 'form-control'));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->