<?php
echo $this->Html->script('admin/ckeditor/ckeditor', array('inline' => false));
?>
<section class="content-header">
    <h1>Article<small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('Article', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Parasha</label>
            <?php echo $this->Form->input('Article.parasha_id',array('options'=>$parashot,'class' => 'form-control chosen-select'));?>
        </div>
        <div class="form-group">
            <label>Title</label>
            <?php echo $this->Form->input('Article.title',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Hebrew title</label>
            <?php echo $this->Form->input('Article.title_hebrew',array("type"=>"text",'class' => 'form-control zohar-text-no-bg'));?>
        </div>
        <div class="form-group">
            <label>Start Paragraph</label>
            <?php echo $this->Form->input('Article.paragraph_start',array("type"=>"text",'class' => 'form-control','placeholder'=>'Start'));?>
        </div>
        <div class="form-group">
            <label>End Paragraph</label>
            <?php echo $this->Form->input('Article.paragraph_end',array('class' => 'form-control','type'=>'text','placeholder'=>'End'));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->