<section class="content-header">
    <h1>Articles <small><i class="fa fa-angle-double-right"></i> All</small></h1>
</section>
<section class="content">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort('Article.id', ' ID',array('class'=>'icon-sort'));?></th>
                <th><?php echo $this->Paginator->sort('Article.title', ' Title',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('Article.title_hebrew', ' Title Hebrew',array('class'=>'icon-sort'));?> </th>
                <th class="text-center"><?php echo $this->Paginator->sort('Parasha.name', ' Parasha',array('class'=>'icon-sort'));?> </th>
                <th width="80px">Action</th>
            </tr>
            <?php foreach ($rows as $row) {?>
            <tr>
                <td><?php echo $row["Article"]["id"] ?></td>
                <td><?php echo $row["Article"]["title"] ?></td>
                <td class="zohar-text-no-bg"><?php echo $row["Article"]["title_hebrew"] ?></td>
                <td><?php echo $row["Parasha"]["name"] ?></td>
                <td>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-edit bigger-120"></i>',
                        array(
                            'controller' => 'articles',
                            'action' => 'edit',
                            $row[Inflector::singularize($this->name)]['id'],
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-mini btn-info','title'=>__("Edit"))
                    );?>
                </td>
            </tr>
            <?php }?>
        </table>
    </div><!-- /.box-body -->
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination center-block pagination-large text-center">
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
            <div class="center-block text-center">
                <p class="muted"> <?php echo $this->Paginator->counter(
                        __('Page').' {:page}/{:pages}, '.__('Records').' {:current}/{:count}, '.__('from').' {:start} '.__('To').' {:end}');?>
                </p>
                <p>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-pencil"></i> '.__("Add new"),
                        array(
                            'controller' => 'articles',
                            'action' => 'add',
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-primary')
                    );?>
                </p>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</section><!-- /.content -->