<?php
/**
 * @var $this view
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo __("Admin dashboard") ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php
    /*---------------------------------------------------------------------------------*/
    echo $this->Html->script(
        array(
            '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
            '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js',
            'admin/bootbox.min',
            'admin/app',
        ));
    /*---------------------------------------------------------------------------------*/
    ?>
    <?php echo $this->Html->css(
                    array(
                        '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css',
                        '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css',
                        'admin/AdminLTE','admin/default'));
    ?>

    <?php
        echo $this->Html->meta('icon');
        echo $this->App->js();
        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
    ?>

</head>
<body class="skin-black fixed">
    <?php echo $this->element('navbar'); ?>
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <?php echo $this->element('sidebar'); ?>
        <aside class="right-side">
            <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
        </aside>
    </div>
</body>
</html>