<?php
/**
 * @var $this view
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo __("Error::Layout") ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $this->Html->script(
        array(
            '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js',
        ));
    ?>
    <?php echo $this->Html->css(
        array(
            '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css',
            '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.min.css',
            'admin/default'));
    ?>
    <?php echo $this->App->js();?>
    <?php echo $this->fetch('meta');?>
    <?php echo $this->fetch('css');?>
    <?php echo $this->fetch('script');?>

</head>
<body class="dark-bg">
<?php if(Configure::read('debug')>0){?>
    <div class="container">
        <div class="well well-lg">
            <?php echo $this->fetch('content'); ?>
        </div>
    </div>
<?php }else{?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="error-template">
                    <h1>
                        Oops!</h1>
                    <h2>Server Error</h2>
                    <div class="error-details">
                        Sorry, an error has occured, Requested page not found!
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php }?>
</body>
</html>
