<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		<?php echo ($title_for_layout=='Pages')?'Unity Zohar':$title_for_layout; ?>
	</title>

    <?php echo $this->Html->script(
        array(
            '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
            '//ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js',
            '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js',
            'web/jquery.cookie',
        ));

    if(Configure::read("debug")>0) {
        echo $this->Html->script(array('web/default.js?time=123'));
    }else{
        echo $this->Html->script(array('web/default.min.js?time=123'));
    }

    ?>
    <?php echo $this->Html->css(
        array(
            '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css',
            '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css',
            ));

    if(Configure::read("debug")>0) {
        echo $this->Html->css(array('web/default.css?time=123'));
    }else{
        echo $this->Html->css(array('web/default.min.css?time=123'));
    }

    ?>
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
        echo $this->App->js();
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-3443450-23']);
        _gaq.push(['_trackPageview']);
        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();

    </script>
</head>
<body>
    <?php echo $this->element('web_navbar'); ?>
    <div class="container">
        <?php echo $this->Session->flash(); ?>
    </div>
    <?php echo $this->fetch('content'); ?>
    <?php echo $this->element('web-footer'); ?>

</body><!-- Default -->
</html>
