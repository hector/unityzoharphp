<?php
/**
 * @var $this view
 */
?>
<!DOCTYPE html>
<html class="bg-black">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $page_title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php echo $this->Html->css(
        array(
            '//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css',
            '//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.2.0/css/font-awesome.min.css',
            'admin/AdminLTE',
            )
    );
    ?>
</head>
<body class="bg-black">

<div class="form-box" id="login-box">
    <div class="header bg-orange">Unity zohar</div>
    <?php echo $this->Form->create('User') ?>
        <div class="body bg-gray">
            <div class="form-group">
                <?php echo $this->Form->input('username',array('class'=>'form-control','placeholder'=>__("Username"),'label'=>false));?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>__("Password"),'label'=>false));?>
            </div>
        </div>
        <div class="footer">
            <button type="submit" class="btn bg-orange btn-block">Sign me in</button>
        </div>
    <?php echo $this->Form->end(); ?>
</div>
<?php //echo $this->fetch('content'); ?>




<?php
    /*------------------------------------------------------------------------------------*/
        echo $this->Html->script(
        array(
            '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js',
            '//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js',
        ));
    /*------------------------------------------------------------------------------------*/
?>
</body>
</html>