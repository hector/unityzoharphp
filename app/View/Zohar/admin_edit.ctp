<?php
    echo $this->Html->script('admin/ckeditor/ckeditor', array('inline' => false));
?>
<section class="content-header">
    <div class="box-body text-center bottom15">
        <form class="form-inline" method="post">
            Go to paragraph:
            <div class="form-group">
                <?php echo $this->Form->input('Zohar.parasha_id',array('options'=>['- Parasha']+$parashot,'class' => 'form-control chosen-select','label'=>false));?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('Zohar.paragraph_num',array('class' => 'form-control','label'=>false));?>
            </div>
            <button type="submit" name="nav" class="btn btn-default">Go</button>
        </form>
    </div>
    <div class="row">
        <div class="col-sm-4">
            <a href="<?php echo Router::url("/admin/zohar/prev/".$this->data["Zohar"]["parasha_id"]."/".$this->data["Zohar"]["paragraph_num"],true) ?>" class="btn bg-olive"><i class="fa fa-angle-double-left"></i> Prev</a>
        </div>
        <div class="col-sm-4 text-center">
            <a href="<?php echo Router::url("/admin/zohar/translation/".$this->data["Zohar"]["id"],true) ?>" class="btn bg-maroon"><i class="fa fa-language"></i> Translate</a>
        </div>
        <div class="col-sm-4 text-right">
            <a href="<?php echo Router::url("/admin/zohar/next/".$this->data["Zohar"]["parasha_id"]."/".$this->data["Zohar"]["paragraph_num"],true) ?>" class="btn bg-olive">
                Next <i class="fa fa-angle-double-right"></i>
            </a>
        </div>
    </div>
    <hr />
    <h1>Zohar<small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">
    <div class="box-body">
        <?php echo $this->Form->create('Zohar', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
            <div class="form-group">
                <label>Parasha</label>
                <?php echo $this->Form->input('Zohar.parasha_id',array('options'=>$parashot,'class' => 'form-control chosen-select','disabled'=>'disabled'));?>
            </div>
            <div class="form-group">
                <label>Paragraph #</label>
                <?php echo $this->Form->input('Zohar.paragraph_num',array("type"=>"text",'class' => 'form-control','disabled'=>'disabled'));?>
            </div>
            <div class="form-group">
                <label>Hebrew Paragraph #</label>
                <?php echo $this->Form->input('Zohar.paragraph_num_h',array("type"=>"text",'class' => 'form-control zohar-text-no-bg','disabled'=>'disabled'));?>
            </div>
            <div class="form-group">
                <label>Aramaic Text</label>
                <?php echo $this->Form->input('Zohar.text_aramaic',array('class' => 'form-control ckeditor zohar-text-no-bg','type'=>'textarea','placeholder'=>''));?>
            </div>
        <div class="form-group">
            <label>Hebrew Text</label>
            <?php echo $this->Form->input('Zohar.text_hebrew',array('class' => 'form-control ckeditor zohar-text-no-bg','type'=>'textarea','placeholder'=>'Short info'));?>
        </div>
<!--        <div class="form-group">-->
<!--            <label>English Text</label>-->
<!--            --><?php //echo $this->Form->input('Zohar.text_english',array('class' => 'form-control ckeditor','type'=>'textarea','placeholder'=>'Short info'));?>
<!--        </div>-->
        <div class="form-group">
            <label>Notes</label>
            <p class="text-muted">
                For youtube or vimeo videos just insert de URL (youtube => https://www.youtube.com/watch?v=VIDEO_ID  | vimeo => https://vimeo.com/VIDEO_ID)
            </p>
            <?php echo $this->Form->input('Zohar.notes',array('class' => 'form-control ckeditor','type'=>'textarea','placeholder'=>'Short info'));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
    <script type="text/javascript">
        CKEDITOR.replace( 'ZoharTextAramaic',{
            bodyClass : "zohar-text-no-bg",
            enterMode :  CKEDITOR.ENTER_BR
        });
        CKEDITOR.replace( 'ZoharTextHebrew',{
            bodyClass : "zohar-text-no-bg",
            enterMode :  CKEDITOR.ENTER_BR
        });
        CKEDITOR.replace( 'ZoharNotes',{
            enterMode :  CKEDITOR.ENTER_BR
        });
        //CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    </script>
</section><!-- /.content -->