<?php
    echo $this->Html->script('admin/ckeditor/ckeditor', array('inline' => false));
?>
<section class="content-header">
    <h1>Zohar<small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('Zohar', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
            <div class="form-group">
                <label>Parasha</label>
                <?php echo $this->Form->input('Zohar.parasha_id',array('options'=>$parashot,'class' => 'form-control chosen-select'));?>
            </div>
            <div class="form-group">
                <label>Paragraph #</label>
                <?php echo $this->Form->input('Zohar.paragraph_num',array("type"=>"text",'class' => 'form-control'));?>
            </div>
            <div class="form-group">
                <label>Hebrew Paragraph #</label>
                <?php echo $this->Form->input('Zohar.paragraph_num_h',array("type"=>"text",'class' => 'form-control zohar-text-no-bg'));?>
            </div>
            <div class="form-group">
                <label>Aramaic Text</label>
                <?php echo $this->Form->input('Zohar.text_aramaic',array('class' => 'form-control ckeditor zohar-text-no-bg','type'=>'textarea','placeholder'=>''));?>
            </div>
        <div class="form-group">
            <label>Hebrew Text</label>
            <?php echo $this->Form->input('Zohar.text_hebrew',array('class' => 'form-control ckeditor zohar-text-no-bg','type'=>'textarea','placeholder'=>'Short info'));?>
        </div>
<!--        <div class="form-group">-->
<!--            <label>English Text</label>-->
<!--            --><?php //echo $this->Form->input('Zohar.text_english',array('class' => 'form-control ckeditor','type'=>'textarea','placeholder'=>'Short info'));?>
<!--        </div>-->
        <div class="form-group">
            <label>Notes</label>
            <?php echo $this->Form->input('Zohar.notes',array('class' => 'form-control ckeditor','type'=>'textarea','placeholder'=>'Short info'));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
    <script type="text/javascript">
        CKEDITOR.replace( 'ZoharTextAramaic',{
            bodyClass : "zohar-text-no-bg",
            enterMode :  CKEDITOR.ENTER_BR
        });
        CKEDITOR.replace( 'ZoharTextHebrew',{
            bodyClass : "zohar-text-no-bg",
            enterMode :  CKEDITOR.ENTER_BR
        });
        CKEDITOR.replace( 'ZoharNotes',{
            bodyClass : "zohar-text-no-bg",
            enterMode :  CKEDITOR.ENTER_BR
        });
    </script>
</section><!-- /.content -->