<section class="content-header">
    <h1>Zohar Paragraph's <small><i class="fa fa-angle-double-right"></i> All</small></h1>
</section>
<section class="content">
    <div class="box-body text-center bottom15">
        <form class="form-inline" method="post">
            Go to paragraph:
            <div class="form-group">
                    <?php echo $this->Form->input('Zohar.parasha_id',array('options'=>['- Parasha']+$parashot,'class' => 'form-control chosen-select','label'=>false));?>
            </div>
            <div class="form-group">
                <?php echo $this->Form->input('Zohar.paragraph_num',array('class' => 'form-control','label'=>false));?>
            </div>
            <button type="submit" name="nav" class="btn btn-default">Go</button>
        </form>
    </div>
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort('Zohar.id', ' ID',array('class'=>'icon-sort'));?></th>
                <th><?php echo $this->Paginator->sort('Parasha.name', ' Parasha',array('class'=>'icon-sort'));?> </th>
                <th>Parasha Hebrew Name</th>
                <th class="text-center"><?php echo $this->Paginator->sort('Zohar.paragraph_num', ' Paragraph',array('class'=>'icon-sort'));?> </th>
                <th class="text-center"><?php echo $this->Paginator->sort('Zohar.paragraph_num_h', ' Hebrew Paragraph',array('class'=>'icon-sort'));?> </th>

                <th width="140px" class="text-center">Action</th>
            </tr>
            <?php foreach ($rows as $row) {?>
            <tr>
                <td><?php echo $row["Zohar"]["id"] ?></td>
                <td><?php echo $row["Parasha"]["name"] ?></td>
                <td class="zohar-text-no-bg"><?php echo $row["Parasha"]["name_hebrew"] ?></td>
                <td class="text-center"><?php echo $row["Zohar"]["paragraph_num"] ?></td>
                <td class="text-center zohar-text-no-bg"><?php echo $row["Zohar"]["paragraph_num_h"] ?></td>
                <td class="text-center">
                    <?php echo $this->Html->link(
                        '<i class="fa fa-edit bigger-120"></i>',
                        array(
                            'controller' => 'zohar',
                            'action' => 'edit',
                            $row[Inflector::singularize($this->name)]['id'],
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-mini btn-info','title'=>__("Edit"))
                    );?>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-language"></i>',
                        array(
                            'controller' => 'zohar',
                            'action' => 'translation',
                            $row[Inflector::singularize($this->name)]['id'],
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-mini bg-maroon','title'=>__("Add translation"))
                    );?>

                </td>
            </tr>
            <?php }?>
        </table>
    </div><!-- /.box-body -->
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination center-block pagination-large text-center">
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
            <div class="center-block text-center">
                <p class="muted"> <?php echo $this->Paginator->counter(
                        __('Page').' {:page}/{:pages}, '.__('Records').' {:current}/{:count}, '.__('from').' {:start} '.__('To').' {:end}');?>
                </p>
                <p>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-pencil"></i> '.__("Add new"),
                        array(
                            'controller' => 'zohar',
                            'action' => 'add',
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-primary')
                    );?>
                </p>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</section><!-- /.content -->