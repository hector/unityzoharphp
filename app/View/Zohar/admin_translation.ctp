<?php
echo $this->Html->script('admin/ckeditor/ckeditor', array('inline' => false));
?>
<section class="content-header">
    <div class="row">
        <div class="col-sm-12 text-center">
            <a href="<?php echo Router::url("/admin/zohar/edit/".$zohar["Zohar"]["id"],true) ?>" class="btn btn-info"><i class="fa fa-pencil"></i> Edit this paragraph</a>
        </div>
    </div>
    <hr />
    <h1>Zohar<small><i class="fa fa-angle-double-right"></i> Translation</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('Translation', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <?php echo $this->Form->input('Translation.id',array('type'=>'hidden'));?>
        <?php echo $this->Form->input('Translation.zohar_id',array('type'=>'hidden'));?>
        <div class="form-group">
            <label>Aramaic Text</label>
            <?php echo $this->Form->input('Zohar.text_aramaic',array('class' => 'form-control ckeditor zohar-text-no-bg','type'=>'textarea','value'=>$zohar["Zohar"]['text_aramaic']));?>
        </div>
        <div class="form-group">
            <label>Hebrew Text</label>
            <?php echo $this->Form->input('Zohar.text_hebrew',array('class' => 'form-control ckeditor zohar-text-no-bg','type'=>'textarea','value'=>$zohar["Zohar"]['text_hebrew']));?>
        </div>
        <div class="form-group">
            <label>Language</label>
            <?php echo $this->Form->input('Translation.language_id',array('options'=>$languages,'class' => 'form-control chosen-select'));?>
        </div>
        <div class="form-group">
            <label>Translation</label>
            <?php echo $this->Form->input('Translation.text',array('class' => 'form-control ckeditor','type'=>'textarea'));?>
        </div>
        <div class="form-group">
            <label>Translation notes</label>
            <?php echo $this->Form->input('Translation.notes',array('class' => 'form-control ckeditor','type'=>'textarea'));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
    <script type="text/javascript">
        CKEDITOR.replace( 'ZoharTextAramaic',{
            bodyClass : "zohar-text-no-bg",
            toolbar: 'Custom',
            toolbarStartupExpanded : false,
            toolbarCanCollapse  : false,
            toolbar_Custom: [],
            readOnly:true
        });
        CKEDITOR.replace( 'ZoharTextHebrew',{
            bodyClass : "zohar-text-no-bg",
            toolbar: 'Custom',
            toolbarStartupExpanded : false,
            toolbarCanCollapse  : false,
            toolbar_Custom: [],
            readOnly:true
        });
        CKEDITOR.replace( 'TranslationText',{
            bodyClass : ""
        });
        CKEDITOR.replace( 'TranslationNotes',{
            bodyClass : ""
        });
        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;

        $(function(){
            $("#TranslationLanguageId").on("change",function(){
                var zohar_id = $("#TranslationZoharId").val();
                document.location.href = Admin.basePath+"admin/zohar/translation/"+zohar_id+"/"+$(this).val();
            });
        });


    </script>
</section><!-- /.content -->