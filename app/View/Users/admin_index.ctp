<section class="content-header">
    <h1>Users <small><i class="fa fa-angle-double-right"></i> All</small></h1>
</section>
<section class="content">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort('id', ' ID',array('class'=>'icon-sort'));?></th>
                <th><?php echo $this->Paginator->sort('name', ' Name',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('lastname', ' Lastname',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('role', ' Role',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('membership_id', ' Membership',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('active', ' Status',array('class'=>'icon-sort'));?> </th>
                <th width="180px">Action</th>
            </tr>
            <?php foreach ($rows as $row) {?>
                <tr>
                    <td><?php echo $row["User"]["id"] ?></td>
                    <td><?php echo $row["User"]["name"] ?></td>
                    <td><?php echo $row["User"]["lastname"] ?></td>
                    <td><?php echo ($row["User"]["role"]=='userapp')?'Web User': $row["User"]["role"]?></td>
                    <td><?php echo ($row["User"]["membership_id"]!=0)?$row["Membership"]['name']:"Not a member"?></td>
                    <td>
                        <?php
                        $class = ($row['User']['active']==1)?'label-success':'label-danger';
                        $type = ($row['User']['active']==1)?'Active':'No active';
                        ?>
                        <label class="label <?php echo $class ?>"><?php echo $type ?></label>

                    </td>
                    <td>
                        <?php echo $this->Html->link(
                            '<i class="fa-edit fa fa-1"></i>',
                            array(
                                'controller' => 'users',
                                'action' => 'edit',
                                $row["User"]['id'],
                                'full_base' => true,
                                'admin'=>true
                            ),array('escape' => false,'class'=>'btn btn-mini btn-info','title'=>__("Edit"))
                        );?>
                        <?php if($row["User"]["role"]=='userapp') {
                            echo $this->Html->link(
                                '<i class="fa-eye fa fa-1"></i>',
                                array(
                                    'controller' => 'users',
                                    'action' => 'detail',
                                    $row["User"]['id'],
                                    'full_base' => true,
                                    'admin' => true
                                ), array('escape' => false, 'class' => 'btn btn-mini btn-success', 'title' => __("Edit"))
                            );
                        }
                        ?>
                    </td>
                </tr>
            <?php }?>
        </table>
    </div><!-- /.box-body -->
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination center-block pagination-large text-center">
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
            <div class="center-block text-center">
                <p class="muted"> <?php echo $this->Paginator->counter(
                        __('Page').' {:page}/{:pages}, '.__('Records').' {:current}/{:count}, '.__('from').' {:start} '.__('To').' {:end}');?>
                </p>
                <p>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-pencil"></i> '.__("Add new"),
                        array(
                            'controller' => 'users',
                            'action' => 'add',
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-primary')
                    );?>
                </p>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</section><!-- /.content -->