<?php echo $this->element("web-header")?>
<div id="unity-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-title text-center">Updating Profile</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
</div>
<div class="container register">
<div class="row">


    <div class="col-lg-6 col-lg-offset-3">
            <?php echo $this->Form->create('User', array(
                'class' => 'form-horizontal',
                'data-role' => 'none',
                'data-ajax' => 'false',
                'type' => 'file',
                'novalidate' => true,
                'inputDefaults' => array(
                    'label' => false,
                    'div' => false,
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
                )
                )); ?>
            <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Email")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.email',array('class' => 'form-control input-xlarge','type'=>'text','readonly'=>'readonly'));?>
                </div>
            </div>
        <?php echo $this->Form->input('User.site',array('class' => 'form-control input-xlarge','type'=>'hidden'));?>
            <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Name")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.name',array('class' => 'form-control input-xlarge','type'=>'text'));?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Last name")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.lastname',array('class' => 'form-control input-xlarge','type'=>'text'));?>
                </div>
            </div>
            <div class="text-center bottom30">
                <?php echo $this->Form->submit(__('Save'),array('class'=>'btn btn-primary','data-role'=>'none'));?>
            </div>

            <?php echo $this->Form->end();?>
    </div><!--/.span-->
</div><!--/.row-fluid-->
</div>