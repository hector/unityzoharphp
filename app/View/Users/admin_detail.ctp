<section class="content-header">
    <h1>Users <small><i class="fa fa-angle-double-right"></i> detail</small></h1>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-5">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-user"></i> Account
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body hidden"></div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>Name:</td><td><?php echo $user["User"]["full_name"] ?></td>
                        </tr>
                        <tr>
                            <td>Email:</td><td><?php echo $this->Text->autoLinkEmails($user["User"]["email"]) ?></td>
                        </tr>
                        <tr>
                            <td>Created:</td>
                            <td><?php echo $this->Time->timeAgoInWords(
                                    $user["User"]["created"],
                                    array(
                                        'format' => 'F jS, Y',
                                        'accuracy' => array('month' => 'month'),
                                        'end' => '+1 month')
                                ); ?>
                            </td>
                        </tr>
                        <tr>
                            <td>Status:</td>
                            <td><?php
                                $class = ($user[Inflector::singularize($this->name)]['active']?'success':'danger');
                                $text = ($user[Inflector::singularize($this->name)]['active']?'Active':'Inactive');
                                echo $this->Html->link(
                                    $text,
                                    array(
                                        'controller' => 'users',
                                        'action' => 'deactivate_user',
                                        $user["User"]['id'],
                                        'full_base' => true,
                                        'admin'=>true
                                    ),
                                    array('escape' => false,'class'=>'label label-'.$class.' del-dev','title'=>__("Deactivate"))
                                );?>
                            </td>
                        </tr>
                        <tr>
                            <td>Membership:</td>
                            <td>
                                <?php  echo $this->Form->input('User.membership_id',
                                    array('options' => [0 => "Not a member"]+$memberships, 'default' => $user["User"]["membership_id"],'label'=>false));
                                    echo $this->Form->input('User.id',
                                        array('label'=>false,'type'=>'hidden','value' =>$user["User"]["id"]));
                                ;?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
    </div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <i class="fa fa-fire"></i> Candles
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body hidden"></div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Type</th>
                            <th>Time</th>
                            <th>From</th>
                            <th>Payment Id</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            foreach($user["Candle"] as $candle){?>
                            <tr>
                                <td><?php echo $candle["id"]?></td>
                                <td><?php echo ($candle["sponsor_type"])?></td>
                                <td><?php echo $candle["donation"]?> days</td>
                                <td><?php echo date("M j, Y",strtotime($candle["from"])) ?></td>
                                <td><?php echo $candle["payment_id"]?> </td>
                                <td><?php echo $this->Time->timeAgoInWords(
                                        $candle["created"],
                                        array(
                                            'format' => 'M jS, Y',
                                            'accuracy' => array('month' => 'month'),
                                            'end' => '+1 month')
                                    ); ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="panel panel-success">
            <div class="panel-heading">
                <i class="fa fa-money"></i> Payments
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body hidden"></div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Status</th>
                            <th>Paypal id</th>
                            <th>Paypal time</th>
                            <th>Platform</th>
                            <th>Environment</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $i = 0;
                        foreach($user["Payment"] as $candle){?>
                            <tr>
                                <td><?php echo $candle["id"]?></td>
                                <td><?php echo ($candle["state"])?></td>
                                <td><?php echo $candle["payment_id"]?></td>
                                <td><?php echo  $this->Time->niceShort($candle["payment_time"])?></td>
                                <td><?php echo $candle["platform"]?></td>
                                <td><?php echo $candle["environment"]?></td>
                                <td><?php echo $this->Time->niceShort($candle["created"])?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
            </div>
        </div>
    </div>
</div>
</section>

<script>
$(function(){
    $("#UserMembershipId").change(function(){
        var id = $(this).val();
        var user_id = $("#UserId").val();
        bootbox.confirm("Sure you want to modify this user membership?", function(result) {
            if(result){
                document.location.href = Admin.basePath+"admin/users/update_membership/"+user_id+"/"+id;
            }
        });
    });
});
</script>