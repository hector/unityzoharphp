<?php echo $this->element("web-header");
$hash = md5(time().rand());
?>
<div id="modal-background"></div>
<div id="modal-content">
    <label class="btn btn-success btn-block" id="label-message">Waiting for payment</label>
    <div class="top15 text-center">
        <p class="text-center">Do not close this tab/window!</p>
        <i class="fa fa-circle-o-notch fa-spin"></i>
    </div>

</div>
<div id="unity-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-title">Sign Up</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
</div>

<div class="container register">

<div class="row">
    <div class="col-lg-5 col-lg-offset-3">
            <?php echo $this->Form->create('User', array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'id' => 'register-form',
                'data-role' => 'none',
                'data-ajax' => 'false',
                'novalidate' => true,
                'inputDefaults' => array(
                    'label' => false,
                    'div' => false,
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
                )
                )); ?>
        <div class="control-group">
            <label class="control-label" for="form-field-1"><?php echo __("Email")?></label>
            <div class="controls">
                <?php echo $this->Form->input('User.email',array('class' => 'form-control input-xlarge','type'=>'text'));?>
            </div>
        </div>
        <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Password")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.password',array('class' => 'form-control input-xlarge','type'=>'password'));?>
                </div>
            </div>
<!--            <div class="control-group">-->
<!--                <label class="control-label" for="form-field-1">--><?php //echo __("Web site")?><!--</label>-->
<!--                <div class="controls">-->
<!--                    --><?php //echo $this->Form->input('User.site',array('class' => 'form-control input-xlarge','type'=>'text'));?>
<!--                </div>-->
<!--            </div>-->
            <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Name")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.name',array('class' => 'form-control input-xlarge','type'=>'text'));?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Last name")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.lastname',array('class' => 'form-control input-xlarge','type'=>'text'));?>
                    <?php if(isset($this->data["User"]["hash"])){
                        echo $this->Form->input('User.hash',array('class' => 'form-control input-xlarge','type'=>'hidden'));
                    }else{
                        echo $this->Form->input('User.hash',array('class' => 'form-control input-xlarge','type'=>'hidden','value'=>$hash));
                    }?>
                </div>
            </div>
            <hr />
            <?php /*<div>
                <input type="checkbox" id="member-check" name="membercheck" /> <label for="member-check"><em>Become a member</em></label>
                <div id="membership-container">
                    <div class="panel panel-default panel-unity top30">
                        <div class="panel-heading">Membership levels</div>
                        <ul class="list-group">
                            <?php foreach ($memberships as $m) {?>
                            <li class="list-group-item">
                                <span class="badge">$<?php echo $m["Membership"]["cost"] ?></span>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="membership" data-price="<?php echo $m["Membership"]["cost"] ?>"
                                               data-name="<?php echo $m["Membership"]["name"] ?>"
                                               value="<?php echo $m["Membership"]["id"] ?>" />
                                        <?php echo $m["Membership"]["name"] ?>
                                    </label>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
 */?>
            <div class="text-center bottom30 top30">
                <?php echo $this->Form->submit(__('Save'),array('id'=>'save-btn','class'=>'btn btn-default btn-unity btn-block','data-role'=>'none'));?>
            </div>
            <?php echo $this->Form->end();?>
        <?php /* ?>
            <form id="paypal-form" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_blank">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="hmontesinos_negocio@screen.com.ve">
                <!--            <input type="hidden" name="business" value="donation@dailyzohar.com">-->
                <input type="hidden" name="amount" value="">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="item_name" value="">
                <input type="hidden" name="rm" value="0">
                <input type="hidden" id="paypal_email" name="email" value="">
                <input type="hidden" name="item_number" value="0">
                <input type="hidden" name="notify_url" value="http://unityzohar.com/beta/payments/ipn">
                <!--            <input type="hidden" name="return" value="--><?php // echo Router::url("",true)?><!--/?invoice=--><?php //echo $hash ?><!--">-->
                <input type="hidden" name="invoice" value="<?php echo $hash ?>" />
                <?php echo $this->Form->submit(__('Save'),array('id'=>'save-btn-paypal','data-run'=>0,'class'=>'btn bottom30 hide btn-default btn-unity btn-block'));?>
            </form>
 */?>
    </div><!--/.span-->
    </div>
    </div><!--/.row-fluid-->

<?php /*
<script>
    $(function(){
        $("#membership-container").hide();
        $("#member-check").on("change",function(){
            if($(this).is(":checked")) {
                $("#membership-container").show();
                $("#save-btn").hide();
                $("#save-btn-paypal").toggleClass("hide");
            }else{
                $("#membership-container").hide();
                $("#save-btn").show();
                $("#save-btn-paypal").toggleClass("hide");
            }
        });

        $("#save-btn-paypal").click(function(e){
            if($(this).data("run")==0) {
                console.log("preventing");
                e.preventDefault();
            }else{
                return true;
            }

            $("#modal-background").fadeIn(100,function(){
                var data_= $("#register-form").serialize();
                $.ajax({
                    type:"POST",
                    dataType: 'json',
                    data:data_,
                    url:  Admin.basePath+"users/validate_user/",
                    async:false,
                    success: function(result) {
                        $("#modal-background").hide();
                        $("#register-form").find(".alert.alert-danger").remove();
                        if(result.result!=1){
                            $.each(result.result,function(key,value){
                                $("#register-form").prepend("<p class='alert-danger alert'>"+value+"</p>");
                            });
                            $("html, body").animate({ scrollTop: $("#register-form").offset().top }, 600);
                        }else{
                            console.log("pasa");
                            $("#save-btn-paypal").data("run","1");
                            $("#save-btn-paypal").trigger("click");
                        }
                    }
                });
            });
        });
        $("#paypal-form").submit(function(e){
            $("#paypal-email").val($("#UserEmail").val());
            $(".text-danger").remove();
            item =  "Unity Zohar '"+$(":radio[name='membership']:checked").data("name")+"' membership";

            if($(":radio[name='membership']:checked").length == 0){

                $("#membership-container").prepend("<h4 class='text-danger text-center'>You must choose an option</h4>");
                return false;
            }
            amount =  $(":radio[name='membership']:checked").data("price");
            $("input[name='amount']").val(amount);
            $("input[name='item_name']").val(item);
            interval = setInterval(checkPayment, 5000);
            $("#modal-content, #modal-background").show();
        });


    });
    function checkPayment() {
        var invoice = $("input[name='invoice']").val();
        $.ajax({
            type:"GET",
            dataType: 'json',
            url:  Admin.basePath+"payments/verify_invoice/"+invoice,
            success: function(result) {
                console.log(result);
                if(result.payment !=0){
                    stopInterval();
                    alert("Payment Received");
                    if(result.payment.Payment.state=="Completed"){
                        $("#label-message").html("Saving membership...");
                        $("input[name='payment_id']").val(result.payment.Payment.id);
                        $("#register-form").submit();
                    }else{
                        alert("Payment invalid");
                    }
                }
            }
        });
    }
    function handleResponse(json){
    }
    function stopInterval() {
        clearInterval(interval);
    }

    function saveCandle() {

    }
</script>*/
 ?>