<section class="content-header">
    <h1>Users <small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('User', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Username</label>
            <?php echo $this->Form->input('User.username',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Name</label>
            <?php echo $this->Form->input('User.name',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Last Name</label>
            <?php echo $this->Form->input('User.lastname',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Membership</label>
            <?php echo $this->Form->input('User.membership_id',array("options"=>["0" => '-Not a member']+$memberships,'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Active</label>
            <?php echo $this->Form->input('User.active',array("type"=>"checkbox",'class' => 'form-control'));?>
        </div>
        <?php if(in_array($this->request->data["User"]['role'],['editor','admin'])){?>
        <div class="form-group">
            <label>Role</label>
            <?php echo $this->Form->input('User.role',array("options"=>['editor'=>'Editor','admin'=>'Admin'],'class' => 'form-control'));?>
        </div>
        <?php } ?>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->