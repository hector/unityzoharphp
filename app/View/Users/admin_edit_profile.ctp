<section class="content-header">
    <h1>Profile <small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('User', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Username</label>
            <?php echo $this->Form->input('User.username',array("type"=>"text",'class' => 'form-control','disabled'=>'disabled'));?>
        </div>
        <div class="form-group">
            <label>Name</label>
            <?php echo $this->Form->input('User.name',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Last Name</label>
            <?php echo $this->Form->input('User.lastname',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <a href="<?php echo Router::url("/admin/users/change_password",true);?>">Change Password</a>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->