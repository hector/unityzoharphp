<?php echo $this->element("web-header")?>
<div id="unity-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-title">Change Password</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
</div>

<div class="container register">

<div class="row">
    <div class="col-lg-5 col-lg-offset-3">
            <?php echo $this->Form->create('User', array(
                'class' => 'form-horizontal',
                'role' => 'form',
                'data-role' => 'none',
                'data-ajax' => 'false',
                'novalidate' => true,
                'inputDefaults' => array(
                    'label' => false,
                    'div' => false,
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
                )
                )); ?>
        <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Password")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.password',array('class' => 'form-control input-xlarge','type'=>'password'));?>
                </div>
        </div>
        <div class="control-group">
            <label class="control-label" for="form-field-1"><?php echo __("Password repeat")?></label>
            <div class="controls">
                <?php echo $this->Form->input('User.password2',array('class' => 'form-control input-xlarge','type'=>'password'));?>
            </div>
        </div>
<!--            <div class="control-group">-->
<!--                <label class="control-label" for="form-field-1">--><?php //echo __("Web site")?><!--</label>-->
<!--                <div class="controls">-->
<!--                    --><?php //echo $this->Form->input('User.site',array('class' => 'form-control input-xlarge','type'=>'text'));?>
<!--                </div>-->
<!--            </div>-->
            <div class="text-center bottom30">
                <?php echo $this->Form->submit(__('Reset Password'),array('class'=>'btn btn-primary','data-role'=>'none'));?>
            </div>

            <?php echo $this->Form->end();?>
    </div><!--/.span-->
</div><!--/.row-fluid-->
</div>