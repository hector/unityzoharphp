<?php echo $this->element("web-header");
echo $this->Html->script(['admin/bootbox.min'], array('inline' => false));
echo $this->Html->css(['web/bootstrap-slider','web/miniplayer'], array('inline' => false));

if(Configure::read("debug") > 0){
    $this->Html->script(['web/membership'], array('inline' => false));
}else{
    $this->Html->script(['web/membership.min'], array('inline' => false));
}



$hash = md5(time() . rand());
?>
    <div id="modal-background"></div>
    <div id="modal-content">
        <label class="btn btn-success btn-block" id="label-message">Your payment is being processing</label>
        <div class="top15 text-center">
            <p class="text-center">Please wait</p>
            <i class="fa fa-circle-o-notch fa-spin"></i>
        </div>

    </div>
    <div id="unity-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-title">Membership</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
        </div>
    </div>
    <div class="main-content clearfix">
        <div class="container top15 bottom15">
            <div class="row">
                <div class="col-md-12">
                    <?php echo $this->Session->flash("nomembership"); ?>
                </div>
            </div>
        </div>

        <div class="container register">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <?php echo $this->Form->create('User', array(
                        'class' => 'form-horizontal',
                        'role' => 'form',
                        'id' => 'register-form',
                        'data-role' => 'none',
                        'data-ajax' => 'false',
                        'novalidate' => true,
                        'inputDefaults' => array(
                            'label' => false,
                            'div' => false,
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
                        )
                    )); ?>
                    <div>
                        <!--                <input type="checkbox" id="member-check" name="membercheck" />-->
                        <div id="membership-container">
                            <div class="panel panel-default panel-unity top30">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-6">Membership levels
                                            <small>*</small>
                                        </div>
                                        <div class="push-left col-md-6 text-right">Pay Monthly</div>
                                    </div>
                                </div>
                                <ul class="list-group">
                                    <?php foreach ($memberships as $m) { ?>
                                        <li class="list-group-item">
                                            <span class="badge">$<?php echo $m["Membership"]["cost"] ?></span>

                                            <div class="radio">
                                                <label>
                                                    <?php
                                                    $selected = "";
                                                    if (AuthComponent::user('membership_id') == $m["Membership"]["id"]) {
                                                        $selected = "checked='checked'";
                                                        $mem = $m["Membership"]["name"];
                                                    } ?>
                                                    <input type="radio" name="membership"
                                                           data-price="<?php echo $m["Membership"]["cost"] ?>"
                                                           data-name="<?php echo $m["Membership"]["name"] ?>"
                                                           value="<?php echo $m["Membership"]["id"] ?>" <?php echo $selected ?> />
                                                    <?php echo $m["Membership"]["name"] ?>
                                                </label>
                                            </div>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="text-center bottom30 top30">
                        <small class="bottom10 text-muted"><em>* All membership levels have full access to the Zohar.
                                The different levels is about your level of support to maintain, expand and improve the
                                study system</em></small>
                    </div>
                    <input type="hidden" name="invoice" value="<?php echo $hash ?>">
                    <?php echo $this->Form->end(); ?>
                </div>
                <!--/.span-->
                <div class="col-lg-6 col-md-6">
                    <div class="top30 bottom15">
                        <div id="membership-card" class="center-block">
                            <span
                                class="name"><?php echo $this->Session->read("Auth.User.name") . " " . $this->Session->read("Auth.User.lastname") ?></span>
                            <span class="level">Membership level: <em> <?php echo @$mem ?></em></span>
                        </div>
                        <?php if (AuthComponent::user('membership_id') > 0) { ?>
                            <div class="top10">
                                <a id="cancel-membership" href="<?php echo Router::url("/", true) . "users/cancel_membership" ?>"
                                   class="text-danger"><em>Cancel Membership</em></a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-5 col-md-offset-4">
                    <?php echo $this->Form->create('User', array(
                        'class' => '',
                        'role' => 'form',
                        'type' => 'file',
                        'id' =>'cc-form',
                        'novalidate' => true,
                        'inputDefaults' => array(
                            'label' => false,
                            'div' => false,
                            'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
                        )
                    )); ?>
                    <fieldset>
                            <input type="hidden"  id="concept" name="data[cc][text]" />
                            <input type="hidden"  id="amount" name="data[cc][amount]" />
                            <input type="hidden"  id="m_id" name="data[User][membership_id]" />
                            <input type="hidden"  id="prev-mem" value="<?php echo AuthComponent::user('membership_id')?>" />

                        <div class="control-group">
                            <label label-default="" class="control-label">Card Holder's Name</label>
                            <div class="controls">
                                <input type="text" class="form-control"  placeholder="First and last name" name="data[cc][card_name]" id="card-name" />
                            </div>
                        </div>
<!--                        <div class="control-group">-->
<!--                            <label label-default="" class="control-label">Card Type</label>-->
<!--                            <div class="controls">-->
<!--                                <div class="row">-->
<!--                                    <div class="col-md-12">-->
<!--                                        <select class="form-control" name="data[cc][type]">-->
<!--                                            <option value="visa">Visa</option>-->
<!--                                            <option value="mastercard">Mastercard</option>-->
<!--                                        </select>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="control-group">
                            <label label-default="" class="control-label">Card Number</label>
                            <div class="controls">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" id="card-number" value="" class="form-control" autocomplete="off" name="data[cc][number]" maxlength="16" placeholder="Only numbers">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label label-default="" class="control-label">Card Expiry Date</label>
                            <div class="controls">
                                <div class="row">
                                    <div class="col-md-9">
                                        <select class="form-control" name="data[cc][expire_month]">
                                            <option value="01">January</option>
                                            <option value="02">February</option>
                                            <option value="03">March</option>
                                            <option value="04">April</option>
                                            <option value="05">May</option>
                                            <option value="06">June</option>
                                            <option value="07">July</option>
                                            <option value="08">August</option>
                                            <option value="09">September</option>
                                            <option value="10">October</option>
                                            <option value="11">November</option>
                                            <option value="12">December</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select class="form-control" name="data[cc][expire_year]">
                                            <?php
                                                for($i=(int)date("Y");$i<(int)date("Y",strtotime("+10 years"));$i++){?>
                                                <option value="<?php echo substr($i,-2)?>"><?php echo $i?></option>
                                            <?php }?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label label-default="" class="control-label">Card CVV</label>
                            <div class="controls">
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" id="card-cvv" maxlength="4" class="form-control col-md-3" name="data[cc][cvv]">
                                    </div>
                                    <div class="col-md-8"></div>
                                </div>
                            </div>
                        </div>
                        <div class="control-group">
                            <label label-default="" class="control-label"></label>
                            <div class="controls">
                                <button type="submit" class="btn btn-default btn-unity btn-block">Save Membership</button>

                            </div>
                        </div>
                    </fieldset>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
        <!--/.row-fluid-->
    </div>
<?php /*
<script>
    $(function(){
        $("#paypal-form").submit(function(e){
            $("#paypal-email").val($("#UserEmail").val());
            $(".text-danger").remove();
            item =  "Unity Zohar '"+$(":radio[name='membership']:checked").data("name")+"' membership";

            if($(":radio[name='membership']:checked").length == 0){

                $("#membership-container").prepend("<h4 class='text-danger text-center'>You must choose an option</h4>");
                return false;
            }
            if($(":radio[name='membership']:checked").val()=='<?php echo AuthComponent::user('membership_id')?>'){
                $("#membership-container").prepend("<h4 class='text-danger text-center'>You already have this membership</h4>");
                return false;
            }
            amount =  $(":radio[name='membership']:checked").data("price");
            $("input[name='amount']").val(amount);
            $("input[name='a3']").val(amount);
            $("input[name='item_name']").val(item);
            //interval = setInterval(checkPayment, 5000);
            //$("#modal-content, #modal-background").show();

            saveMembership();
        });
//        $("#cancel-payment").click(function(){
//            document.location.href = document.location.href;
//        });

        $(":radio[name='membership']").change(function(){
            var lvl = $(this).parent().contents()
                .filter(function() {
                    return this.nodeType === 3; //Node.TEXT_NODE
                });
            $("#membership-card span.level em").html($.trim(lvl.text()));
        });


    });
    function saveMembership() {
        data_ = $("#register-form").serialize();
        $.ajax({
            type:"POST",
            dataType: 'json',
            data : data_,
            url:  Admin.basePath+"users/membership/",
            success: function(result) {
            }
        });
    }

    /*function checkPayment() {
        var invoice = $("input[name='invoice']").val();
        $.ajax({
            type:"GET",
            dataType: 'json',
            url:  Admin.basePath+"payments/verify_invoice/"+invoice,
            success: function(result) {
                console.log(result);
                if(result.payment !=0){
                    stopInterval();
                    alert("Payment Received");
                    if(result.payment.Payment.state=="Completed"){
                        $("#label-message").html("Saving membership...");
                        $("input[name='payment_id']").val(result.payment.Payment.id);
                        $("#register-form").submit();
                    }else{
                        alert("Payment invalid");
                    }
                }
            }
        });
    }
    function handleResponse(json){
    }
    function stopInterval() {
        clearInterval(interval);
    }

    function saveCandle() {

    }
</script>

 */ ?>