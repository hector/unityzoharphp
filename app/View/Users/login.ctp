<?php echo $this->element("web-header")?>
<div id="unity-container" class="bottom15">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                 <h1 class="text-center page-title"><?php echo $page_title  ?></h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="">Please Sign In</h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->Session->flash('auth'); ?>
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->Form->create('User',array('data-ajax' => 'false')) ?>
                    <fieldset>
                        <div class="form-group">
                            <?php echo $this->Form->input('username',array('class'=>'form-control','placeholder'=>__("Email / Username"),'label'=>false));?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>__("Password"),'label'=>false));?>
                        </div>
                        <button data-role="none" class="btn btn-lg btn-success btn-block">
                            <i class="icon-key"></i>
                            <?php echo __("Login")?>
                        </button>
                        <br>
                        <a href="<?php echo Router::url("/",true)?>users/registerapp">Doesn't have an account?</a>
                        <br>
                        <a href="<?php echo Router::url("/",true)?>users/forgot_password">Forgot password?</a>
                    </fieldset>
                    <?php echo $this->Form->end(); ?>
                </div>

            </div>
            <div class="center-block">
                <?php echo $this->Html->image('/img/smg-logo.png',array('class'=>'center-block'));?>
            </div>
        </div>
    </div>
</div>