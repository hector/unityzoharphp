<?php echo $this->element("web-header")?>
<div id="unity-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-title text-center">Account</h1>
            </div>
        </div>
    </div>
</div>
<div class="container top15 bottom15">
    <div class="row">
        <div class="col-md-12">
            <?php echo $this->Session->flash("middle"); ?>
        </div>
    </div>
</div>
<div class="container register">
<div class="row">
    <div class="row">
        <div class="col-lg-6 col-lg-offset-3 text-center top15 bottom15">
            <?php /* if(AuthComponent::user('id') && AuthComponent::user('membership_id')>0){?>
                <div class="top30 bottom15">
                    <div id="membership-card" class="center-block">
                        <span class="name"><?php echo $this->Session->read("Auth.User.name")." ".$this->Session->read("Auth.User.lastname") ?></span>
                        <span class="level">Membership level: <em><?php echo $memberships[AuthComponent::user('membership_id')] ?></em></span>
                    </div>
                </div>
                <div class="list-group text-left">
                    <a class="list-group-item" href="<?php echo Router::url("/",true)?>users/membership"><i class="fa fa-users"></i> Update membership</a>
                    <a href="<?php echo Router::url("/",true)?>users/update_profile" class="list-group-item"><i class="fa fa-user"></i> Update Profile</a>
                    <a class="list-group-item" role="button" href="<?php echo Router::url("/",true)?>users/payments"><i class="fa fa-money"> </i> Payments</a>
                    <a  class="list-group-item" href="<?php echo Router::url("/",true)?>users/logout"><i class="fa fa-sign-out"></i> Logout</a>
                </div>

            <?php }else{ */?>
                <div class="list-group">
                    <div class="list-group text-left">
<!--                        <a class="list-group-item" href="--><?php //echo Router::url("/",true)?><!--users/membership"><i class="fa fa-users text-success"></i> Become a member to have full access to the Zohar study by book</a>-->
                        <a href="<?php echo Router::url("/",true)?>users/update_profile" class="list-group-item"><i class="fa fa-user"></i> Update Profile</a>
                        <a class="list-group-item" role="button" href="<?php echo Router::url("/",true)?>users/payments"><i class="fa fa-money"> </i> Payments</a>
                        <a  class="list-group-item" href="<?php echo Router::url("/",true)?>users/logout"><i class="fa fa-sign-out"></i> Logout</a>
                    </div>
                </div>
            <?php //} ?>
<!--            <a href="--><?php //echo Router::url("/",true)?><!--users/change_password">Change Password</a>-->
        </div>
    </div>
    <?php /*
    <div class="col-lg-6 col-lg-offset-3">
            <?php echo $this->Form->create('User', array(
                'class' => 'form-horizontal',
                'data-role' => 'none',
                'data-ajax' => 'false',
                'type' => 'file',
                'novalidate' => true,
                'inputDefaults' => array(
                    'label' => false,
                    'div' => false,
                    'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
                )
                )); ?>
            <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Email")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.email',array('class' => 'form-control input-xlarge','type'=>'text','readonly'=>'readonly'));?>
                </div>
            </div>
        <?php echo $this->Form->input('User.site',array('class' => 'form-control input-xlarge','type'=>'hidden'));?>
            <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Name")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.name',array('class' => 'form-control input-xlarge','type'=>'text'));?>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="form-field-1"><?php echo __("Last name")?></label>
                <div class="controls">
                    <?php echo $this->Form->input('User.lastname',array('class' => 'form-control input-xlarge','type'=>'text'));?>
                </div>
            </div>
            <div class="text-center bottom30">
                <?php echo $this->Form->submit(__('Save'),array('class'=>'btn btn-primary','data-role'=>'none'));?>
            </div>

            <?php echo $this->Form->end();?>
    </div><!--/.span-->
  */?>
</div><!--/.row-fluid-->
</div>
<script>
    <?php if(isset($this->request->query["invoice"])){?>
        var interval = setInterval(checkPayment, 2000);
        function checkPayment() {
            var invoice = '<?php echo $this->request->query["invoice"]?>';
            $.ajax({
                type:"GET",
                dataType: 'json',
                url:  Admin.basePath+"payments/verify_invoice/"+invoice,
                success: function(result) {
                    console.log(result);
                    if(result.payment !=0){
                        clearInterval(interval);
                        if(result.payment.Payment.state=="Completed"){
                            alert("Payment Received successfully");
                        }else{
                            alert("Payment invalid");
                        }
                        document.location.href = document.location.href;
                    }
                }
            });
        }
<?php }?>
</script>