<section class="content-header">
    <h1>Change password<small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('User', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Username</label>
            <?php echo $this->Form->input('User.username',array("type"=>"text",'class' => 'form-control','disabled'=>'disabled'));?>
        </div>
        <div class="form-group">
            <label>Password</label>
            <?php echo $this->Form->input('User.password',array("type"=>"password",'class' => 'form-control','value'=>''));?>
        </div>
        <div class="form-group">
            <label>Repeat Password</label>
            <?php echo $this->Form->input('User.confirm_password',array("type"=>"password",'class' => 'form-control','value'=>''));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->