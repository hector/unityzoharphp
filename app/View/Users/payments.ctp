<?php echo $this->element("web-header")?>
<div id="unity-container">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-title">Payments history</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
    </div>
</div>
<div class="container">
            <?php foreach($payments as $row){?>
                <div class="row">
                    <div class="col-lg-8 col-lg-push-2">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <i class="fa fa-paypal"></i> Payment Details <span class="text-"><em>#<?php echo $row["Payment"]["id"] ?></em></span>
                            </div>
                            <!-- /.panel-heading -->
                            <div class="panel-body hidden"></div>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <td>Created:</td>
                                        <td>
                                            <?php
                                                $p = 'laptop';
                                                ?>
                                            <i class="text-muted fa fa-<?php echo $p ?>"></i>
                                            <?php echo $this->Time->timeAgoInWords(
                                                $row["Payment"]["created"],
                                                array(
                                                    'format' => 'F jS, Y',
                                                    'accuracy' => array('month' => 'month'),
                                                    'end' => '+1 month')
                                            );
                                            ?>
                                            (<?php echo date("m/d/2014 h:i a",strtotime($row["Payment"]["created"])) ?>)
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Paypal Id:</td><td><?php echo $row["Payment"]["payment_id"]?></td>
                                    </tr>
                                    <tr>
                                        <td>Status:</td>
                                        <td><?php
                                            $class = ($row["Payment"]['state']?'success':'danger');
                                            $text = ($row["Payment"]['state']);
                                            ?>
                                            <span class="label label-<?php echo $class ?>"><?php echo $text ?></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <?php  if($row["Candle"]["id"]){?>
                            <ul class="list-group">
                                <li class="list-group-item list-group-item-success">
                                    <i class="fa fa-fire"></i> Candle Details
                                </li>
                            </ul>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Type</th>
                                    <th>Time</th>
                                    <th>From</th>
                                    <th>Payment Id</th>
                                    <th>Created</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><?php echo $row["Candle"]["id"]?></td>
                                    <td><?php echo ($row["Candle"]["sponsor_type"]=="wish")?'Wish':"Refua"?></td>
                                    <td><?php echo $row["Candle"]["donation"]?> days</td>
                                    <td><?php echo date("M j, Y",strtotime($row["Candle"]["from"])) ?></td>
                                    <td><?php echo $row["Candle"]["payment_id"]?> </td>
                                    <td><?php echo $this->Time->timeAgoInWords(
                                            $row["Candle"]["created"],
                                            array(
                                                'format' => 'M jS, Y',
                                                'accuracy' => array('month' => 'month'),
                                                'end' => '+1 month')
                                        ); ?>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <hr />
            <?php } ?>

</div>