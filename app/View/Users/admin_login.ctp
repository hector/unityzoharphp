<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1 class="text-center" style="color:#F5F5F5">Unity Zohar</h1>
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Please Sign In</h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->Session->flash('auth'); ?>
                    <?php echo $this->Session->flash(); ?>
                    <?php echo $this->Form->create('User') ?>
                    <fieldset>
                        <div class="form-group">
                            <?php echo $this->Form->input('username',array('class'=>'form-control','placeholder'=>__("Username"),'label'=>false));?>
                        </div>
                        <div class="form-group">
                            <?php echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>__("Password"),'label'=>false));?>
                        </div>
                        <button class="btn btn-lg btn-success btn-block">
                            <i class="icon-key"></i>
                            <?php echo __("Login")?>
                        </button>
                    </fieldset>
                    <?php echo $this->Form->end(); ?>
                </div>

            </div>
            <div class="center-block">
                <?php echo $this->Html->image('/img/smg-logo.png',array('class'=>'center-block'));?>
            </div>
        </div>
    </div>
</div>