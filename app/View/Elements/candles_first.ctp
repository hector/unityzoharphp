<?php
//if(isset($pre)){
//    $candles = $this->requestAction('/pages/get_candles/limit:3');
//}else{
//    $candles = $this->requestAction('/pages/get_candles/');
//}
//?>
<div class="row">

        <?php
            $candle = array_pop($candles);
            if(!empty($candle)){
        ?>
            <div class="col-md-8 col-md-offset-2  top17  col-xs-12 bottom15">
                <div class="panel panel-default panel-healing no-margin">
                   <?php  if(!isset($pre)){?> <div class="panel-heading"><strong>Healing</strong></div><?php }?>
                    <ul class="list-group healing-list">
                        <?php
                        $classes ="";
                            if($candle["Candle"]["hebrew"]=="1"){$classes ="rtl-text heb-text ";}
                            if($candle["Candle"]["sponsor_type"]=="refua"){
                                $classes .="refua";
                            }elseif($candle["Candle"]["sponsor_type"]=="bless"){
                                $classes .="bless";
                            }else{
                                $classes .="wish";
                            }

                            ?>
                            <li class="list-group-item <?php echo $classes ?>">
                                <?php echo $candle["Candle"]["name"]." ".$candle["Candle"]["gender"]." ".$candle["Candle"]["parent_name"]?>
                            </li>
                    </ul>
                </div>
            </div>
        <?php } ?>


        <?php
        $candle = array_pop($candles);
        if(!empty($candle)){
        ?>
            <div class="col-md-8 col-md-offset-2  col-xs-12 bottom15">
                <div class="panel panel-default panel-healing no-margin">
                    <?php  if(!isset($pre)){?> <div class="panel-heading"><strong>Soul Elevations</strong></div><?php }?>
                    <ul class="list-group healing-list">
                        <?php
                            $classes ="";
                            if($candle["Candle"]["hebrew"]=="1"){$classes ="rtl-text heb-text ";}
                            if($candle["Candle"]["sponsor_type"]=="refua"){
                                $classes .="refua";
                            }elseif($candle["Candle"]["sponsor_type"]=="bless"){
                                $classes .="bless";
                            }else{
                                $classes .="wish";
                            }
                            ?>
                            <li class="list-group-item <?php echo $classes ?>">
                                <?php echo $candle["Candle"]["name"]." ".$candle["Candle"]["gender"]." ".$candle["Candle"]["parent_name"]?>
                            </li>
                    </ul>
                </div>
            </div>
        <?php } ?>


        <?php
        $candle = array_pop($candles);
        if(!empty($candle)){
        ?>
            <div class="col-md-8 col-md-offset-2  bottom15 col-xs-12">
                <div class="panel panel-default panel-healing no-margin">
                    <?php  if(!isset($pre)){?> <div class="panel-heading"><strong>Success in life</strong></div><?php }?>
                    <ul class="list-group healing-list">
                        <?php
                            $classes ="";
                            if($candle["Candle"]["hebrew"]=="1"){$classes ="rtl-text heb-text ";}
                            if($candle["Candle"]["sponsor_type"]=="refua"){
                                $classes .="refua";
                            }elseif($candle["Candle"]["sponsor_type"]=="bless"){
                                $classes .="bless";
                            }else{
                                $classes .="wish";
                            }
                            ?>
                            <li class="list-group-item <?php echo $classes ?>">
                                <?php echo $candle["Candle"]["name"]." ".$candle["Candle"]["gender"]." ".$candle["Candle"]["parent_name"]?>
                            </li>
                    </ul>
                </div>
            </div>
        <?php } ?>

</div>