<?php $this->Paginator->options(array('url' => array('search'=>@$this->data["search"])));?>
<aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="search" class="form-control" value="<?php echo @$this->data['search'] ?>" placeholder="Search..."/>
                    <span class="input-group-btn">
                        <button type='submit' class="btn btn-flat"><i class="fa fa-search"></i></button>
                    </span>
            </div>
        </form>
        <ul class="sidebar-menu">
            <li>
                <a href="<?php echo Router::url("/admin",true); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <?php if(in_array($this->Session->read('Auth.User.role'),['admin','editor'])){?>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bookmark-o"></i>
                    <span>Zohar</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo Router::url("/admin/parashot/index",true); ?>">
                            <i class="fa fa-angle-double-right"></i> <span>Parashot</span>
                        </a>
                    </li>
<!--                    <li>-->
<!--                        <a href="--><?php //echo Router::url("/admin/articles/index",true); ?><!--">-->
<!--                            <i class="fa fa-angle-double-right"></i> <span>Articles</span>-->
<!--                        </a>-->
<!--                    </li>-->
                    <li>
                        <a href="<?php echo Router::url("/admin/parashot/order",true); ?>">
                            <i class="fa fa-angle-double-right"></i> <span>Parashot Order</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url("/admin/zohar/index",true); ?>">
                            <i class="fa fa-angle-double-right"></i> <span>Zohar's Paragraph</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php } if(in_array($this->Session->read('Auth.User.role'),['admin'])){ ?>
            <li>
                <a href="<?php echo Router::url("/admin/bible_books/index",true); ?>">
                    <i class="fa fa-file-text"></i> <span>Bible Books</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Router::url("/admin/bible/index",true); ?>">
                    <i class="fa fa-file-text-o"></i> <span>Bible</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Router::url("/admin/unity_text/index",true); ?>">
                    <i class="fa fa-font"></i> <span>Unity Texts</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Router::url("/admin/languages",true); ?>">
                    <i class="fa fa-language"></i> <span>Languages</span>
                </a>
            </li>
            <?php } ?>
            <?php if(in_array($this->Session->read('Auth.User.role'),['admin','editor'])){?>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-mobile-phone"></i>
                        <span>App</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li>
                            <a href="<?php echo Router::url("/admin/translation_strings",true); ?>">
                                <i class="fa fa-angle-double-right"></i> <span>Strings</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url("/admin/app_translations",true); ?>">
                                <i class="fa fa-angle-double-right"></i> <span>Translations</span>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo Router::url("/admin/app_translations/how_it_works",true); ?>">
                                <i class="fa fa-angle-double-right"></i> <span>How it works</span>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php } ?>
            <?php if(in_array($this->Session->read('Auth.User.role'),['admin'])){?>
            <li>
                <a href="<?php echo Router::url("/admin/candles/index",true); ?>">
                    <i class="fa fa-fire"></i> <span>Candles</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-cc"></i>
                    <span>Payments</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo Router::url("/admin/payments/index",true); ?>">
                            <i class="fa fa-angle-double-right"></i> <span>All</span>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo Router::url("/admin/membership_agreements/",true); ?>">
                            <i class="fa fa-angle-double-right"></i> <span>Memberships</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="<?php echo Router::url("/admin/payments/content",true); ?>">
                    <i class="fa fa-money"></i> <span>Payments values</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Router::url("/admin/memberships/index",true); ?>">
                    <i class="fa fa-group"></i> <span>Memberships Values</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Router::url("/admin/users",true); ?>">
                    <i class="fa fa-user"></i> <span>Users</span>
                </a>
            </li>
            <li>
                <a href="<?php echo Router::url("/admin/?clear=1",true); ?>">
                    <i class="fa fa-eraser"></i> <span>Clear Cache</span>
                </a>
            </li>
            <?php } ?>
        </ul>
    </section>
</aside>