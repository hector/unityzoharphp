<?php
$pray = $this->requestAction('/pages/get_text/PRAYTOP');
?>
<button class="btn center-block" id="meditation-button"> Read or Scan once for every session <span id="more-meditation">(+)</span></button>
<div id="meditation-text" class="well">
    <br />
    <?php echo $pray["UnityText"]["text"] ?>
</div>