<?php
$visitors = $this->requestAction('/pages/get_visitors/pinchas');
$class = "PinchasCycle";
?>
<div class="well">
    <?php foreach($visitors as $visitor){
        if($visitor[$class]["country"]){?>
            <span class="small">
                                <?php echo $this->Html->image("flags/".strtolower($visitor[$class]["country_iso"])."_16.png")?>
                                <?php echo  $visitor[$class]["city"]."-".$visitor[$class]["country"]?>
                            </span>
        <?php }} ?>
</div>