<header class="header">
<a href="<?php echo Router::url("/admin",true); ?>" class="logo">
    Unity Zohar
</a>
<nav class="navbar navbar-static-top" role="navigation">
<a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
    <span class="sr-only">Toggle navigation</span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
</a>
<div class="navbar-right">
<ul class="nav navbar-nav">
    <li><a href="<?php echo Router::url("/",true) ?>" target="_blank">Go to site</a></li>
<li class="dropdown user user-menu">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="glyphicon glyphicon-user"></i>
        <span><?php echo $this->Session->read("Auth.User.name")." ".$this->Session->read("Auth.User.lastname") ?> <i class="caret"></i></span>
    </a>
    <ul class="dropdown-menu">
        <li class="user-header bg-orange">
            <p>
                <?php echo $this->Session->read("Auth.User.name")." ".$this->Session->read("Auth.User.lastname") ?> - <?php echo $this->Session->read("Auth.User.role")?>
                <small>Member since <?php echo $this->Session->read("Auth.User.created") ?></small>
            </p>
        </li>
        <li class="user-footer">
            <div class="pull-left">
                <a href="<?php echo Router::url("/admin/users/edit_profile",true);?>" class="btn btn-default btn-flat">Profile</a>
            </div>
            <div class="pull-right">
                <a href="<?php echo Router::url("/admin/users/logout",true) ?>" class="btn btn-default btn-flat">Sign out</a>
            </div>
        </li>
    </ul>
</li>
</ul>
</div>
</nav>
</header>