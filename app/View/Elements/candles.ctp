<?php
//if(isset($pre)){
//    $candles = $this->requestAction('/pages/get_candles/limit:3');
//}else{
//    $candles = $this->requestAction('/pages/get_candles/');
//}
//?>
<div class="row">
    <div class="col-md-4 col-xs-12 bottom15 top17">
        <?php if(isset($candles["refua"]) && count($candles["refua"])>0){?>
        <div class="panel panel-default panel-healing">
           <?php  if(!isset($pre)){?> <div class="panel-heading"><strong>Healing</strong></div><?php }?>
            <ul class="list-group healing-list">
                <?php foreach ($candles["refua"] as $candle) {
                    $classes ="";
                    if($candle["Candle"]["hebrew"]=="1"){$classes ="rtl-text heb-text ";}
                    if($candle["Candle"]["sponsor_type"]=="refua"){
                        $classes .=" refua";
                    }elseif($candle["Candle"]["sponsor_type"]=="bless"){
                        $classes .="bless";
                    }else{
                        $classes .="wish";
                    }

                    ?>
                    <li class="list-group-item <?php echo $classes ?>">
                        <?php echo $candle["Candle"]["name"]." ".$candle["Candle"]["gender"]." ".$candle["Candle"]["parent_name"]?>
                    </li>
                <?php  }?>
            </ul>
        </div>
        <?php } ?>
    </div>
    <div class="col-md-4 col-xs-12 bottom15 top17 dddd">
        <?php if(isset($candles["bless"]) &&  count($candles["bless"])>0){?>
        <div class="panel panel-default panel-healing 2">
            <?php  if(!isset($pre)){?> <div class="panel-heading"><strong>Soul Elevations</strong></div><?php }?>
            <ul class="list-group healing-list">
                <?php foreach ($candles["bless"] as $candle) {
                    $classes ="";
                    if($candle["Candle"]["hebrew"]=="1"){$classes ="rtl-text heb-text ";}
                    if($candle["Candle"]["sponsor_type"]=="refua"){
                        $classes .="refua";
                    }elseif($candle["Candle"]["sponsor_type"]=="bless"){
                        $classes .="bless";
                    }else{
                        $classes .="wish";
                    }
                    ?>
                    <li class="list-group-item <?php echo $classes ?>">
                        <?php echo $candle["Candle"]["name"]." ".$candle["Candle"]["gender"]." ".$candle["Candle"]["parent_name"]?>
                    </li>
                <?php  }?>
            </ul>
        </div>
        <?php } ?>
    </div>
    <div class="col-md-4 col-xs-12 bottom15 top17">
        <?php if(isset($candles["wish"]) && count($candles["wish"])>0){?>
        <div class="panel panel-default panel-healing">
            <?php  if(!isset($pre)){?> <div class="panel-heading"><strong>Success in life</strong></div><?php }?>
            <ul class="list-group healing-list">
                <?php foreach ($candles["wish"] as $candle) {
                    $classes ="";
                    if($candle["Candle"]["hebrew"]=="1"){$classes ="rtl-text heb-text ";}
                    if($candle["Candle"]["sponsor_type"]=="refua"){
                        $classes .="refua";
                    }elseif($candle["Candle"]["sponsor_type"]=="bless"){
                        $classes .="bless";
                    }else{
                        $classes .="wish";
                    }
                    ?>
                    <li class="list-group-item <?php echo $classes ?>">
                        <?php echo $candle["Candle"]["name"]." ".$candle["Candle"]["gender"]." ".$candle["Candle"]["parent_name"]?>
                    </li>
                <?php  }?>
            </ul>
        </div>
    </div>
    <?php } ?>
</div>