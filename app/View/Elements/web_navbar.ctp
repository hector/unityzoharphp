<!--<div class="text-center alert alert-warning" style="border:0 none;border-radius: 0;margin: 0">-->
<!--    <em>This website is in beta, but you can still use it and give us your feedback to <a href="mailto:zion@dailyzohar.com?Subject=Mail%20from%20web">zion@dailyzohar.com</a>. Your support is important to complete this and all the Unity Zohar projects.</em>-->
<!--</div>-->
<div class="navbar navbar-default center" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" data-role="none" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="collapse navbar-collapse">
            <ul id="main-navbar" class="nav navbar-nav list-inline center-block text-center">
                <li><a href="http://dailyzohar.com" target="_blank">Daily Zohar</a></li>
                <li class="active"><a href="http://www.unityzohar.com/" target="_blank">Unity Zohar</a></li>
                <li><a href="http://dailyzohar.com/tzadikim" target="_blank">Tzadikim</a></li>
                <li><a href="http://zoharvoice.com/" target="_blank">Zohar Voice</a></li>
                <li><a href="http://zoharmap.com/" target="_blank">Zohar Map</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>