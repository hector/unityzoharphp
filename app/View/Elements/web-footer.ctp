<div class="container-fluid" id="footer">
    <div class="container">
        <div class="row top10">
            <div class="col-md-7">
                <ul class="list-inline dz-links">
<!--                    <li>DAILY ZOHAR LINKS:</li>-->
<!--                    <li><a href="">Holidays</a> <span>|</span></li>-->
<!--                    <li><a href="">Studies</a> <span>|</span></li>-->
<!--                    <li><a href="">Books</a> <span>|</span></li>-->
<!--                    <li><a href="">Tools</a> <span>|</span></li>-->
<!--                    <li><a href="">Audio</a> <span>|</span></li>-->
<!--                    <li><a href="">Videos</a></li>-->
                </ul>

            </div>
            <div class="col-md-5 text-right">
<!--                <ul class="list-inline dz-share">-->
<!--                    <li class="valign-middle">SHARE US:</li>-->
<!--                    <li><a href="#" class="social facebook">Facebook</a></li>-->
<!--                    <li><a href="#" class="social twitter">Twitter</a></li>-->
<!--                    <li><a href="#" class="social gplus">Google</a></li>-->
<!--                </ul>-->

            </div>
        </div>
        <div class="row top30 bottom30">
            <div class="col-md-12">
                <h3 class="spread text-center">
                    "Spreading The Light and Protection of The Zohar to All People"
                </h3>
            </div>
        </div>
        <div class="row top10">
            <div class="col-md-7 mail-link col-xs-7">
                <i class="fa fa-envelope"></i> Email comments and suggestions to: <a href="mailto:info@unityzohar.com?Subject=Mail%20from%20web">info@unityzohar.com</a>
            </div>
            <div class="col-md-5 text-right mail-link col-xs-5">
                &copy; <?php echo date("Y") ?> - <a style="color: #FFF" href="http://dailyzohar.com" target="_blank">DailyZohar.com</a> by <a href="http://screenmediagroup.com" target="_blank"><img src="<?php echo Router::url("/",true)?>img/smg-logo.png" /></a>
            </div>
        </div>
    </div>

</div>