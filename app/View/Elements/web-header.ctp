<div class="container header-banner">
    <h1 id="logo"><img src="<?php echo Router::url("/",true)?>img/logo.png">Unity Zohar</h1>
    <nav id="site-nav">
        <ul class="list-inline text-center">
            <li><a href="<?php echo Router::url("/",true)?>">Home</a></li>
            <li><a href="<?php echo Router::url("/",true)?>unity-zohar">Unity Zohar</a></li>
            <li><a href="<?php echo Router::url("/",true)?>unity-pinchas">Unity Pinchas</a></li>
            <li><a href="<?php echo Router::url("/",true)?>unity-tikunim">Unity Tikunim</a></li>
            <li><a href="<?php echo Router::url("/",true)?>unity-psalms">Unity Psalms</a></li>
            <li><a href="<?php echo Router::url("/",true)?>zohar-books/">Zohar by Books</a></li>
            <li><a href="<?php echo Router::url("/",true)?>how-to-use">How does it work</a></li>
            <?php if(AuthComponent::user('id') && AuthComponent::user('role')=='userapp'){?>
            <li><a href="<?php echo Router::url("/",true)?>users/appaccount"><?php echo __("My account") ?></a>,
                <a href="<?php echo Router::url("/",true)?>users/logout"><small>logout</small></a>
            <?php }else{?>
            <li><a href="<?php echo Router::url("/",true)?>users/login">Sign In</a></li>
            <?php } ?>
        </ul>
    </nav>
</div>