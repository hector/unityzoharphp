<div class="container-fluid app-banner">
    <div class="container">
        <div class="row">
            <div class="col-md-7 col-md-offset-0">
                <h2 id="banner-head"><img src="<?php echo Router::url("/",true)?>img/logo.png">The Unity Zohar</h2>
                <p class="text-center">
                   Zohar new amazing app
                </p>
                <div class="row top30">
                    <div class="col-md-6 text-center col-md-offset-3">
                        <a href="https://play.google.com/store/apps/details?id=unityzohar.unityzohar" target="_blank">
                            <img alt="Android app on Google Play"
                                 src="https://developer.android.com/images/brand/en_app_rgb_wo_45.png" />
                        </a>
                        <a href="https://itunes.apple.com/us/app/unity-zohar-app/id1025094640" target="_blank">
                            <img src="<?php echo Router::url("/",true)?>img/istore-button.svg" style="width: 151px;" /></a>
                        <br />
                        <br />

                        <a class="btn btn-primary" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=PNNGU4DEVNV3S">
                            <i class="fa fa-paypal"></i> Support the Zohar projects
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="top30 bottom30 phones-container hidden-xs">
                    <img style="height:280px" src="<?php echo Router::url("/",true)?>img/phones.png">
                </div>
            </div>
        </div>
    </div>
</div>