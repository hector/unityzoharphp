<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');

/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
    public $helpers = array(
        'Html',
        'Form',
        'Session',
        'Js',
    );

    public function js() {
        $shop = array();
        $shop['basePath'] = Router::url('/',true);

        if($this->params['admin'] == true){
            $action = str_replace('admin_','',$this->params['action']);
        }else{
            $action = $this->params['action'];
        }

        $shop['params'] = array(
            'controller' => $this->params['controller'],
            'action' => $action,
            'prefix' => $this->params['prefix'],
        );
        return $this->Html->scriptBlock('var Admin = ' . $this->Js->object($shop) . ';');
    }

    function addOrdinalNumberSuffix($num) {
        if (!in_array(($num % 100),array(11,12,13))){
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:  return $num.'st';
                case 2:  return $num.'nd';
                case 3:  return $num.'rd';
            }
        }
        return $num.'th';
    }

function writeComments($comments){
    foreach($comments as $comment){
        $clas = "";
        if($comment["Comment"]["parent_id"]!="0"){
            $clas = "response";
        }
        ?>
        <?php if($comment["Comment"]["approved"] || $comment["Comment"]["user_id"]==$this->Session->read('Auth.User.id')){?>
            <div class="comment-container clearfix <?php echo $clas?>">
                <label><?php echo $comment["User"]["nombres"]. " " .$comment["User"]["apellidos"]?> /
                    <?php echo CakeTime::niceShort($comment["Comment"]["created"]);?>
                </label>
                <p class="comment">
                    <?php echo $comment["Comment"]["comment"] ?>
                </p>
                <?php if(!$comment["Comment"]["approved"]){?>
                    <span class="text-danger">Not approved</span>
                <?php }?>
                <a href="#" class="pull-right reply" data-user-comment="<?php echo  $comment["User"]["nombres"]." ".$comment["User"]["apellidos"]?>" data-comment="<?php echo  $comment["Comment"]["id"]?>">Reply</a>
            </div>

            <?php if(!empty($comment["children"])){
                $this->writeComments($comment["children"]);
            } ?>
        <?php }?>
    <?php }?>

<?php }

}
