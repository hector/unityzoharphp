<section class="content-header">
    <h1>Parasha <small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('Parasha', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Parasha</label>
            <?php echo $this->Form->input('Parasha.name',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Hebrew Name</label>
            <?php echo $this->Form->input('Parasha.name_hebrew',array("type"=>"text",'class' => 'form-control zohar-text-no-bg'));?>
        </div>
        <div class="form-group">
            <label>Active</label>
            <?php echo $this->Form->input('Parasha.active',array("type"=>"checkbox",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Audio text</label>
            <?php echo $this->Form->input('Parasha.audio_name',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->