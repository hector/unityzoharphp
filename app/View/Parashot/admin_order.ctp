<?php
echo $this->Html->script(array('admin/jquery.nestable','admin/jquery.bootstrap-growl.min'), array('inline' => false));
?>
<section class="content-header">
    <h1>Parashot <small><i class="fa fa-angle-double-right"></i> Order</small></h1>
</section>
<section class="content">
<div class="row">
    <div class="col-md-8">
        <p class="text-muted"><em>Drag items to modify order</em></p>
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-push-2">
        <div class="dd">
            <ol class="dd-list">
                <?php foreach($rows as $row){?>
                <li class="dd-item" data-id="<?php echo $row["Parasha"]["id"]?>">
                    <div class="dd-handle"><?php echo $row["Parasha"]["name"]?></div>
                </li>
                <?php } ?>
            </ol>
        </div>
    </div>
</div>
<div class="row">
    <hr />
    <div class="col-md-8 text-center col-md-push-2">
        <?php echo $this->Form->button(__('<i class="fa fa-download"></i> Save'),array('class'=>'btn btn-primary','id'=>'save','data-loading-text'=>"<i class='fa fa-clock-o'></i> Saving...","escape"=>false));?>
    </div>
</div>
</section>
<script>
    $(function(){
        $('.dd').nestable({ maxDepth:1 }).on('change',save);
        $("#save").click(function(){
            $(this).button('loading');
            save();
        });
    });
    function save(){
        var _data = $('.dd').nestable('serialize');
        $.ajax({
            type: "POST",
            url: Admin.basePath+'admin/parashot/save_order/',
            data: {data:_data},
            success: function(data){
                if(data.result == "saved"){
                    $.bootstrapGrowl("Order saved.", { type: 'success' });
                }else{
                    $.bootstrapGrowl("Error, order cannot be saved.", { type: 'danger' });
                }
                $("#save").button('reset');

            },
            dataType: 'json'
        });
    }
</script>