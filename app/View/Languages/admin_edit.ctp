<section class="content-header">
    <h1>Language <small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('Language', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Language</label>
            <?php echo $this->Form->input('Language.language',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Abbrev</label> <small><em>Please use the <a href="https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes">ISO 639-1</a> specification (only 2 letters)</em></small>
            <?php echo $this->Form->input('Language.abbreviation',array("type"=>"text",'class' => 'form-control'));?>
        </div>
        <div class="form-group">
            <label>Use in app?</label>
            <?php echo $this->Form->checkbox('Language.apply_app', array(
                'value' => '1'
            ));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->