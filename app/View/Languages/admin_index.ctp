<section class="content-header">
    <h1>Language <small><i class="fa fa-angle-double-right"></i> All</small></h1>
</section>
<section class="content">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort('id', ' ID',array('class'=>'icon-sort'));?></th>
                <th><?php echo $this->Paginator->sort('language', ' Name',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('abbreviation', ' Abbr',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('apply_app', ' Use in app?',array('class'=>'icon-sort'));?> </th>
                <th width="80px">Action</th>
            </tr>
            <?php foreach ($rows as $row) {?>
            <tr>
                <td><?php echo $row["Language"]["id"] ?></td>
                <td><?php echo $row["Language"]["language"] ?></td>
                <td><?php echo $row["Language"]["abbreviation"] ?></td>
                <td><?php echo ($row["Language"]["apply_app"])?'Yes':'No' ?></td>

                <td>
                    <?php echo $this->Html->link(
                        '<i class="fa-edit fa fa-1"></i>',
                        array(
                            'controller' => 'languages',
                            'action' => 'edit',
                            $row["Language"]['id'],
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-mini btn-info','title'=>__("Edit"))
                    );?>
                </td>
            </tr>
            <?php }?>
        </table>
    </div><!-- /.box-body -->
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination center-block pagination-large text-center">
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
            <div class="center-block text-center">
                <p class="muted"> <?php echo $this->Paginator->counter(
                        __('Page').' {:page}/{:pages}, '.__('Records').' {:current}/{:count}, '.__('from').' {:start} '.__('To').' {:end}');?>
                </p>
                <p>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-pencil"></i> '.__("Add new"),
                        array(
                            'controller' => 'languages',
                            'action' => 'add',
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-primary')
                    );?>
                </p>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</section><!-- /.content -->