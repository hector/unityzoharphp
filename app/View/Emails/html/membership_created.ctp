<h2>Hi <?php echo $user["User"]["full_name"] ?></h2>
<br />
    Thank you, Your Payment has been processed.
    <br />
    Now you are a member of the unity zohar community.
<p>
    Membership level: <strong><?php echo $user["Membership"]["name"]?></strong><br />
    Membership Cost: <strong>US$<?php echo $user["Membership"]["cost"]?></strong><br />
    Billing Frequency: <strong>Monthly</strong><br />
</p>
<p>
    <small>You may cancel your membership any time at the membership update  link.</small>
</p>
<p>
    The UnityZohar.com Team
</p>