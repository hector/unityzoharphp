<p>Hi <?php echo $candle["User"]["full_name"] ?></p>
<br />
    Thank you, Your operation has been processed.
<p>
    Candle type: <strong><?php echo $candle["Candle"]["sponsor_type"]?></strong><br />
    Candle time: <strong><?php echo $candle["Candle"]["donation"]?> days</strong><br />
    Candle Cost: <strong>US$<?php echo $candle["Payment"]["amount"]?></strong><br />
</p>
<p>
    The UnityZohar.com Team
</p>

<!--

<p>Hi <?php echo $candle["Candle"]["full_name"] ?></p>
<br />
Thank you, Your operation has been processed.
<p>
    The candle for <strong><?php echo $candle["Candle"]["sponsor_type"]?></strong> is going to be active for <strong><?php echo $candle["Candle"]["donation"]?> days</strong>
    days with the support of the Zohar and Rabbi Shimon
    Candle Cost: <strong>US$<?php echo $candle["Payment"]["amount"]?></strong><br />
</p>
<p>
    https://unityzohar.com <br />
    Best Blessings,
</p>
<p>
    The Daily Zohar Team
</p>
-->