<section class="content-header">
    <h1>Payments values</h1>
</section>
<section class="content">
<div class="row">
    <div class="col-lg-12">
        <?php echo $this->Form->create('Payment', array(
            'class' => 'form-horizontal',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="row">
            <div class="col-md-5 center-block col-md-push-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">Support Unity Zohar for the next <em>{number}</em> days. $<em>{price}</em></div>
                    <div class="panel-body hidden"></div>
                    <table class="table">
                        <thead>
                            <th>#</th>
                            <th>Days</th>
                            <th>Price</th>
                        </thead>
                        <tbody>
                        <?php foreach ($payments as $pay) {?>
                        <tr>
                            <td class="text-center">1</td>
                            <td>
                                <div class="input-group">
                                    <?php echo $this->Form->input('Payment.'.$pay['Payment']['id'].'.days.'.$pay['Payment']['id'],
                                                array('class' => 'form-control','type'=>'text','placeholder'=>'days','value'=>$pay['Payment']['days']));
                                    ?>
                                    <span class="input-group-addon">days</span>

                                </div>
                            </td>
                            <td>
                                <div class="input-group">
                                    <span class="input-group-addon">$</span>
                                    <?php echo $this->Form->input('Payment.'.$pay['Payment']['id'].'.price.'.$pay['Payment']['id'],
                                                array('class' => 'form-control price-box','type'=>'text','placeholder'=>'price','value'=>$pay['Payment']['price']));
                                    ?>
                                </div>


                            </td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'),array('class'=>'btn btn-primary'));?>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</div>
</section>
<script>
    $(function(){
        $(".price-box").keypress(function(e){
            var unicode = e.charCode? e.charCode : e.keyCode
            if(unicode == 8 || unicode == 13){
                return true;
            }
            text = $(this).val().toString() + String.fromCharCode(unicode);
            if(!/^[0-9]+(\.([0-9]{1,2})?)?$/.test(text)){
                e.preventDefault();
            }

        });

    });

</script>