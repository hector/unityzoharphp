<?php $this->Paginator->options(array('url' => array('search'=>$this->data["search"])));?>
<section class="content-header">
    <h1>Payments <small><i class="fa fa-angle-double-right"></i> All</small></h1>
</section>
<section class="content">
<div class="row bottom30">
    <div class="col-lg-8">
    </div>
    <div class="col-lg-4">
        <form class="form-search form-search-top text-right">
            <div class="input-group custom-search-form">
                <input name="search" type="text" class="form-control" placeholder="<?php echo __("Search")?>" value="<?php echo $this->data['search']?>">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
            </div>
        </form>
    </div>
</div>
    <?php foreach($rows as $row){?>
        <div class="row">
            <div class="col-lg-8 col-lg-push-2">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <i class="fa fa-paypal"></i> Payment Details <span class="text-"><em>#<?php echo $row["Payment"]["id"] ?></em></span>
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body hidden"></div>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>User:</td><td><?php echo $row["User"]["full_name"]?></td>
                            </tr>
                            <tr>
                                <td>Email:</td><td><?php echo $this->Text->autoLinkEmails($row["User"]["email"]) ?></td>
                            </tr>
                            <tr>
                                <td>Created:</td>
                                <td>
                                    <?php echo date("m/d/2014 h:i a",strtotime($row["Payment"]["created"])); ?>
                                </td>
                            </tr>
<!--                            <tr>-->
<!--                                <td>Paypal Id:</td><td>-->
<!--                                   <a href="#" title="Paypal Info" class="pp-ag" data-id="--><?php //echo $row["Payment"]["payment_id"]?><!--">--><?php //echo $row["Payment"]["payment_id"]?><!--</a>-->
<!--                                </td>-->
<!--                            </tr>-->
                            <tr>
                                <td>Amount:</td><td>
                                    US$<?php echo $row["Payment"]["amount"]?>
                                </td>
                            </tr>
                            <tr>
                                <td>Status:</td>
                                <td><?php
                                    $class = ($row["Payment"]['state']?'success':'danger');
                                    $text = ($row["Payment"]['state']);
                                    ?>
                                    <span class="label label-<?php echo $class ?>"><?php echo $text ?></span>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <?php if($row["Candle"]["id"]){?>
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-danger">
                            <i class="fa fa-fire"></i> Candle Details
                        </li>
                    </ul>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Type</th>
                            <th>Time</th>
                            <th>From</th>
                            <th>Payment Id</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?php echo $row["Candle"]["id"]?></td>
                                <td><?php echo ($row["Candle"]["sponsor_type"]=="wish")?'Wish':"Refua"?></td>
                                <td><?php echo $row["Candle"]["donation"]?> days</td>
                                <td><?php echo date("M j, Y",strtotime($row["Candle"]["from"])) ?></td>
                                <td><?php echo $row["Candle"]["payment_id"]?> </td>
                                <td><?php echo $this->Time->timeAgoInWords(
                                        $row["Candle"]["created"],
                                        array(
                                            'format' => 'M jS, Y',
                                            'accuracy' => array('month' => 'month'),
                                            'end' => '+1 month')
                                    ); ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <?php }?>
                </div>
            </div>
        </div>
        <hr />
<?php } ?>
<div class="row">
    <div class="col-lg-12">
        <div class="pagination center-block pagination-large text-center">
            <ul class="pagination">
                <?php
                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                ?>
            </ul>
        </div>
    </div>
</div><!-- row -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="myModal" aria-hidden="true" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Paypal data</h4>
                </div>
                <div class="modal-body">
                    Loading data from paypal, please wait
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $(function(){
        $(".pp-ag").on("click",function(){
            $('#myModal').modal({
                backdrop: 'static',
                keyboard: false  // to prevent closing with Esc button (if you want this too)
            });
            var _id = $(this).data("id");
            $.ajax({
                type:"GET",
                url:  Admin.basePath+"admin/payments/get_paypal_data/"+_id,
                success: function(result){
                    console.log(result);
                    $('#myModal').modal("hide");
                    bootbox.dialog({
                        size:'large',
                        title: "Paypal Data",
                        message: "<pre>"+result+"</pre>"
                    });
                }
            });
        });
    });
</script>