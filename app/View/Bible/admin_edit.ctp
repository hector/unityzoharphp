<?php
    echo $this->Html->script('admin/ckeditor/ckeditor', array('inline' => false));
?>
<section class="content-header">
    <h1>Bible<small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('Zohar', array(
            'class' => '',
            'role' => 'form',
            'type' => 'file',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
            <div class="form-group">
                <label>Book</label>
                <?php echo $this->Form->input('Bible.bible_book_id',array('options'=>$books,'class' => 'form-control chosen-select'));?>
            </div>
            <div class="form-group">
                <label>Chapter #</label>
                <?php echo $this->Form->input('Bible.chapter',array("type"=>"text",'class' => 'form-control'));?>
            </div>
            <div class="form-group">
                <label>Verse #</label>
                <?php echo $this->Form->input('Bible.verse_number',array("type"=>"text",'class' => 'form-control'));?>
            </div>
            <div class="form-group">
                <label>Hebrew verse #</label>
                <?php echo $this->Form->input('Bible.verse_number_hebrew',array("type"=>"text",'class' => 'form-control zohar-text-no-bg'));?>
            </div>
            <div class="form-group">
                <label>Text</label>
                <?php echo $this->Form->input('Bible.verse_text',array('class' => 'form-control ckeditor','type'=>'textarea','placeholder'=>'Short info'));?>
            </div>
            <div class="form-group">
                <label>Hebrew Text</label>
                <?php echo $this->Form->input('Bible.verse_text_hebrew',array('class' => 'form-control','type'=>'textarea','placeholder'=>'Short info'));?>
            </div>
            <div class="form-group">
                <label>Zion Translation</label>
                <?php echo $this->Form->input('Bible.zion_translation',array('class' => 'form-control ckeditor','type'=>'textarea'));?>
            </div>
            <div class="form-group">
                <label>Verse Notes</label>
                <?php echo $this->Form->input('Bible.verse_note',array('class' => 'form-control ckeditor','type'=>'textarea'));?>
            </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
    <script type="text/javascript">
        CKEDITOR.replace( 'BibleVerseTextHebrew',{
            bodyClass : "zohar-text-no-bg"
        });
    </script>
</section><!-- /.content -->