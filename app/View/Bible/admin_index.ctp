<section class="content-header">
    <h1>Bible <small><i class="fa fa-angle-double-right"></i> All</small></h1>
</section>
<section class="content">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort('Bible.id', ' ID',array('class'=>'icon-sort'));?></th>
                <th><?php echo $this->Paginator->sort('BibleBook.name', ' Book',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('BibleBook.name_hebrew', ' Book Hebrew',array('class'=>'icon-sort'));?> </th>
                <th class="text-center"><?php echo $this->Paginator->sort('Bible.chapter', ' Chapter #',array('class'=>'icon-sort'));?> </th>
                <th class="text-center"><?php echo $this->Paginator->sort('Bible.verse_number', ' Verse #',array('class'=>'icon-sort'));?> </th>
                <th class="text-center"><?php echo $this->Paginator->sort('Bible.verse_number_hebrew', ' Hebrew verse #',array('class'=>'icon-sort'));?> </th>
                <th width="80px">Action</th>
            </tr>
            <?php foreach ($rows as $row) {?>
            <tr>
                <td><?php echo $row["Bible"]["id"] ?></td>
                <td><?php echo $row["BibleBook"]["name"] ?></td>
                <td class="zohar-text-no-bg"><?php echo $row["BibleBook"]["name_hebrew"] ?></td>
                <td class="text-center"><?php echo $row["Bible"]["chapter"] ?></td>
                <td class="text-center"><?php echo $row["Bible"]["verse_number"] ?></td>
                <td class="text-center zohar-text-no-bg"><?php echo $row["Bible"]["verse_number_hebrew"] ?></td>
                <td>
                    <?php echo $this->Html->link(
                        '<i class="fa fa-edit bigger-120"></i>',
                        array(
                            'controller' => 'bible',
                            'action' => 'edit',
                            $row[Inflector::singularize($this->name)]['id'],
                            'full_base' => true,
                            'admin'=>true
                        ),array('escape' => false,'class'=>'btn btn-mini btn-info','title'=>__("Edit"))
                    );?>

                </td>
            </tr>
            <?php }?>
        </table>
    </div><!-- /.box-body -->
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination center-block pagination-large text-center">
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
            <div class="center-block text-center">
                <p class="muted"> <?php echo $this->Paginator->counter(
                        __('Page').' {:page}/{:pages}, '.__('Records').' {:current}/{:count}, '.__('from').' {:start} '.__('To').' {:end}');?>
                </p>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
</section><!-- /.content -->