<section class="content-header">
    <h1>Memberships <small><i class="fa fa-angle-double-right"></i> All</small></h1>
</section>

<section class="content">
    <div class="box-body table-responsive no-padding">
        <table class="table table-hover table-striped">
            <tr>
                <th><?php echo $this->Paginator->sort('MembershipAgreement.id', ' ID',array('class'=>'icon-sort'));?></th>
                <th><?php echo $this->Paginator->sort('User.full_name', ' User',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('User.email', ' Email',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('User.membership_id', ' Membership',array('class'=>'icon-sort'));?> </th>
                <th><?php echo $this->Paginator->sort('MembershipAgreement.start_date', ' From',array('class'=>'icon-sort'));?> </th>
<!--                <th>--><?php //echo $this->Paginator->sort('MembershipAgreement.plan_id', ' Paypal PLan',array('class'=>'icon-sort'));?><!-- </th>-->
<!--                <th>--><?php //echo $this->Paginator->sort('MembershipAgreement.agreement_id', ' Paypal Agreement',array('class'=>'icon-sort'));?><!-- </th>-->
                <th><?php echo $this->Paginator->sort('MembershipAgreement.paypal_state', ' Status',array('class'=>'icon-sort'));?> </th>
                <th width="80px">Action</th>
            </tr>
            <?php foreach ($rows as $row) {?>
            <tr>
                <td><?php echo $row["MembershipAgreement"]["id"] ?></td>
                <td><?php echo $row["User"]["full_name"] ?></td>
                <td><?php echo $row["User"]["email"] ?></td>
                <td><?php echo $memberships[$row["MembershipAgreement"]["membership_id"]] ?></td>
<!--                <td>--><?php //echo $row["MembershipAgreement"]["plan_id"] ?><!--</td>-->
<!--                <td>--><?php //echo $row["MembershipAgreement"]["agreement_id"] ?><!--</td>-->
                <td><?php echo $row["MembershipAgreement"]["start_date"] ?></td>
                <td><?php echo $row["MembershipAgreement"]["paypal_state"] ?></td>
                <td>

                    <?php
                        if($row["MembershipAgreement"]["paypal_state"]!='Suspended'){
                            echo $this->Html->link(
                                '<i class="fa fa-user-times bigger-120"></i>',
                                array(
                                    'controller' => 'membership_agreements',
                                    'action' => 'cancel_membership',
                                    $row["MembershipAgreement"]["id"],
                                    'full_base' => true,
                                    'admin'=>true
                                ),array('escape' => false,'class'=>'btn btn-mini btn-danger','title'=>__("Suspend membership"))
                            );
                        }
                    ?>
<!--                    <button class="pp-ag btn btn-mini btn-primary" title="View paypal data" data-id="--><?php //echo $row["MembershipAgreement"]["agreement_id"]?><!--">-->
<!--                        <i class="fa fa-paypal bigger-120"></i>-->
<!--                    </button>-->
                </td>
            </tr>
            <?php }?>
        </table>
    </div><!-- /.box-body -->
    <div class="row">
        <div class="col-lg-12">
            <div class="pagination center-block pagination-large text-center">
                <ul class="pagination">
                    <?php
                    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));
                    echo $this->Paginator->next(__('next'), array('tag' => 'li','currentClass' => 'disabled'), null, array('tag' => 'li','class' => 'disabled','disabledTag' => 'a'));
                    ?>
                </ul>
            </div>
            <div class="center-block text-center">
                <p class="muted"> <?php echo $this->Paginator->counter(
                        __('Page').' {:page}/{:pages}, '.__('Records').' {:current}/{:count}, '.__('from').' {:start} '.__('To').' {:end}');?>
                </p>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="myModal" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Paypal data</h4>
                    </div>
                    <div class="modal-body">
                        Loading data from paypal, please wait
                    </div>
                </div>
            </div>
        </div>
</section><!-- /.content -->

<!--<script>-->
<!--    $(function(){-->
<!--        $(".pp-ag").on("click",function(){-->
<!--            $('#myModal').modal({-->
<!--                backdrop: 'static',-->
<!--                keyboard: false  // to prevent closing with Esc button (if you want this too)-->
<!--            });-->
<!--            var _id = $(this).data("id");-->
<!--            $.ajax({-->
<!--                type:"GET",-->
<!--                url:  Admin.basePath+"admin/membership_agreements/get_paypal_data/"+_id,-->
<!--                success: function(result){-->
<!--                    console.log(result);-->
<!--                    $('#myModal').modal("hide");-->
<!--                    bootbox.dialog({-->
<!--                        size:'large',-->
<!--                        title: "Paypal Data",-->
<!--                        message: "<pre>"+result+"</pre>"-->
<!--                    });-->
<!--                }-->
<!--            });-->
<!--        });-->
<!--    });-->
<!--</script>-->