<?php
    $output = [];
    foreach ($strings as $string) {
        if(!empty(trim($string["AppTranslation"]["translation"]))) {

            $patrón = '/(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?([\w\-]{10,12})(?:&feature=related)?(?:[\w\-]{0})?/';
            $sustitución = '<div class="video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/$1"></iframe></div>';
            $string["AppTranslation"]["translation"] = preg_replace($patrón, $sustitución,$string["AppTranslation"]["translation"]);
            //vimeo
            $patrón = '/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(\d+)/';
            $sustitución = '<div class=" video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/$1"></iframe></div>';
            $string["AppTranslation"]["translation"] = preg_replace($patrón, $sustitución, $string["AppTranslation"]["translation"]);

            $output[$string["TranslationString"]["string"]] = $string["AppTranslation"]["translation"];
        }

    }
    echo json_encode($output,JSON_PRETTY_PRINT);
?>