<section class="content-header">
    <div class="row">
        <div class="col-sm-4">
            <a href="<?php echo Router::url("/admin/app_translations/edit/".($neighbors['prev']['TranslationString']['id'] ?? 1) ,true) ?>"
               class="btn bg-olive"><i class="fa fa-angle-double-left"></i> Prev</a>
        </div>
        <div class="col-sm-4 text-center">
        </div>
        <div class="col-sm-4 text-right">
            <a href="<?php echo Router::url("/admin/app_translations/edit/".($neighbors['next']['TranslationString']['id'] ?? 1) ,true) ?>"
               class="btn bg-olive"><i class="fa fa-angle-double-right"></i> Next</a>
            </a>
        </div>
    </div>
    <hr />
    <h1>App translations <small><i class="fa fa-angle-double-right"></i> edit</small></h1>

</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('AppTranslation', array(
            'class' => '',
            'role' => 'form',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Content: </label>
            <?php echo $string["TranslationString"]["notes"];?>
        </div>
        <div class="form-group">
            <label>String</label>
            <?php echo $this->Form->input('TranslationString.id',array('type'=>'hidden','class' => 'form-control chosen-select','value' => $string["TranslationString"]["id"]));?>
            <?php echo $this->Form->input(null,array('type'=>'text','class' => 'form-control chosen-select','disabled'=> 'disabled','value' => $string["TranslationString"]["string"]));?>
        </div>

        <?php foreach($languages as $key => $lang){?>

            <div class="form-group">
                <label>Translation -> <?php echo $lang?></label>
                <?php
                    echo $this->Form->input('AppTranslation.'.$key.'.language_id',array('type'=>'hidden','class' => 'form-control chosen-select','value'=>$key));
                ?>

                <?php
                $trad ="";
                foreach ($string["AppTranslation"] as $transl) {
                        if($transl["language_id"]==$key){
                            echo $this->Form->input('AppTranslation.'.$key.'.id',array("type"=>"hidden",'class' => 'form-control','value'=>$transl["id"]));
                            $trad =$transl["translation"];
                        }
                 }
                echo $this->Form->input('AppTranslation.'.$key.'.translation',array("type"=>"textarea",'class' => 'form-control','value'=>$trad));
                ?>
                </div>
            <?php }?>

        <div class="form-group top30">
            <div class="row">
                <div class="col-md-6">
                    <button class="btn btn-primary btn-block" name="option" value="save"><i class="fa fa-edit"></i> Save & Exit</button>
                </div>
                <div class="col-md-6">
                    <button class="btn btn-primary btn-block" name="option" value="save-continue"><i class="fa fa-edit"></i> Save & Continue</button>
                </div>
            </div>

        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->