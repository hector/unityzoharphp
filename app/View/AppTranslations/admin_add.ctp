<section class="content-header">
    <h1>App translations <small><i class="fa fa-angle-double-right"></i> edit</small></h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('AppTranslation', array(
            'class' => '',
            'role' => 'form',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>String</label>
            <?php echo $this->Form->input('AppTranslation.translation_string_id',array('options'=>[0 => '-Select']+$strings,'class' => 'form-control chosen-select'));?>
        </div>
        <div class="form-group">
            <label>Language</label>
            <?php echo $this->Form->input('AppTranslation.language_id',array('options'=>[0 => '-Select']+$languages,'class' => 'form-control chosen-select'));?>
        </div>

        <div class="form-group">
            <label>Translation</label>
            <?php echo $this->Form->input('AppTranslation.translation',array("type"=>"textarea",'class' => 'form-control'));?>
        </div>
        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
</section><!-- /.content -->