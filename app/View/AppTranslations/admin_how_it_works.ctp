<section class="content-header">
    <h1>How It works Html</h1>
</section>
<section class="content">

    <div class="box-body">
        <?php echo $this->Form->create('AppTranslation', array(
            'class' => '',
            'role' => 'form',
            'novalidate' => true,
            'inputDefaults' => array(
                'label' => false,
                'div' => false,
                'error' => array('attributes' => array('wrap' => 'div', 'class' => 'text-danger'))
            )
        )); ?>
        <div class="form-group">
            <label>Content: </label>
            <?php echo $string["TranslationString"]["notes"];?>
        </div>
        <div class="form-group">
            <label>String</label>
            <?php echo $this->Form->input('TranslationString.id',array('type'=>'hidden','class' => 'form-control chosen-select','value' => $string["TranslationString"]["id"]));?>
            <?php echo $this->Form->input(null,array('type'=>'text','class' => 'form-control chosen-select','disabled'=> 'disabled','value' => $string["TranslationString"]["string"]));?>
        </div>

        <?php foreach($languages as $key => $lang){?>

            <div class="form-group">
                <label>Translation -> <?php echo $lang?></label>
                <?php
                echo $this->Form->input('AppTranslation.'.$key.'.language_id',array('type'=>'hidden','class' => 'form-control chosen-select','value'=>$key));
                ?>

                <?php
                $trad ="";
                foreach ($string["AppTranslation"] as $transl) {
                    if($transl["language_id"]==$key){
                        echo $this->Form->input('AppTranslation.'.$key.'.id',array("type"=>"hidden",'class' => 'form-control','value'=>$transl["id"]));
                        $trad =$transl["translation"];
                    }
                }
                echo $this->Form->input('AppTranslation.'.$key.'.translation',array("type"=>"textarea",'class' => 'form-control ckeditor','value'=>$trad));
                ?>
            </div>
        <?php }?>

        <div class="form-group top30">
            <button class="btn btn-primary btn-block"><i class="fa fa-edit"></i> Save</button>
        </div>
        <?php echo $this->Form->end();?>
    </div>
    <?php
    echo $this->Html->script('admin/ckeditor/ckeditor', array('inline' => false));
    ?>
    <script>
        CKEDITOR.stylesSet.add( 'my_styles', [
            { name: 'Hebrew text', element: 'span', attributes: { 'class': 'zohar-text-no-bg' } }
        ] );
        CKEDITOR.config.contentsCss = '<?php echo Router::url("/css/admin/ckeditor.css") ?>';
        CKEDITOR.config.stylesSet = ('my_styles');
        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
    </script>
</section><!-- /.content -->