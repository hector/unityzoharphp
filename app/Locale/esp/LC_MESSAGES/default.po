# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2014-06-05 16:36+0000\n"
"PO-Revision-Date: 2014-06-05 12:09-0430\n"
"Last-Translator: \n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"Language: es_ES\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.6.5\n"

#: /Elements/navbar.ctp:14
msgid "Welcome"
msgstr "Bienvenido"

#: /Elements/navbar.ctp:24
msgid "Logout"
msgstr "Salir"

#: /Elements/sidebar.ctp:3
msgid "Home"
msgstr "Inicio"

#: /Layouts/admin.ctp:5 /Layouts/login.ctp:5
msgid "Admin dashboard"
msgstr ""

#: /Users/add.ctp:4
msgid "Add User"
msgstr ""

#: /Users/add.ctp:12
msgid "Submit"
msgstr ""

#: /Users/admin_login.ctp:7
msgid "Username"
msgstr ""

#: /Users/admin_login.ctp:13
msgid "Password"
msgstr ""

#: /Users/admin_login.ctp:21
msgid "Remember Me"
msgstr ""

#: /Users/admin_login.ctp:25
msgid "Login"
msgstr ""

#: /Errors/error400.ctp:21 /Errors/error500.ctp:21
msgid "Error"
msgstr ""

#: /Errors/error400.ctp:23
msgid "The requested address %s was not found on this server."
msgstr ""

#: /Errors/error500.ctp:22
msgid "An Internal Error Has Occurred."
msgstr ""

#: /Layouts/error.ctp:19
msgid "CakePHP: the rapid development php framework"
msgstr ""
