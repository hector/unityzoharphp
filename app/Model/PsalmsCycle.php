<?php
App::uses('AppModel', 'Model');
class PsalmsCycle extends AppModel {

    var $recursive = -1;
    public $belongsTo = array(
        'Bible' => array(
            'className' => 'Bible',
            'foreignKey' => 'bible_id'
        ),
    );



    function find_by_session($session_id,$limit = 13){
        $cycle = $this->findByUnitySessionId($session_id);
        if(empty($cycle)){
            $cycle = $this->find("first",[
                'order'=>'id DESC'
            ]);

            $new_cycle = [
                "unity_session_id" => $session_id,
                "bible_id" => $cycle["PsalmsCycle"]['bible_id'],
                "cycle"=>$cycle["PsalmsCycle"]["cycle"]
            ];
            $this->create();
            $this->save($new_cycle);
            $cycle = $this->read();
        }else{
            $bible = $this->Bible->get_next_psalms_sync($limit,false);
            $count = count($bible);
            $this->id = $cycle["PsalmsCycle"]["id"];
            $cycle = $this->read();
            if($count<$limit){
                $this->id = $cycle["PsalmsCycle"]["id"];
                $new_cycle = $cycle["PsalmsCycle"]["cycle"] + 1;
                $this->saveField('cycle',$new_cycle);
                $cycle = $this->read();
            }

        }

        return $cycle;
    }

    function update_location($session_id,$location){
        $cycle = $this->findByUnitySessionId($session_id);
        $this->read(null,$cycle["PsalmsCycle"]["id"]);
        $this->set($location);
        return $this->save();
    }

    function get_last_visitors(){
          $visitors = $this->find("all",[
              'conditions' => [
                  'ip_address <>' =>NULL
              ],
              'fields' => [
                  'DISTINCT PsalmsCycle.ip_address',
                  'PsalmsCycle.country',
                  'PsalmsCycle.city',
                  'PsalmsCycle.lat',
                  'PsalmsCycle.lng',
                  'PsalmsCycle.country_iso',
              ],
              'order' => 'modified DESC',
              'limit' => 20,
          ]);

        return $visitors;
    }

}