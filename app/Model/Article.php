<?php
App::uses('AppModel', 'Model');
class Article extends AppModel {
    var $recursive = -1;
    public $belongsTo = [
        'Parasha' => [
            'className' => 'Parasha',
            'foreignKey' => 'parasha_id'
        ],
    ];
}