<?php
App::uses('AppModel', 'Model');
class AppTranslation extends AppModel {
   // var $recursive = -1;

    public $belongsTo = [
        'Language' => [
            'className' => 'Language',
            'foreignKey' => 'language_id'
        ],
        'TranslationString' => [
            'className' => 'TranslationString',
            'foreignKey' => 'translation_string_id'
        ],
    ];

    var $validate = array(
        "translation"=>array(
            "unique"=>array(
                "rule"=>array("checkUnique", array("translation_string_id", "language_id")),
                "message"=>"This translation already exists"
            )
        ),
        'translation_string_id' => array(
            'required' => array(
                'rule' => array('comparison', '>',0),
                'message' => 'Select an string to translate'
            ),
        ),
        'language_id' => array(
            'required' => array(
                'rule' => array('comparison', '>',0),
                'message' => 'Select a language to translate'
            ),
        ),
    );
    function checkUnique($data, $fields) {
        if (!is_array($fields)) {
            $fields = array($fields);
        }
        foreach($fields as $key) {
            $tmp[$key] = $this->data[$this->name][$key];
        }
        if (isset($this->data[$this->name][$this->primaryKey])) {
            $tmp[$this->primaryKey] = "<>".$this->data[$this->name][$this->primaryKey];

                }
        return $this->isUnique($tmp, false);
    }

}