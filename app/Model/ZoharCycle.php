<?php
App::uses('AppModel', 'Model');
class ZoharCycle extends AppModel {

    var $recursive = -1;
    public $belongsTo = array(
        'Zohar' => array(
            'className' => 'Zohar',
            'foreignKey' => 'zohar_id'
        ),
        'Parasha' => array(
            'className' => 'Parasha',
            'foreignKey' => 'parasha_id'
        ),
    );



    function find_by_session($session_id,$limit = 2){
        $cycle = $this->findByUnitySessionId($session_id);
        $q = Router::getRequest();
        $token = isset($q->query["_access_token"])?$q->query["_access_token"]:null;
        if(empty($cycle)){
            $this->log("new cycle", "debug");
            $cycle = $this->find("first",[
                'order'=>'id DESC'
            ]);
//            $zohar = $this->Zohar->get_next_sync($limit,false);
//            $zohar = $zohar["result"];
//            $zohar = array_pop($zohar);

            $new_cycle = [
                "unity_session_id" => $session_id,
                "zohar_id" => 0,
                "cycle"=>$cycle["ZoharCycle"]["cycle"],
                "parasha_id" =>$cycle["ZoharCycle"]["parasha_id"],
                "paragraph" =>$cycle["ZoharCycle"]["paragraph"],
                "token" => $token
            ];
            $this->create();
            $this->save($new_cycle);
            $cycle = $this->read();
            $zohar = $this->Zohar->get_next_sync($limit,false);
            if($zohar["last_paragraph"]) {
                $this->id = $cycle['ZoharCycle']["id"];
                $cn = $cycle['ZoharCycle']["cycle"]+1;
                $this->saveField("cycle", $cn);
            }
        }else{
            $this->log("exist cycle", "debug");
            $zohar = $this->Zohar->get_next_sync($limit,false);
            $lasts = null;
            if($zohar["last_paragraph"]) {
                $this->id = $cycle['ZoharCycle']["id"];
                $cn = $cycle['ZoharCycle']["cycle"]+1;
                $this->saveField("cycle", $cn);
            }else{
                $lasts = $this->find("first",[
                    'order' => 'ZoharCycle.cycle DESC',
                ]);
                if($cycle['ZoharCycle']["cycle"]< $lasts['ZoharCycle']["cycle"]){
                    $this->id = $cycle['ZoharCycle']["id"];
                    $cn = $lasts['ZoharCycle']["cycle"];
                    $this->saveField("cycle", $cn);
                }

            }
            $zohar = $zohar["result"];
            $zohar = array_pop($zohar);
            $this->id = $cycle["ZoharCycle"]["id"];
            //$this->saveField('zohar_id',$zohar["Zohar"]['id']);
//            $this->updateAll(
//                array(
//                    'ZoharCycle.parasha_id' =>$zohar["Zohar"]["parasha_id"],
//                    'ZoharCycle.paragraph' =>$zohar["Zohar"]["paragraph_num"],
//                ),
//                array('ZoharCycle.id' => $cycle["ZoharCycle"]["id"])
//            );
            $cycle = $this->read();
        }

        return $cycle;
    }

    function update_location($session_id,$location){
        $cycle = $this->findByUnitySessionId($session_id);
        $this->read(null,$cycle["ZoharCycle"]["id"]);
        $this->set($location);
        return $this->save();
    }

    function get_last_visitors($limit = 200,$offset = 0){
          $visitors = $this->find("all",[
              'conditions' => [
                  'ip_address <>' =>NULL,
                  'lat <>' => "0",
                  'lng <>' => "0",
              ],
              'fields' => [
                  'DISTINCT ZoharCycle.ip_address',
                  'ZoharCycle.country',
                  'ZoharCycle.city',
                  'ZoharCycle.lat',
                  'ZoharCycle.lng',
                  'ZoharCycle.country_iso',
              ],
              'order' => 'ZoharCycle.modified DESC',
              'limit' => $limit,
              'offset' => $offset,
          ]);
        return $visitors;
    }

    function get_country_readers(){
        $visitors = $this->find("all",[
            'conditions' => [
                'ZoharCycle.country_iso <>' =>NULL,
                'ZoharCycle.country_iso <>' =>"",
            ],
            'fields' => [
                'DISTINCT ZoharCycle.country_iso',
            ],
        ]);

        return $visitors;
    }

    function get_connected_users($days = 1){
        echo "";
        $result =  $this->find("count",[
            'conditions' => [
                'ZoharCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
            ],
            'fields' =>
                'DISTINCT ZoharCycle.ip_address'
            ,


        ]);
        /*$dbo = $this->getDatasource();
        $logs = $dbo->getLog();
        pr($logs);
*/
        return $result;

    }

    function get_paragraphs_read($days = "1",$tokens = null){

        $conditions = [
            'ZoharCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
        ];
        if($tokens){
            $conditions+=["ZoharCycle.token" => $tokens];
        }
        $count =  $this->find("count",[
            'conditions' => $conditions,
            //'fields' => 'DISTINCT ZoharCycle.ip_address',


        ]);

        return ($count * 2);

    }
    function get_top_countries($days = "500",$limit = 10){

        $result =  $this->find("all",[
            'conditions' => [
                'ZoharCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
                'ZoharCycle.country_iso <>'=> "",
                'ZoharCycle.ip_address <>' => null
            ],
            'fields' => [
                'DISTINCT ZoharCycle.country_iso',
                'count(DISTINCT ZoharCycle.ip_address) as total',
                'ZoharCycle.country',
            ],
            'group' => [
                'ZoharCycle.country_iso,ZoharCycle.country'
            ],
            'order' => ['total DESC'],
            'limit' => $limit
        ]);
        $return = [];
        foreach($result as $r){
            $result2 =  $this->find("first",[
                'conditions' => [
                    'ZoharCycle.created >' => date('Y-m-d', strtotime("-365 days")),
                    'ZoharCycle.country_iso'=>$r['ZoharCycle']['country_iso'],
                ],
                'fields' => [
                    'DISTINCT ZoharCycle.country_iso',
                    'count(DISTINCT ZoharCycle.ip_address) as total',
                    'ZoharCycle.country',
                ],
                'group' => [
                    'ZoharCycle.country_iso,ZoharCycle.country'
                ],
                'order' => ['total DESC']
            ]);
            $return[] = [
                "ZoharCycle" => [
                    'country_iso' => strtolower($r['ZoharCycle']['country_iso']),
                    'country' => $r['ZoharCycle']['country'],
                    'total' => $result2[0]['total'],
                    'total_day' => $r[0]['total'],
                ]
            ];
        }
        return $return;

    }

}