<?php
App::uses('AppModel', 'Model');
class Bookmark extends AppModel
{
    //var $recursive = 1;
    public $belongsTo = array(
        'Zohar' => array(
            'className' => 'Zohar',
            'foreignKey' => 'zohar_id'
        ),
        'Parasha' => [
            'className' => 'Parasha',
            'foreignKey' => 'parasha_id'
        ]
    );
}