<?php
App::uses('AppModel', 'Model');
class Translation extends AppModel {
    var $recursive = -1;
    public $belongsTo = [
        'Language' => [
            'className' => 'Language',
            'foreignKey' => 'language_id'
        ],
        'Zohar' => [
            'className' => 'Zohar',
            'foreignKey' => 'zohar_id'
        ],
    ];

}