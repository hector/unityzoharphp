<?php
App::uses('AppModel', 'Model');

class UnitySession extends AppModel {

    public function getActiveUsers(){
        $minutes = 5;
        $sessionData = $this->find('all',array(
            'conditions' => array(
                'expires >=' => time() - ($minutes * 60)
            )
        ));
        return count($sessionData);
    }

    public function getReaders($type = 'zohar'){
        $minutes = 15;
        $sessionData = Cache::read('latests_readers', 'minute15');
        if (!$sessionData) {
            $sessionData = $this->find('all',array(
                'conditions' => array(
                    'expires >' => time() - ($minutes * 60)
                )
            ));
            Cache::write('latests_readers', $sessionData, 'minute15');
        }
        $readers = 0;
        foreach ($sessionData as $session) {
            $data = $session["UnitySession"]["data"];
            $data = SessionReaderComponent::unserialize($data);
            if(isset($data[$type."_time"]) && ($data[$type."_time"] > time())){
                $readers++;
            }
        }
        return $readers == 0 ? 1 : $readers;
    }

    function get_time($type = 'zohar'){
        $sessionData = $this->find('all',[
            'conditions'=> [
                'expires >' => (time() - (1 * YEAR))
            ]
        ]);
        $total = 0;
        foreach ($sessionData as $session) {
            $data = $session["UnitySession"]["data"];
            $data = SessionReaderComponent::unserialize($data);
            if(isset($data[$type."_time"])){
                $start = strtotime($session["UnitySession"]["created"]);
                $end = strtotime($session["UnitySession"]["modified"]);
                $total += $end - $start;
            }
        }
        $dtF = new DateTime("@0");
        $dtT = new DateTime("@$total");
        return $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');


    }
}