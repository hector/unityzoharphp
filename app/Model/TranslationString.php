<?php
App::uses('AppModel', 'Model');
class TranslationString extends AppModel {
    //var $recursive = -1;

    public $hasMany = [
        'AppTranslation' => [
            'className' => 'AppTranslation',
            'foreignKey' => 'translation_string_id'
        ],
    ];
    public $validate = array(
        'string' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'An string is required'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'String already exists',
                'last' => true
            )
        ),
    );



    public function beforeSave($options = array()) {
        if (!empty($this->data['TranslationString']['string'])) {
            $this->data['TranslationString']['string'] = strtoupper($this->data['TranslationString']['string']);
        }
        return true;
    }
}