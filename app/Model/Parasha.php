<?php
App::uses('AppModel', 'Model');
class Parasha extends AppModel {
    var $recursive = -1;
    public $hasMany = [
        'Zohar',
        'Article'
    ];



    function get_total_paragraph($parasha_id){
       return $this->Zohar->find("count",[
            'conditions' => [
                "Parasha.id" =>$parasha_id
            ]
        ]);
    }

    function get_first(){
        $parasha = $this->find("first",[
            'conditions' => ['Parasha.active' => true],
            'order' => "Parasha.order ASC"
        ]);

        return $parasha;
    }

    function get_last(){
        $parasha = $this->find("first",[
            'conditions' => ['Parasha.active' => true],
            'order' => "Parasha.order DESC"
        ]);

        return $parasha;
    }

    function get_one($id){

        $parashot = $this->find("first",[
            'conditions' => [
                'Parasha.active' => 1,
                'Parasha.id' => $id
            ],
        ]);
        $count = $this->get_total_paragraph($id);
        $parashot["Parasha"]["parcount"] = $count;
        return $parashot;
    }


}