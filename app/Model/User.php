<?php
/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher', 'Controller/Component/Auth');
App::uses('CakeEmail', 'Network/Email');

class User extends AppModel {

    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'An username is required'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Username taken',
                'last' => true
            )
        ),
        'email' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'An email is required'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Email already registered',
                'last' => true
            ),
            'email' => array(
                'rule' => 'email',
                'message' => 'Invalid email',
                'last' => true
            )
        ),
        'site' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'An URL is required'
            ),
            'url' => array(
                'rule' => 'url',
                'message' => 'invalid URL',
                'last' => true
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            ),
            'largo' => array(
                'rule'    => array('minLength', 6),
                'message' => 'Password minimum length is 6 characters'
            ),
            'alphaNumeric' => array(
                'rule'     => 'alphaNumeric',
                'message'  => 'Password letters and numbers only'
            ),
        ),
        'confirm_password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            ),
            'largo' => array(
                'rule'    => array('minLength', 6),
                'message' => 'Password minimum length is 6 characters'
            ),
            'alphaNumeric' => array(
                'rule'     => 'alphaNumeric',
                'message'  => 'Password letters and numbers only'
            ),
            'compare'    => array(
                'rule' => array('validate_passwords', 'password'),
                'message' => 'The passwords you entered do not match.',
            )
        )
    );

    var $virtualFields = array(
        'full_name' => 'CONCAT(User.name, " ", User.lastname)'
    );
    var $displayField = 'full_name';

    public $belongsTo = [
        'Membership' => [
            'className' => 'Membership',
            'foreignKey' => 'membership_id'
        ],
    ];

    public $hasMany = array('Candle','Payment','MembershipAgreement');

    public function beforeSave($options = array()) {
        if (isset($this->data[$this->alias]['password'])) {
            $passwordHasher = new SimplePasswordHasher();
            $this->data[$this->alias]['password'] = $passwordHasher->hash(
                $this->data[$this->alias]['password']
            );
        }
        return true;
    }
    public function validate_passwords() {
        return $this->data[$this->alias]['password'] === $this->data[$this->alias]['confirm_password'];
}


    function registration_email($user_id){
        $this->recursive = -1;
        $user = $this->findById($user_id);
        $Email = new CakeEmail();
        $Email->from(array('noreply@unityzohar.com' => 'Unity Zohar'));
        $Email->to($user["User"]["email"]);
        $Email->subject('Unity Zohar Email Verification');

        $Email->template('registration')
            ->emailFormat('html');
        $Email->viewVars(array('hash' => md5($user["User"]["email"].$user["User"]["id"]),'user'=>$user["User"]["username"],'email'=>$user["User"]["email"]));
        $Email->send();
    }

    function forgot_password_email($user_id){
        $this->recursive = -1;
        $user = $this->findById($user_id);
        $Email = new CakeEmail();
        $Email->from(array('noreply@unity.com' => 'Unity Zohar'));
        $Email->to($user["User"]["email"]);
        $Email->subject('Unity Zohar password recovery email');

        $Email->template('forgot_password_web')
            ->emailFormat('html');
        $Email->viewVars(array('hash' => md5($user["User"]["id"].$user["User"]["email"]."password-verification"),'email'=>$user["User"]["email"],'user'=>$user["User"]["name"]));
        $Email->send();
    }
}