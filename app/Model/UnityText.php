<?php
App::uses('AppModel', 'Model');

class UnityText extends AppModel {
    public $validate = array(
        'abbreviation' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'An abbreviation is required'
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'abbreviation taken',
                'last' => true
            )
        ),
    );


    function beforeSave($options = array()){
        Cache::clear();
        return true;
    }

}