<?php
App::uses('AppModel', 'Model');

class Payment extends AppModel {

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
    );

    public $hasOne = array("Candle");

    function save_options($data){


        foreach ($data["Payment"] as $id => $payment) {
            $this->setSource('payments_values');
            $this->recursive = -1;
            $this->read(null,$id);
            $this->set('days',$payment["days"][$id]);
            $this->set('price',$payment["price"][$id]);
            $result = $this->save();
        }

        return true;

    }

    function get_by_user($user_id){
        $this->unbindModel(
            array(
                'belongsTo' => array('User')
            )

        );
       return $this->find("all",array('conditions'=>
            array(
                'Payment.user_id' => $user_id
            )
        ));


    }

    function received_email($user_id,$payment_id){
        $this->recursive = -1;
        $payment = $this->findById($payment_id);
        $user = $this->User->findById($user_id);
        $Email = new CakeEmail();
        $Email->from(array('noreply@unityzohar.com' => 'Unity Zohar'));
        $Email->to([$user["User"]["email"]]);
        //$Email->bcc('zion@dailyzohar.com');
        $Email->subject('Unity Zohar - Payment received');
        $Email->template('payment_received')
            ->emailFormat('html');
        $Email->viewVars(array('user'=>$user,'payment' => $payment));
        $result = $Email->send();
        $this->log(print_r($result,true),'info');
    }


}//class