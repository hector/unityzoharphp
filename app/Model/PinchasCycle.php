<?php
App::uses('AppModel', 'Model');
class PinchasCycle extends AppModel {

    var $recursive = -1;
    public $belongsTo = array(
        'Zohar' => array(
            'className' => 'Zohar',
            'foreignKey' => 'zohar_id'
        ),
    );



    function find_by_session($session_id){
        $cycle = $this->findByUnitySessionId($session_id);
        $q = Router::getRequest();
        $token = isset($q->query["_access_token"])?$q->query["_access_token"]:null;
        $limit = 2;
        if(empty($cycle)){
            $cycle = $this->find("first",[
                'order'=>'id DESC'
            ]);
            $new_cycle = [
                "unity_session_id" => $session_id,
                "zohar_id" => $cycle["PinchasCycle"]['zohar_id'],
                "cycle"=>$cycle["PinchasCycle"]["cycle"],
                "token" => $token
            ];
            $this->create();
            $this->save($new_cycle);
            $cycle = $this->read();
            $zohar = $this->Zohar->get_next_pinchas_sync($limit,false);
            if($zohar["last_paragraph"]) {
                $last = $this->find("first",[
                    'order' => 'PinchasCycle.id DESC'
                ]);
                $this->id = $last['PinchasCycle']["id"];
                $cn = $last['PinchasCycle']["cycle"]+1;
                $this->saveField("cycle", $cn);
            }
        }else{
            $zohar = $this->Zohar->get_next_pinchas_sync($limit,false);
            if($zohar["last_paragraph"]) {
                $last = $this->find("first",[
                    'order' => 'PinchasCycle.id DESC'
                ]);
                $this->id = $last['PinchasCycle']["id"];
                $cn = $last['PinchasCycle']["cycle"]+1;
                $this->saveField("cycle", $cn);
            }
            $this->id = $cycle["PinchasCycle"]["id"];
            $cycle = $this->read();

        }

        return $cycle;
    }

    function update_location($session_id,$location){
        $cycle = $this->findByUnitySessionId($session_id);
        $this->read(null,$cycle["PinchasCycle"]["id"]);
        $this->set($location);
        return $this->save();
    }

    function get_last_visitors(){
          $visitors = $this->find("all",[
              'conditions' => [
                  'ip_address <>' =>NULL
              ],
              'fields' => [
                  'DISTINCT PinchasCycle.ip_address',
                  'PinchasCycle.country',
                  'PinchasCycle.city',
                  'PinchasCycle.lat',
                  'PinchasCycle.lng',
                  'PinchasCycle.country_iso',
              ],
              'order' => 'PinchasCycle.modified DESC',
              'limit' => 20,
          ]);

        return $visitors;
    }

    function get_country_readers(){
        $visitors = $this->find("all",[
            'conditions' => [
                'PinchasCycle.country_iso <>' =>NULL,
                'PinchasCycle.country_iso <>' =>"",
            ],
            'fields' => 'DISTINCT PinchasCycle.country_iso',
        ]);

        return $visitors;
    }

    function get_connected_users($days = 1){

        return $this->find("count",[
            'conditions' => [
                'PinchasCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
            ],
            'fields' => 'DISTINCT PinchasCycle.ip_address',


        ]);

    }

    function get_paragraphs_read($days = "1",$tokens = null){

        $conditions = [
            'PinchasCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
        ];
        if($tokens){
            $conditions+=["PinchasCycle.token" => $tokens];
        }
        $count =  $this->find("count",[
            'conditions' => $conditions,
          //  'fields' => 'DISTINCT PinchasCycle.ip_address',


        ]);

        return ($count * 2);

    }
    function get_top_countries($days = "500",$limit = 10){

        $result =  $this->find("all",[
            'conditions' => [
                'PinchasCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
                'PinchasCycle.country_iso <>'=> "",
                'PinchasCycle.ip_address <>' => null
            ],
            'fields' => [
                'DISTINCT PinchasCycle.country_iso',
                'count(DISTINCT PinchasCycle.ip_address) as total',
                'PinchasCycle.country',
            ],
            'group' => [
                'PinchasCycle.country_iso,PinchasCycle.country'
            ],
            'order' => ['total DESC'],
            'limit' => $limit
        ]);
        $return = [];
        foreach($result as $r){
            $result2 =  $this->find("first",[
                'conditions' => [
                    'PinchasCycle.created >' => date('Y-m-d', strtotime("-365 days")),
                    'PinchasCycle.country_iso'=>$r['PinchasCycle']['country_iso'],
                ],
                'fields' => [
                    'DISTINCT PinchasCycle.country_iso',
                    'count(DISTINCT PinchasCycle.ip_address) as total',
                    'PinchasCycle.country',
                ],
                'group' => [
                    'PinchasCycle.country_iso,PinchasCycle.country'
                ],
                'order' => ['total DESC'],
                'limit' => 1
            ]);


            $return[] = [
                "PinchasCycle" => [
                    'country_iso' => strtolower($r['PinchasCycle']['country_iso']),
                    'country' => $r['PinchasCycle']['country'],
                    'total' => $result2[0]['total'],
                    'total_day' => $r[0]['total'],
                ]
            ];
        }
        return $return;

    }

}