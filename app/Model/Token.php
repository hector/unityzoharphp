<?php
App::uses('AppModel', 'Model');
class Token extends AppModel{

    var $name = "Token";

    function get($email){
        $this->updateAll(
            array('Token.active' => false),
            array('Token.email' => $email)
        );
        $token =  hash_hmac('sha256', $email,Configure::read('salt').time());
        $data = array(
            'Token' => array(
                'token' => $token,
                'email' => $email
            )
        );
        if($this->save($data)){
            return $token;
        }else{
            return false;
        }


    }
}
