<?php
App::uses('AppModel', 'Model');

class MembershipAgreement extends AppModel {

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
    );



//    function _suspend_previous_membership($exclude_id){
//        App::import('Component','Paypal');
//        $Paypal = new PaypalComponent(new ComponentCollection);
//        $agreements = $this->find("all",[
//            'conditions' => [
//                "MembershipAgreement.agreement_id <>" => $exclude_id,
//                "MembershipAgreement.paypal_state <>" => "Suspended",
//            ]
//        ]);
//        foreach($agreements as $ag){
//            $agpp = $Paypal->suspend_agreement($ag["MembershipAgreement"]["agreement_id"]);
//            $this->User->MembershipAgreement->id = $ag["MembershipAgreement"]["id"];
//            $this->User->MembershipAgreement->saveField("paypal_state",$agpp->getState());
//            $this->log("----------> agreement suspended",'info');
//            $this->log("agreement id: ".$ag["MembershipAgreement"]["agreement_id"],'info');
//        }
//    }

    function _suspend_previous_membership($exclude_id){
        App::import('Component','Paypal');
        $Paypal = new PaypalComponent(new ComponentCollection);
        $agreements = $this->find("all",[
            'conditions' => [
                "MembershipAgreement.agreement_id <>" => $exclude_id,
                "MembershipAgreement.paypal_state <>" => "Suspended",
            ]
        ]);
        foreach($agreements as $ag){
            $r = $Paypal->suspend_agreement_payflow($ag["MembershipAgreement"]["agreement_id"]);
            $this->User->MembershipAgreement->id = $ag["MembershipAgreement"]["id"];
            $this->User->MembershipAgreement->saveField("paypal_state","Suspended");
            $this->log("----------> agreement suspended",'info');
            $this->log("agreement id: ".$ag["MembershipAgreement"]["agreement_id"],'info');
        }
    }

//    function _suspend_membership($user_id){
//
//        App::import('Component','Paypal');
//        $Paypal = new PaypalComponent(new ComponentCollection);
//        $agreements = $this->find("all",[
//            'conditions' => [
//                "MembershipAgreement.user_id" =>$user_id,
//                "MembershipAgreement.paypal_state <>" => "Suspended",
//            ],
//            'order' => "MembershipAgreement.created DESC"
//        ]);
//        foreach($agreements as $ag){
//            $agpp = $Paypal->suspend_agreement($ag["MembershipAgreement"]["agreement_id"]);
//            $this->User->MembershipAgreement->id = $ag["MembershipAgreement"]["id"];
//            $this->User->MembershipAgreement->saveField("paypal_state",$agpp->getState());
//            $this->log("----------> agreement suspended",'info');
//            $this->log("agreement id: ".$ag["MembershipAgreement"]["agreement_id"],'info');
//        }
//    }
    function _suspend_membership($user_id){

        App::import('Component','Paypal');
        $Paypal = new PaypalComponent(new ComponentCollection);
        $agreements = $this->find("all",[
            'conditions' => [
                "MembershipAgreement.user_id" =>$user_id,
                "MembershipAgreement.paypal_state <>" => "Suspended",
            ],
            'order' => "MembershipAgreement.created DESC"
        ]);
        foreach($agreements as $ag){
            $agpp = $Paypal->suspend_agreement_payflow($ag["MembershipAgreement"]["agreement_id"]);
            $this->User->MembershipAgreement->id = $ag["MembershipAgreement"]["id"];
            $this->User->MembershipAgreement->saveField("paypal_state","Suspended");
            $this->log("----------> agreement suspended",'info');
            $this->log("agreement id: ".$ag["MembershipAgreement"]["agreement_id"],'info');
        }
    }



    function received_email($user_id,$payment_id){
        $this->recursive = -1;
        $payment = $this->findById($payment_id);
        $user = $this->User->findById($user_id);
        $Email = new CakeEmail();
        $Email->from(array('noreply@unityzohar.com' => 'Unity Zohar'));
        $Email->to([$user["User"]["email"]]);
        $Email->bcc('znefesh@gmail.com');
        $Email->bcc('hectormotesinos@gmail.com');
        $Email->subject('Unity Zohar - Payment received');
        $Email->template('membership_created')
            ->emailFormat('html');
        $Email->viewVars(array('user'=>$user,'payment' => $payment));
        $result = $Email->send();
        $this->log(print_r($result,true),'info');
    }
    function cancel_membership_mail($user_id){
        $this->recursive = -1;
        $user = $this->User->findById($user_id);
        $Email = new CakeEmail();
        $Email->from(array('noreply@unityzohar.com' => 'Unity Zohar'));
        $Email->to([$user["User"]["email"]]);
        $Email->bcc('znefesh@gmail.com');
        $Email->bcc('hectormotesinos@gmail.com');
        $Email->subject('Unity Zohar - Membership cancel');
        $Email->template('membership_cancel')
            ->emailFormat('html');
        $Email->viewVars(array('user'=>$user));
        $result = $Email->send();
        $this->log(print_r($result,true),'info');
    }


}//class