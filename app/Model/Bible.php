<?php
App::uses('AppModel', 'Model');
class Bible extends AppModel {

    public $belongsTo = array(
        'BibleBook' => array(
            'className' => 'BibleBook',
            'foreignKey' => 'bible_book_id'
        ),
    );


    function find_all($params){

        $search_defaults = array(
            'book_id' => -1,
            'chapter' => 0,
            'verse' =>0,
            'verse_from' => 1,
            'limit' => 50
        );
        $params = array_merge($search_defaults,$params);
        $params = array_uintersect_assoc($params,$search_defaults, create_function(null, "return 0;"));


        $conditions = array();

        if($params["book_id"]){
            $conditions += array('Bible.bible_book_id' =>$params["book_id"]);
        }
        if($params["chapter"]){
            $conditions += array('Bible.chapter' =>$params["chapter"]);
        }
        if($params["verse"]){
            $conditions += array('Bible.verse_number' =>$params["verse"]);
        }
        if($params["verse_from"]){
            $conditions += array('Bible.verse_number >=' =>$params["verse_from"]);
        }
        $this->recursive = -1;
        $rows = $this->find("all",array('conditions'=>$conditions,'limit'=>$params["limit"]));

        return $rows;
    }


    function get_next_psalms($bible_id,$limit = 2,$exclude = false){

        $condition = ($exclude)?'>':'>=';

        $rows = $this->find("all",
            [
                'conditions' =>
                    [
                        'Bible.id '.$condition => $bible_id,
                        'BibleBook.slug' => 'psalms',
                    ],
                'limit' => $limit,
            ]
        );

        if(empty($rows)){
            //start cycle
            $rows = $this->find("all",
                [
                    'conditions' =>
                        [
                            'BibleBook.slug' => 'psalms',
                            'Bible.chapter' => 1,
                            'Bible.verse_number >=' => 1
                        ],
                    'limit' => $limit,
                ]
            );
        }

        $results = [];
        $book_id = isset($rows[0]["BibleBook"]["id"])?$rows[0]["BibleBook"]["id"]:0;

        foreach ($rows as &$result) {
            if($book_id == $result["BibleBook"]['id']){
                $result["Bible"]["verse_text_hebrew"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Bible"]["verse_text_hebrew"]);
                //$result["Zohar"]["text_aramaic"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_aramaic"]);
                $result["Bible"]['transliterated_hebrew'] = TransliterateComponent::transliterate($result["Bible"]['verse_text_hebrew']);
               // $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate($result["Zohar"]['text_aramaic']);
                $results[] = $result;
            }
        }
        return $results;
    }

    function get_next_psalms_sync($limit = 13,$update = true){
        $this->PsalmsCycle = ClassRegistry::init("PsalmsCycle");
        $last = $this->PsalmsCycle->find("first",[
            'order' => 'PsalmsCycle.id DESC'
        ]);
        $rows = $this->find("all",
            [
                'conditions' =>
                    [
                        'Bible.id >' => $last["PsalmsCycle"]["bible_id"],
                        'BibleBook.slug' => 'psalms',
                    ],
                'limit' => $limit,
            ]
        );

        if(empty($rows)){
            //start cycle
            $rows = $this->find("all",
                [
                    'conditions' =>
                        [
                            'BibleBook.slug' => 'psalms',
                            'Bible.chapter' => 1,
                            'Bible.verse_number >=' => 1
                        ],
                    'limit' => $limit,
                ]
            );
        }
        if(isset($rows[count($rows)-1]) && $update){
            $this->PsalmsCycle->id = $last["PsalmsCycle"]["id"];
            $this->PsalmsCycle->saveField("bible_id",$rows[count($rows)-1]["Bible"]["id"]);

        }
        $results = [];
        $book_id = isset($rows[0]["BibleBook"]["id"])?$rows[0]["BibleBook"]["id"]:0;

        foreach ($rows as &$result) {
            if($book_id == $result["BibleBook"]['id']){
                $result["Bible"]["verse_text_hebrew"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Bible"]["verse_text_hebrew"]);
                //$result["Zohar"]["text_aramaic"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_aramaic"]);
                $result["Bible"]['transliterated_hebrew'] = TransliterateComponent::transliterate($result["Bible"]['verse_text_hebrew']);
                // $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate($result["Zohar"]['text_aramaic']);
                $results[] = $result;
            }
        }
        return $results;
    }

    function get_psalms_percent($bible_id){
        $this->recursive = 1;
        $total = $this->find("count",[
            'conditions' => [
                'BibleBook.slug' => 'psalms'
            ]
        ]);
        $rows = $this->find("count",
            [
                'conditions' =>
                    [
                        'Bible.id <=' => $bible_id,
                        'BibleBook.slug' => 'psalms',
                    ],
            ]
        );
        $percent = $rows / $total * 100;
        return round($percent,2);
    }

}