<?php
App::uses('AppModel', 'Model');
class TikunimCycle extends AppModel {

    var $recursive = -1;
    public $belongsTo = array(
        'Zohar' => array(
            'className' => 'Zohar',
            'foreignKey' => 'zohar_id'
        ),
    );



    function find_by_session($session_id){
        $cycle = $this->findByUnitySessionId($session_id);
        $q = Router::getRequest();
        $token = isset($q->query["_access_token"])?$q->query["_access_token"]:null;
        $limit = 2;
        if(empty($cycle)){
            $cycle = $this->find("first",[
                'order'=>'id DESC'
            ]);
            $new_cycle = [
                "unity_session_id" => $session_id,
                "zohar_id" => $cycle["TikunimCycle"]['zohar_id'],
                "cycle"=>$cycle["TikunimCycle"]["cycle"],
                "token" => $token
            ];
            $this->create();
            $this->save($new_cycle);
            $cycle = $this->read();
            $zohar = $this->Zohar->get_next_tikunim_sync($limit,false);
            if($zohar["last_paragraph"]) {
                $last = $this->find("first",[
                    'order' => 'TikunimCycle.id DESC'
                ]);
                $this->id = $last['TikunimCycle']["id"];
                $cn = $last['TikunimCycle']["cycle"]+1;
                $this->saveField("cycle", $cn);
            }
        }else{
            $zohar = $this->Zohar->get_next_tikunim_sync($limit,false);
            if($zohar["last_paragraph"]) {
                $last = $this->find("first",[
                    'order' => 'TikunimCycle.id DESC'
                ]);
                $this->id = $last['TikunimCycle']["id"];
                $cn = $last['TikunimCycle']["cycle"]+1;
                $this->saveField("cycle", $cn);
            }
            $this->id = $cycle["TikunimCycle"]["id"];
            $cycle = $this->read();
        }
        return $cycle;
    }

    function update_location($session_id,$location){
        $cycle = $this->findByUnitySessionId($session_id);
        $this->read(null,$cycle["TikunimCycle"]["id"]);
        $this->set($location);
        return $this->save();
    }

    function get_last_visitors(){
          $visitors = $this->find("all",[
              'conditions' => [
                  'ip_address <>' =>NULL
              ],
              'fields' => [
                  'DISTINCT TikunimCycle.ip_address',
                  'TikunimCycle.country',
                  'TikunimCycle.city',
                  'PTikunimCycle.lat',
                  'TikunimCycle.lng',
                  'TikunimCycle.country_iso',
              ],
              'order' => 'modified DESC',
              'limit' => 20,
          ]);

        return $visitors;
    }
    function get_country_readers(){
        $visitors = $this->find("all",[
            'conditions' => [
                'TikunimCycle.country_iso <>' =>NULL,
                'TikunimCycle.country_iso <>' =>"",
            ],
            'fields' => [
                'DISTINCT TikunimCycle.country_iso',
            ],
        ]);

        return $visitors;
    }

    function get_connected_users($days = 1){

        return $this->find("count",[
            'conditions' => [
                'TikunimCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
            ],
            'fields' => 'DISTINCT TikunimCycle.ip_address',

        ]);

    }

    function get_paragraphs_read($days = "1",$tokens = null){

        $conditions = [
            'TikunimCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
        ];
        if($tokens){
            $conditions+=["TikunimCycle.token" => $tokens];
        }
        $count =  $this->find("count",[
            'conditions' => $conditions,
            //'fields' => 'DISTINCT TikunimCycle.ip_address',


        ]);

        return ($count * 2);

    }
    function get_top_countries($days = "500",$limit = 10){

        $result =  $this->find("all",[
            'conditions' => [
                'TikunimCycle.created >' => date('Y-m-d', strtotime("-".$days." days")),
                'TikunimCycle.country_iso <>'=> "",
                'TikunimCycle.ip_address <>' => null
            ],
            'fields' => [
                'DISTINCT TikunimCycle.country_iso',
                'count(DISTINCT TikunimCycle.ip_address) as total',
                'TikunimCycle.country',
            ],
            'group' => [
                'TikunimCycle.country_iso,TikunimCycle.country'
            ],
            'order' => ['total DESC'],
            'limit' => $limit
        ]);
        $return = [];
        foreach($result as $r){
            $result2 =  $this->find("first",[
                'conditions' => [
                    'TikunimCycle.created >' => date('Y-m-d', strtotime("-365 days")),
                    'TikunimCycle.country_iso'=>$r['TikunimCycle']['country_iso'],
                ],
                'fields' => [
                    'DISTINCT TikunimCycle.country_iso',
                    'count(DISTINCT TikunimCycle.ip_address) as total',
                    'TikunimCycle.country',
                ],
                'group' => [
                    'TikunimCycle.country_iso,TikunimCycle.country'
                ],
                'order' => ['total DESC'],
                'limit' => 1
            ]);


            $return[] = [
                "TikunimCycle" => [
                    'country_iso' => strtolower($r['TikunimCycle']['country_iso']),
                    'country' => $r['TikunimCycle']['country'],
                    'total' => $result2[0]['total'],
                    'total_day' => $r[0]['total'],
                ]
            ];
        }
        return $return;

    }

}