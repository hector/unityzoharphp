<?php
App::uses('AppModel', 'Model');

class Candle extends AppModel {

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Payment' => array(
            'className' => 'Payment',
            'foreignKey' => 'payment_id'
        ),
    );
    public $validate = array(
        'donation' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'You must specify the time field'
            ),
            'numeric' => array(
                'rule' => 'numeric',
                'message' => 'Time must be a numeric value',
                'last' => true
            )
        ),
        'text' => array(
            'required' => array(
                'rule' => array('notBlank'),
                'message' => 'You must specify the text field'
            ),
        )
    );


    function get_by_event($type='all',$limit = 90){

        $options = [
            'conditions' => [
                'Candle.from + INTERVAL Candle.donation DAY >= current_date',
            ],
            'order'=>'RAND()',
            'limit' => $limit
        ];
        if($type != 'all'){
            $options['conditions'][] = ["Candle.sponsor_type" => $type];
        }

        $this->recursive = -1;

        $result = $this->find("all",$options);

        foreach($result as &$candle){
            $candle["Candle"]["hebrew"] = 0;
            if(preg_match("/\p{Hebrew}/u", $candle["Candle"]["name"])){
                $candle["Candle"]["hebrew"] = 1;
                $candle["Candle"]["gender"] = ($candle["Candle"]["gender"]=="ben")?"בן":"בת";
            }
        }



//        $log = $this->getDataSource()->getLog(false, false);
        return $result;
    }

    function received_email($candle_id, $email= null){
        $this->recursive = 1;
        $candle = $this->findById($candle_id);
        $Email = new CakeEmail();
        $Email->from(array('noreply@unityzohar.com' => 'Unity Zohar'));
        if(!$email) {
            $Email->to([$candle["User"]["email"]]);
        }else{
            $Email->to([$email]);
        }

        //$Email->bcc('znefesh@gmail.com');
        //$Email->bcc('hectormotesinos@gmail.com');
        $Email->subject('Virtual Unity Zohar candle');
        $Email->template('candle_created')
            ->emailFormat('html');
        $Email->viewVars(array('candle' => $candle));
        $result = $Email->send();
        $this->log(print_r($result,true),'info');
    }


}