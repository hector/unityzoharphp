<?php
App::uses('AppModel', 'Model');
App::uses('HttpSocket', 'Network/Http');
class Unity extends AppModel {

    var $useTable = false;

    function get_text($type){
        $HttpSocket = new HttpSocket();
        $ws = Configure::read('WebService');
        $results = $HttpSocket->get($ws["url"],
            array(
                'p' => $ws["password"],
                'b' =>$type,
                't' =>1,
            )
        );
        return json_decode($results->body);
    }

}