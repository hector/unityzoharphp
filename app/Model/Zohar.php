<?php
App::uses('AppModel', 'Model');
class Zohar extends AppModel {

    public $belongsTo = [
        'Parasha' => [
            'className' => 'Parasha',
            'foreignKey' => 'parasha_id'
        ],
    ];
    public $hasMany = [
        'Translation','ZoharCycle',
        'ZoharCommentary' => [
            'foreignKey' => false,
            'finderQuery' =>
            'SELECT ZoharCommentary.*, Zohar.parasha_id
              FROM ZoharCommentary as ZoharCommentary JOIN zohars as Zohar on Zohar.id = {$__cakeID__$}
              WHERE ZoharCommentary.zoharp = Zohar.parasha_id and ZoharCommentary.zoharparag = Zohar.paragraph_num and ZoharCommentary.active = 1'
        ]
    ];


    function find_all($params){

        $search_defaults = array(
            'parasha_id' => -1,
            'paragraph' => 0,
            'paragraph_from' => 1,
            'limit' => 50
        );
        $params = array_merge($search_defaults,$params);
        $params = array_uintersect_assoc($params,$search_defaults, create_function(null, "return 0;"));


        $conditions = array();

        if($params["parasha_id"]){
            $conditions += array('Zohar.parasha_id' =>$params["parasha_id"]);
        }
        if($params["paragraph"]){
            $conditions += array('Zohar.paragraph_num' =>$params["paragraph"]);
        }
        if($params["paragraph_from"]){
            $conditions += array('Zohar.paragraph_num >=' =>$params["paragraph_from"]);
        }

        $this->recursive = -1;
        $rows = $this->find("all",array('conditions'=>$conditions,'limit'=>$params["limit"]));

        return $rows;
    }


    function get_next($parasha_id,$paragraph,$limit = 2,$exclude = false){
        $last_paragraph = false;
        $condition = ($exclude)?'>':'>=';

        $rows = $this->find("all",
            [
                'conditions' =>
                    [
                        'Zohar.paragraph_num '.$condition => $paragraph,
                        'Zohar.parasha_id '=> $parasha_id
                    ],
                'limit' => $limit,
                'order' => 'Parasha.order ASC,Zohar.paragraph_num ASC'
            ]
        );
        //new book
        if(empty($rows)){
            $new_parasha = $this->Parasha->find("first",[
                'conditions' => [
                    'Parasha.order >' => $parasha_id,
                    'Parasha.active' => 1,

                ],
                'order' => 'Parasha.order ASC'
            ]);
            if(empty($new_parasha)){//last book
                $new_parasha = $this->Parasha->find("first",[
                    'conditions' =>[
                        'Parasha.active' => 1
                    ],
                    'order' => 'Parasha.order ASC',
                ]);
                $last_paragraph = true;
            }
            $parasha_id = $new_parasha["Parasha"]["id"];
            $paragraph = 1;
            $rows = $this->find("all",
                [
                    'conditions' =>
                        [
                            'Zohar.paragraph_num' => $paragraph,
                            'Zohar.parasha_id '=> $parasha_id
                        ],
                    'limit' => $limit,
                    'order' => 'Parasha.order ASC,Zohar.paragraph_num ASC'
                ]
            );
        }

        $results = [];

        $parasha_id = $rows[0]["Parasha"]["id"];

        foreach ($rows as &$result) {
            if($parasha_id == $result["Parasha"]['id']){
                $result = self::format_zohar($result);
                $results[] = $result;
            }
        }
        return array('result' => $results,'last_paragraph' => $last_paragraph);
    }


    function get_next_sync($limit = 2,$update = true){
        $last_paragraph = false;
        $this->ZoharCycle->recursive = 0;
        $last = $this->ZoharCycle->find("first",[//last zohar readed
            'order' => 'ZoharCycle.id DESC'
        ]);
        //$this->recursive = -1;
        $this->unbindModel(['hasMany'=>'ZoharCycle']);
        $rows = $this->find("all",
            [
                'conditions' =>
                    [
                        'Zohar.paragraph_num >' => $last["ZoharCycle"]["paragraph"],
                        'Zohar.parasha_id '=> $last["ZoharCycle"]["parasha_id"]
                    ],
                'limit' => $limit,
                'order' => 'Parasha.order ASC,Zohar.paragraph_num ASC'
            ]
        );

        //new book
        if(empty($rows)){
            $new_parasha = $this->Parasha->find("first",[
                'conditions' => [
                    'Parasha.order >' => $last["Parasha"]["order"],
                    'Parasha.active' => 1,

                ],
                'order' => 'Parasha.order ASC'
            ]);
            if(empty($new_parasha)){//last book
                $new_parasha = $this->Parasha->find("first",[
                    'conditions' =>[
                        'Parasha.active' => 1
                    ],
                    'order' => 'Parasha.order ASC',
                ]);
                $last_paragraph = true;
            }
            $parasha_id = $new_parasha["Parasha"]["id"];
            $paragraph = 1;
            $rows = $this->find("all",
                [
                    'conditions' =>
                        [
                            'Zohar.paragraph_num >=' => $paragraph,
                            'Zohar.parasha_id '=> $parasha_id
                        ],
                    'limit' => $limit,
                    'order' => 'Parasha.order ASC,Zohar.paragraph_num ASC'
                ]
            );
        }
        //
        if(isset($rows[count($rows)-1]) && $update){
            $this->ZoharCycle->id = $last["ZoharCycle"]["id"];
            $this->ZoharCycle->saveField("paragraph",$rows[count($rows)-1]["Zohar"]["paragraph_num"]);
            $this->ZoharCycle->saveField("parasha_id",$rows[count($rows)-1]["Zohar"]["parasha_id"]);
        }

        $results = [];

        $parasha_id = $rows[0]["Parasha"]["id"];

        foreach ($rows as &$result) {
            if($parasha_id == $result["Parasha"]['id']){
                $result = self::format_zohar($result);
                if(is_file(WWW_ROOT."audio".DS.$result["Parasha"]["audio_name"].DS."-".sprintf("%03d", $result['Zohar']["paragraph_num"]).".mp3" )){
                    $result['Zohar']["audio"] = Router::url("/audio/",true).$result["Parasha"]["audio_name"]."-".sprintf("%03d", $result['Zohar']["paragraph_num"]).".mp3";
                    $result['Zohar']["audio"] = str_replace("http","https",$result['Zohar']["audio"]);
                }else{
                    $result['Zohar']["audio"] = 0;
                }



                $results[] = $result;
            }
        }
        return array('result' => $results,'last_paragraph' => $last_paragraph);
    }



    function format_zohar($result){

        $result["Zohar"]['transliterated_hebrew'] = TransliterateComponent::transliterate($result["Zohar"]['text_hebrew']);
        $result["Zohar"]['transliterated_hebrew'] = str_replace("sama'e\"l","S’M'",$result["Zohar"]['transliterated_hebrew']);
        $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate($result["Zohar"]['text_aramaic']);

        $result["Zohar"]["text_aramaic"] = str_replace("(", "<span class='normal_text_par'>&#40;", $result["Zohar"]["text_aramaic"]); // change the style for any text appears in ()
        $result["Zohar"]["text_aramaic"] =   str_replace(")", "&#41;</span>", $result["Zohar"]["text_aramaic"]);
        $result["Zohar"]["text_aramaic"] =   str_replace('"', "''", $result["Zohar"]["text_aramaic"]);
        $result["Zohar"]["text_aramaic"] =   str_replace("<span class='normal_text_par'>&#40;\xd7\xa0''\xd7\x90", "<span class='nun_aleph_par'>&#40;\xd7\xa0''\xd7\x90", $result["Zohar"]["text_aramaic"]);//nun"aleph  The letters נ"א means 'other version'
        $result["Zohar"]["text_aramaic"] =   str_replace("<span class='normal_text_par'>&#40;\xd7\xa1''\xd7\x90", "<span class='samech_aleph_par'>&#40;\xd7\xa1''\xd7\x90", $result["Zohar"]["text_aramaic"]); //samech"aleph same as נ"א above
        $result["Zohar"]["text_aramaic"] =   str_replace("<span class='normal_text_par'>&#40;\xd7\x93\xd7\xa3", "<span class='dalet_pei_par'>&#40;\xd7\x93\xd7\xa3", $result["Zohar"]["text_aramaic"]); //dalet pei . This represent a reference to page number in the old 3

        $result["Zohar"]['text_hebrew'] =   str_replace("(", "<span class='normal_text_par'>&#40;", $result["Zohar"]['text_hebrew']);//other text in ()
        $result["Zohar"]['text_hebrew'] =   str_replace(")", "&#41;</span>", $result["Zohar"]['text_hebrew']);
        $result["Zohar"]['text_hebrew'] =   str_replace('"', "''", $result["Zohar"]['text_hebrew']);
        $result["Zohar"]['text_hebrew'] =   str_replace("<span class='normal_text_par'>&#40;\xd7\xa0''\xd7\x90", "<span class='nun_aleph_par'>&#40;\xd7\xa0''\xd7\x90", $result["Zohar"]['text_hebrew']);//nun"aleph
        $result["Zohar"]['text_hebrew'] =   str_replace("<span class='normal_text_par'>&#40;\xd7\xa1''\xd7\x90", "<span class='samech_aleph_par'>&#40;\xd7\xa1''\xd7\x90", $result["Zohar"]['text_hebrew']); //samech"aleph
        $result["Zohar"]['text_hebrew'] =   str_replace("<span class='normal_text_par'>&#40;\xd7\x93\xd7\xa3", "<span class='dalet_pei_par'>&#40;\xd7\x93\xd7\xa3", $result["Zohar"]['text_hebrew']); //dalet pei


        return $result;
    }

    function get_next_pinchas($zohar_id,$limit = 2,$exclude = false){

        $condition = ($exclude)?'>':'>=';

        $rows = $this->find("all",
            [
                'conditions' =>
                    [
                        'Zohar.id '.$condition => $zohar_id,
                        'Parasha.slug' => 'pinchas',
                    ],
                'limit' => $limit,
            ]
        );
        if(empty($rows)){
            //start cycle
            $rows = $this->find("all",
                [
                    'conditions' =>
                        [
                            'Parasha.slug' => 'pinchas',
                            'Zohar.paragraph_num' => 1,
                        ],
                    'limit' => $limit,
                ]
            );
        }
        $results = [];
        $parasha_id = isset($rows[0]["Parasha"]["id"])?$rows[0]["Parasha"]["id"]:0;

        foreach ($rows as &$result) {
            if($parasha_id == $result["Parasha"]['id']){
                $result["Zohar"]["text_hebrew"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_hebrew"]);
                $result["Zohar"]["text_aramaic"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_aramaic"]);
                $result["Zohar"]['transliterated_hebrew'] = TransliterateComponent::transliterate($result["Zohar"]['text_hebrew']);
                $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate($result["Zohar"]['text_aramaic']);
                $results[] = $result;
            }
        }
        return $results;
    }


    function get_next_pinchas_sync($limit = 2,$update = true){
        $last_paragraph = false;
        $this->PinchasCycle = ClassRegistry::init("PinchasCycle");
        $last = $this->PinchasCycle->find("first",[
            'order' => 'PinchasCycle.id DESC'
        ]);

        $rows = $this->find("all",
            [
                'conditions' =>
                    [
                        'Zohar.id >'=> $last["PinchasCycle"]["zohar_id"],
                        'Parasha.slug' => 'pinchas',
                    ],
                'limit' => $limit,
            ]
        );

        if(empty($rows)){
            //start cycle
            $rows = $this->find("all",
                [
                    'conditions' =>
                        [
                            'Parasha.slug' => 'pinchas',
                            'Zohar.paragraph_num >=' => 1,
                        ],
                    'limit' => $limit,
                ]
            );
            $last_paragraph = true;
        }
        if(isset($rows[count($rows)-1]) && $update){
            $this->PinchasCycle->id = $last["PinchasCycle"]["id"];
            $this->PinchasCycle->saveField("zohar_id",$rows[count($rows)-1]["Zohar"]["id"]);

        }
        $results = [];
        $parasha_id = isset($rows[0]["Parasha"]["id"])?$rows[0]["Parasha"]["id"]:0;

        foreach ($rows as &$result) {
            if($parasha_id == $result["Parasha"]['id']){
                $result["Zohar"]["text_hebrew"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_hebrew"]);
                $result["Zohar"]["text_aramaic"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_aramaic"]);
                $result["Zohar"]['transliterated_hebrew'] = TransliterateComponent::transliterate($result["Zohar"]['text_hebrew']);
                $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate($result["Zohar"]['text_aramaic']);
                if(is_file(WWW_ROOT."audio".DS.$result["Parasha"]["audio_name"]."-".sprintf("%03d", $result['Zohar']["paragraph_num"]).".mp3" )){
                    $result['Zohar']["audio"] = Router::url("/audio/",true).$result["Parasha"]["audio_name"]."-".sprintf("%03d", $result['Zohar']["paragraph_num"]).".mp3";
                    //$result['Zohar']["audio"] = str_replace("http","https",$result['Zohar']["audio"]);
                }else{
                        $result['Zohar']["audio"] = 0;
                }
                $results[] = $result;
            }
        }
        return array('result' => $results,'last_paragraph' => $last_paragraph);
    }



    function get_next_tikunim($zohar_id,$limit = 2,$exclude = false){

        $condition = ($exclude)?'>':'>=';

        $rows = $this->find("all",
            [
                'conditions' =>
                    [
                        'Zohar.id '.$condition => $zohar_id,
                        'Parasha.slug' => 'tikunei-zohar',
                    ],
                'limit' => $limit,
            ]
        );
        $results = [];
        $parasha_id = isset($rows[0]["Parasha"]["id"])?$rows[0]["Parasha"]["id"]:0;

        foreach ($rows as &$result) {
            if($parasha_id == $result["Parasha"]['id']){
                $result["Zohar"]["text_hebrew"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_hebrew"]);
                $result["Zohar"]["text_aramaic"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_aramaic"]);
                $result["Zohar"]['transliterated_hebrew'] = TransliterateComponent::transliterate($result["Zohar"]['text_hebrew']);
                $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate($result["Zohar"]['text_aramaic']);
                $results[] = $result;
            }
        }
        return $results;
    }

    function get_next_tikunim_sync($limit = 2,$update = true){
        $last_paragraph = false;
        $this->TikunimCycle = ClassRegistry::init("TikunimCycle");
        $last = $this->TikunimCycle->find("first",[
            'order' => 'TikunimCycle.id DESC'
        ]);
        $rows = $this->find("all",
            [
                'conditions' =>
                    [
                        'Zohar.id >' => $last["TikunimCycle"]["zohar_id"],
                        'Parasha.slug' => 'tikunei-zohar',
                    ],
                'limit' => $limit,
            ]
        );
        if(empty($rows)){
            //start cycle
            $rows = $this->find("all",
                [
                    'conditions' =>
                        [
                            'Parasha.slug' => 'tikunei-zohar',
                            'Zohar.paragraph_num >=' => 1,
                        ],
                    'limit' => $limit,
                ]
            );
            $last_paragraph = true;
        }
        if(isset($rows[count($rows)-1]) && $update){
            $this->TikunimCycle->id = $last["TikunimCycle"]["id"];
            $this->TikunimCycle->saveField("zohar_id",$rows[count($rows)-1]["Zohar"]["id"]);

        }
        $results = [];
        $parasha_id = isset($rows[0]["Parasha"]["id"])?$rows[0]["Parasha"]["id"]:0;

        foreach ($rows as &$result) {
            if($parasha_id == $result["Parasha"]['id']){
                $result["Zohar"]["text_hebrew"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_hebrew"]);
                $result["Zohar"]["text_aramaic"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_aramaic"]);
                $result["Zohar"]['transliterated_hebrew'] = TransliterateComponent::transliterate(strip_tags($result["Zohar"]['text_hebrew']));
                $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate(strip_tags($result["Zohar"]['text_aramaic']));
                if(is_file(WWW_ROOT."audio".DS.$result["Parasha"]["audio_name"].DS."-".sprintf("%03d", $result['Zohar']["paragraph_num"]).".mp3" )){
                    $result['Zohar']["audio"] = Router::url("/audio/",true).$result["Parasha"]["audio_name"]."-".sprintf("%03d", $result['Zohar']["paragraph_num"]).".mp3";
                    $result['Zohar']["audio"] = str_replace("http","https",$result['Zohar']["audio"]);
                }else{
                    $result['Zohar']["audio"] = 0;
                }
                $results[] = $result;
            }
        }
        return array('result' => $results,'last_paragraph' => $last_paragraph);
    }




    function get_percent($parasha_id,$paragraph){
        $this->recursive = 1;
        $total = $this->find("count",[
            'conditions' => [
                'Parasha.active' => 1
            ]
        ]);
        $this->recursive = -1;
        $parasha = $this->Parasha->findById($parasha_id);
        /*$sql ="SELECT count(Zohar.id) AS count FROM unity_zohar.zohars AS Zohar
              LEFT JOIN
                unity_zohar.parashot AS Parasha ON (Zohar.parasha_id = Parasha.id)
              WHERE Parasha.id in (select p.id from parashot p where p.order <= ".$parasha['Parasha']['order'].") and Zohar.paragraph_num < '".$paragraph."'
              ORDER BY Parasha.order ASC, Zohar.paragraph_num";*/

        $sql = "SELECT count(Zohar.id) AS count FROM unity_zohar.zohars AS Zohar
                  LEFT JOIN
                    unity_zohar.parashot AS Parasha ON (Zohar.parasha_id = Parasha.id)
                  WHERE Parasha.id in (select p.id from parashot p where p.order < ".$parasha['Parasha']['order'].")
                UNION
                SELECT count(Zohar.id) AS count FROM unity_zohar.zohars AS Zohar
                  WHERE Zohar.parasha_id = ".$parasha['Parasha']['id']." and Zohar.paragraph_num < '".$paragraph."'";


        $rows = $this->query($sql);
        $tc = $rows[0][0]['count'];
        if(isset($rows[1][0]['count'])){
            $tc += $rows[1][0]['count'];
        }
        $percent = ($tc) / $total * 100;
        return round($percent,2);
    }
    function get_pinchas_percent($zohar_id){
        $this->recursive = 1;
        $total = $this->find("count",[
            'conditions' => [
                'Parasha.slug' => 'pinchas'
            ]
        ]);
        $rows = $this->find("count",
            [
                'conditions' =>
                    [
                        'Zohar.id <=' => $zohar_id,
                        'Parasha.slug' => 'pinchas',
                    ],
            ]
        );
        $percent = $rows / $total * 100;
        return round($percent,2);
    }
    function get_tikunim_percent($zohar_id){
        $this->recursive = 1;
        $total = $this->find("count",[
            'conditions' => [
                'Parasha.slug' => 'tikunei-zohar'
            ]
        ]);
        $rows = $this->find("count",
            [
                'conditions' =>
                    [
                        'Zohar.id <=' => $zohar_id,
                        'Parasha.slug' => 'tikunei-zohar',
                    ],
            ]
        );
        $percent = $rows / $total * 100;
        return round($percent,2);
    }

    function get_paragraphs($params = []){
        $defaults = [
            'min_paragraph' => 0,
            'limit' => 3,
            'Parasha.id' => 1,
            'reverse' => false
        ];
        $defaults = array_merge($defaults,$params);

        $conditions = [
            'Parasha.id' => $defaults["Parasha.id"]
        ];
        if($defaults["reverse"]){
            $conditions['Zohar.paragraph_num <'] = $defaults["min_paragraph"];
            $order = "Zohar.paragraph_num DESC";
        }else{
            $conditions['Zohar.paragraph_num >'] = $defaults["min_paragraph"];
            $order = "Zohar.paragraph_num ASC";
        }

        $this->recursive = 1;
        $this->unbindModel(['hasMany'=> ['ZoharCycle']]);
        $paragraphs = $this->find("all",[
            'conditions' => $conditions,
            'limit' => $defaults["limit"],
            'order' => $order
        ]);

        if(empty($paragraphs)){//next or prev book
            $parasha = $this->Parasha->findById($defaults["Parasha.id"]);
            $neighbours = $this->Parasha->find('neighbors', array('field'=>'order', 'value'=>$parasha['Parasha']['order'], "conditions" => ['active' => true]));
            if(empty($neighbours["prev"]) && $defaults["reverse"]){//primero y atras
                $new_parasha = $this->Parasha->get_last();
                $defaults["min_paragraph"] = 100000;
                $conditions['Zohar.paragraph_num <'] = 100000;
            }elseif(empty($neighbours["next"]) && !$defaults["reverse"]){//ultimo y adelante
                $new_parasha = $this->Parasha->get_first();
                $defaults["min_paragraph"] = 0;
                $conditions['Zohar.paragraph_num >'] = 0;
            }else{
                if(!$defaults["reverse"]){
                    $new_parasha = $neighbours["next"];
                    $conditions['Zohar.paragraph_num >'] = 0;
                    $order = "Zohar.paragraph_num ASC";
                }else{
                    $new_parasha = $neighbours["prev"];
                    $conditions['Zohar.paragraph_num <'] = 100000;
                    $order = "Zohar.paragraph_num DESC";
                }
            }

            $conditions["Parasha.id"] = $new_parasha["Parasha"]["id"];
            $paragraphs = $this->find("all",[
                'conditions' => $conditions,
                'limit' => $defaults["limit"],
                'order' => $order
            ]);

        }

        if($defaults["reverse"]){
            $paragraphs = Hash::sort($paragraphs, '{n}.Zohar.paragraph_num', 'asc','numeric');
        }

        foreach ($paragraphs as &$result) {
            $result["Zohar"]["text_hebrew"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_hebrew"]);
            $result["Zohar"]["text_aramaic"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_aramaic"]);
            $result["Zohar"]['transliterated_hebrew'] = TransliterateComponent::transliterate(strip_tags($result["Zohar"]['text_hebrew']));
            $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate(strip_tags($result["Zohar"]['text_aramaic']));
            if (is_file(WWW_ROOT . "audio" . DS . $result["Parasha"]["audio_name"] . "-" . sprintf("%03d", $result['Zohar']["paragraph_num"]) . ".mp3")) {
                $result['Zohar']["audio"] = Router::url("/audio/", true) . $result["Parasha"]["audio_name"] . "-" . sprintf("%03d", $result['Zohar']["paragraph_num"]) . ".mp3";
                $result['Zohar']["audio"] = str_replace("http", "https", $result['Zohar']["audio"]);
            } else {
                $result['Zohar']["audio"] = 0;
            }
        }
        return $paragraphs;



    }



}