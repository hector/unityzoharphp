<?php
App::uses('CakeEventListener', 'Event');

class EventListener implements CakeEventListener {

    public function implementedEvents() {
        return array(
            'Model.Event.visited' => 'tzadik_visited',
            'Model.Comment.created' => 'comment_created',
        );
    }



    public function tzadik_visited(CakeEvent $event) {
        $EventC = ClassRegistry::init('Event');
        $EventC->updateAll(
            array(
                    'Event.hits'=>'Event.hits+1',
                    'Event.last_visit' =>"'".date("Y-m-d H:i:s")."'",
            ),
            array('Event.id'=>$event->data["id"])
        );
    }

    public function comment_created(CakeEvent $event) {
        if(!isset($event->data["data"]["approved"])){
            $comment = ClassRegistry::init('Comment')->findById($event->data["data"]["id"]);
            $Email = new CakeEmail();
            $Email->from(array('noreply@tzadikim.com' => 'Tzadikim'));
            $Email->to("znefesh@gmail.com");
            //$Email->to("hectormotesinos@gmail.com");
            $Email->subject('Tzadikim new comment waiting for approval');
            $Email->template('new_comment')
                ->emailFormat('html');
            $Email->viewVars(array('comment' =>$comment));
            $Email->send();
        }
    }


}