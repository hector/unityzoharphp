<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * CakePHP Component & Model Code Completion
 *
 * ==============================================
 * CakePHP Core Components
 * ==============================================
 * @property AuthComponent $Auth
 * @property AclComponent $Acl
 * @property CookieComponent $Cookie
 * @property EmailComponent $Email
 * @property RequestHandlerComponent $RequestHandler
 * @property SecurityComponent $Security
 * @property SessionComponent $Session
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    public $components = array('DebugKit.Toolbar','Session','Auth','Paginator',
                                'RequestHandler','Headers','SessionReader','Location','Transliterate',"Stripe");

    public function beforeFilter() {
        $this->Auth->loginAction = array('controller' => 'users', 'action' => 'login', 'admin' => true);
        $this->Auth->logoutRedirect = array('controller' => 'users', 'action' => 'login','admin' => true);
        $this->Auth->loginRedirect = array('controller' => 'pages', 'action' => 'index', 'admin' => true);
        $this->Auth->unauthorizedRedirect = array('controller' => 'users', 'action' => 'login', 'admin' => true);
        $this->Auth->authError = __("You must log in");

        if(isset($this->params['prefix']) && $this->params['prefix']=='admin'){
            $this->Auth->authenticate = array(
                'all' => array (
                    'scope' => array('User.active' => 1)
                ),
                'Form'
            );
            $this->Auth->authorize = 'Controller';
            $this->layout = 'admin';
        }elseif(isset($this->params['prefix']) && $this->params['prefix']=='api'){
            $this->Auth->unauthorizedRedirect = false;
            $this->Auth->authenticate = array(
                'all' => array (
                    'scope' => array('User.active' => 1)
                ),
                'Basic' => [
                    'scope' => ['User.role' => 'developer']
                ],
            );


        }

        if(!isset($this->params['prefix'])){
            $this->Auth->allow();
            $this->forceSSL();
        }
    }

    function isAuthorized($user) {
        if (($this->params['prefix'] === 'admin') && (!in_array($user['role'],['admin','editor']))) {
            $this->redirect(Router::url('/admin/users/login',true));
        }
        if (($this->params['prefix'] === 'api') && ($user['role'] != 'developer')) {
            $this->redirect(Router::url('/api/auth',true));
        }
        if(in_array($user['role'],['editor'])){
            if(!in_array($this->params['controller'],['zohar','pages','parashot','articles','user'])){
                if($this->params['controller'] =='users' && in_array($this->params['action'],['admin_logout','admin_edit_profile','admin_change_password'])){
                    return true;
                }
                return false;
            }
        }

        return true;
    }

    function _search(){
        if(isset($this->params['url']['search']))
            $this->request->data["search"] = $this->params['url']['search'];
        elseif(isset($this->params['named']['search']))
            $this->request->data["search"] = $this->params['named']['search'];
        else $this->request->data["search"] = '';
        return $this->request->data["search"];
    }

    public function forceSSL() {
        if(!isset($_SERVER['HTTPS']) && (isset($_SERVER['SERVER_NAME']) && strpos($_SERVER['SERVER_NAME'],'dev') !==0)){
            $this->redirect('https://' . $_SERVER['SERVER_NAME'] . $this->here);
        }
    }


    function _getReaders(){
        $readers = 0;
        $this->UnitySession = ClassRegistry::init("UnitySession");
        $readers += $this->UnitySession->getReaders();
        $sessions = $this->UnitySession->findById($this->Session->id());
        if(count($sessions) == 0){
            $readers++;
        }
        if($readers == 0){
            $readers++;
        }
        return $readers;
    }

}
