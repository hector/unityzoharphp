<?php
    class ArticlesController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Article.id' => 'ASC'
        )
    );

    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("Article.id = '$search' OR
                                Article.title like '%$search%' OR
                                Parasha.name like '%$search%'

            ");
        }
        $this->Article->recursive = 1;
        $rows = $this->Paginator->paginate('Article',$conditions);
        $this->set(compact('rows'));
    }
        public function admin_add() {
            if ($this->request->is('post')) {
                $this->Article->create();
                if ($this->Article->save($this->request->data)) {
                    $this->Session->setFlash(__('The Article has been saved'),
                        'default',
                        array('class' => 'alert alert-success'));
                    return $this->redirect(array('action' => 'index'));
                }
                $this->Session->setFlash(
                    __('The Article could not be saved. Please, try again.'),
                    'default',
                    array('class' => 'alert alert-danger'));
            }
            $parashot = $this->Article->Parasha->find("list",array('fields'=>array('id','name'),'order'=>'Parasha.order ASC'));
            $this->set(compact('parashot'));
        }
    public function admin_edit($id){

        $this->Article->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->Article->read();
        } else {
            $this->request->data["Article"]['id'] = $this->Article->id;
            if ($this->Article->save($this->request->data)) {
                $this->Session->setFlash(__("Article saved!"),'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__("Article saved!"),'default', array('class' => 'alert alert-danger'));
            }
        }
        $parashot = $this->Article->Parasha->find("list",array('fields'=>array('id','name'),'order'=>'Parasha.order ASC'));
        $this->set(compact('parashot'));

        /*$books = $this->Bible->BibleBook->find("list",array('fields'=>array('id','name'),'order'=>'BibleBook.name ASC'));
        $this->set(compact('books'));*/
    }

    public function api_get() {
        $books = $this->Bible->find_all($this->request->query);
        $this->set(array(
            'books' => $books,
            '_serialize' => array('books')
        ));
    }

}