<?php
class CandlesController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Candle.created' => 'DESC'
        )
    );


    public function admin_index() {

        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("Candle.id = '$search' OR
                                Candle.name like '%$search%' OR
                                Candle.parent_name like '%$search%'
                                ");
        }

        $rows = $this->Paginator->paginate('Candle',$conditions);
        //pr($rows);
        $this->set(compact('rows'));
    }
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Candle->create();
            $this->request->data["Candle"]["active"] ="1";
            if(empty($this->request->data["Candle"]["from"])){
                $this->request->data["Candle"]["from"] = date("Y-m-d");
            }
            if ($this->Candle->save($this->request->data)) {
                $this->Session->setFlash(__('The Candle has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Candle could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }

        $users = $this->Candle->User->find("list",array('conditions'=>array('User.role'=>'userapp'),'fields'=>array('id','full_name')));
        $this->set(compact('users'));
    }
    public function admin_edit($id) {
        if ($this->request->is('put')) {

            $this->request->data["Candle"]['id'] = $id;

            if(empty($this->request->data["Candle"]["from"])){
                $this->request->data["Candle"]["from"] = date("Y-m-d");
            }

            if ($this->Candle->save($this->request->data)) {
                $this->Session->setFlash(__('The Candle has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Candle could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }elseif($this->request->is('get')){
            $this->request->data = $this->Candle->findById($id);
            $this->request->data["Candle"]['id'] = $id;
        }

        $users = $this->Candle->User->find("list",array('conditions'=>array('User.role'=>'userapp'),'fields'=>array('id','full_name')));
        $this->set(compact('users'));
    }


    function index(){

        if(isset($this->request->query["invoice"])){
            $id = $this->request->query["invoice"];
            $p = $this->Candle->Payment->findByInvoice($id);
            if(!empty($p)){
                $this->Candle->recursive = -1;
                $c = $this->Candle->findByInvoice($id);
                if(!empty($c)){
                    if($c["Candle"]["active"] != 1) {
                        $this->Candle->id = $c["Candle"]["id"];
                        $this->Candle->saveField("active", "1");
                        $this->Session->setFlash(__('The Candle has been saved and is now active'),
                            'default',
                            array('class' => 'alert alert-success'));
                    }
                }
            }
        }
        /*if($this->Session->read('Auth.User.role')!='userapp'){
            $this->Session->write("redirect",Router::url("/candles/",true));
            $this->redirect(Router::url('/users/login',true));

        }*/

        $this->Candle->Payment->setSource('payments_values');
        $this->Candle->Payment->recursive = -1;
        $prices = $this->Candle->Payment->find("all");
        $this->Candle->recursive = -1;
        $candles = $this->Candle->find("all",array(
            'conditions' => array(
                "Candle.user_id" => $this->Auth->user("id"),
                'Candle.from + INTERVAL Candle.donation DAY >= current_date',
            )
        ));
        $title_for_layout = "Healing List";
        $this->set(compact('prices','title_for_layout','candles'));
    }


    function save_web(){

        $result = array("saved" => 0);
        if ($this->request->is("post")/* && $this->Auth->login()*/) {
            $pay = $this->pay_candle($this->request->data);
            if($pay=='0'){
                $result = array("saved" => 0,'error' => "Payment couldn't be created<br />There's probably an error in your credit card info, try again");
                die(json_encode($result));
            }elseif($pay == 1){
                $result = array("saved" => 0,'error' => "The Payment has been created but there's a failure trying to create the candle.<br /> Please contact to the site administration - 1");
                die(json_encode($result));
            }elseif(is_array($pay)) {
                if (!isset($this->request->data["gender"])) {
                    $gender = 'ben';
                }
                $from = date("Y-m-d");
                $data = array('Candle' =>
                    array(
                        'user_id' => $pay["User"]["id"],
                        'donation' => $this->request->data["Candle"]["donation"],
                        'sponsor_type' => $this->request->data["Candle"]["sponsor_type"],
                        'name' => $this->request->data["Candle"]["name"],
                        'parent_name' => $this->request->data["Candle"]["parent_name"],
                        'gender' => $gender,
                        'from' => $from,
                        'payment_id' => $pay["Payment"]["id"],
                    )
                );
                if ($this->Candle->save($data)) {
                    $result = array('saved' => 1);
                    $this->Candle->received_email($this->Candle->id);
                } else {
                    $result = array('saved' => 0,'error' => "The Payment has been created but there's a failure trying to create the candle.<br /> Please contact to the site administration - 2");
                }
                die(json_encode($result));
            }
        }
        die(json_encode($result));
    }

    function pay_candle($data){
        $payment = $this->Stripe->create_payment($data);
        $user_id = $this->Auth->user("id");
        if(!$user_id){
            $user_id = 298;
        }
        if(!empty($payment)){
            $pay = [
                "Payment" => [
                    'user_id' => $user_id,
                    "payment_time" => date("Y-m-d H:i:s",strtotime($payment->created)),
                    "state" => $payment->status,
                    "payment_id" => $payment->id,
                    "amount" => floatval($payment->amount/100)
                ]
            ];
            if($this->Candle->Payment->save($pay)){
                $pay = $this->Candle->Payment->read();
                return $pay;
            }else{
                return 1;
            }
        }else{
            return 0;
        }
    }

    function free_candle(){
        if($this->request->is("post") && $this->Auth->login()){
            $last_candle = $this->Candle->find("first",
                array('conditions'=>
                    array(
                        'Candle.user_id'=>$this->Auth->user('id'),
                        'Candle.payment_id' => "-1"
                    ),
                    'order' => 'from DESC'
                ));
            if(!empty($last_candle)) {
                $last_time = strtotime($last_candle["Candle"]["from"]) + (10 * 24 * 60 * 60);
                if ($last_time >= time()) {
                    $result = array("saved" => 0, 'error' => "The Candle cannot be saved, you already have a free candle");
                    die(json_encode($result));
                }
            }

            if (!isset($this->request->data["gender"])) {
                $gender = 'ben';
            }
            $from = date("Y-m-d");
            $data = array('Candle' =>
                array(
                    'user_id' => $this->Auth->user("id"),
                    'donation' => "10",
                    'sponsor_type' => $this->request->data["Candle"]["sponsor_type"],
                    'name' => $this->request->data["Candle"]["name"],
                    'parent_name' => $this->request->data["Candle"]["parent_name"],
                    'gender' => $gender,
                    'from' => $from,
                    'payment_id' => "-1",
                )
            );
            if ($this->Candle->save($data)) {
                $result = array('saved' => 1);
            } else {
                $result = array('saved' => 0,'error' => "The candle couldn't be created, try again");
            }
            die(json_encode($result));
        }
        $result = array('saved' => 0,'error' => "You are no logged in");
        die(json_encode($result));
    }


    function api_get_candles(){
        $candles = $this->Candle->get_by_event();
        $candles = Hash::combine($candles,'{n}.Candle.id', '{n}','{n}.Candle.sponsor_type');
        $this->set(array(
            'candles' => $candles,
            '_serialize' => array('candles')
        ));
    }

    function api_get_prices(){
        $this->Candle->Payment->setSource('payments_values');
        $this->Candle->Payment->recursive = -1;
        $prices = $this->Candle->Payment->find("all",array('order' => array('Payment.days'),));
        $this->set(array(
            'prices' => $prices,
            '_serialize' => array('prices')
        ));
    }

    function api_buy_candle(){
        $result = array("saved" => 0);
        if (!isset($this->request->data["Candle"]["gender"]) || empty($this->request->data["Candle"]["gender"])) {
                           $gender = 'ben';

                       }elseif($this->request->data["Candle"]["gender"] == 'male'){
                           $gender = "bat";
                       }else{
                           $gender = "ben";
       }

        if ($this->request->is("post") && $this->Auth->login()) {
            $this->Candle->Payment->setSource('payments_values');
            $this->Candle->Payment->recursive = -1;
            $prices = $this->Candle->Payment->find("all");
            //$this->log(print_r($prices,true),'info');
            foreach ($prices as $price) {
                if($price["Payment"]["days"] == $this->request->data["Candle"]["donation"]){
                    $this->request->data["cc"]["amount"] = $price["Payment"]["price"];
                    $this->request->data["cc"]["text"] = "UZ app candle";
                    break;
                }
            }
            $this->Candle->Payment->setSource('payments');
           // $this->log(print_r($this->request->data,true),'info');
            $pay = $this->pay_candle($this->request->data);

            //$this->log(print_r($pay,true),'info');

            if($pay=='0'){
                $result = array("saved" => 0,'error' => "Payment couldn't be created<br />There's probably an error in your credit card info, try again");
                die(json_encode($result));
            }elseif($pay == 1){
                $result = array("saved" => 0,'error' => "The Payment has been created but there's a failure trying to create the candle.<br /> Please contact to the site administration - 1");
                die(json_encode($result));
            }elseif(is_array($pay)) {
                $user = $this->Auth->user();
                if (!isset($this->request->data["Candle"]["gender"]) || empty($this->request->data["Candle"]["gender"])) {
                    $gender = 'ben';

                }elseif($this->request->data["Candle"]["gender"] == 'male'){
                    $gender = "bat";
                }else{
                    $gender = "ben";
                }
                $this->log(print_r($gender,true),'debug');
                $from = date("Y-m-d");
                $data = array('Candle' =>
                    array(
                        'user_id' => $user["id"],
                        'donation' => $this->request->data["Candle"]["donation"],
                        'sponsor_type' => $this->request->data["Candle"]["sponsor_type"],
                        'name' => $this->request->data["Candle"]["name"],
                        'parent_name' => $this->request->data["Candle"]["parent_name"],
                        'gender' => $gender,
                        'from' => $from,
                        'payment_id' => $pay["Payment"]["id"],
                    )
                );
                if ($this->Candle->save($data)) {
                    $result = array('saved' => 1);
                } else {
                    $result = array('saved' => 0,'error' => "The Payment has been created but there's a failure trying to create the candle.<br /> Please contact to the site administration - 2");
                }
                die(json_encode($result));
            }
        }
        die(json_encode($result));
    }

    function api_donation(){
        $result = array("saved" => 0);
        //$this->log("holaaaa",'info');
        if ($this->request->is("post") && $this->Auth->login()) {
            $this->request->data["cc"]["text"] = "Unity Zohar app Donation";
            $pay = $this->pay_candle($this->request->data);

            $this->log(print_r($pay,true),'info');

            if($pay=='0'){
                $result = array("saved" => 0,'error' => "Payment couldn't be created<br />There's probably an error in your credit card info, try again");
                die(json_encode($result));
            }elseif($pay == 1){
                $result = array("saved" => 0,'error' => "The Payment has been created but there's a failure trying to create the candle.<br /> Please contact to the site administration - 1");
                die(json_encode($result));
            }elseif(is_array($pay)) {
                $result = array('saved' => 1);
                die(json_encode($result));
            }
        }
        die(json_encode($result));
    }

    function api_register_candle(){
        $data = $this->request->data['data'];
        $user = $this->Auth->user();
        $from = date("Y-m-d");
        $data = ['Candle' => [
            'user_id' => $user["id"],
            'donation' => $data["donation"],
            'sponsor_type' => $data["type"],
            'name' => $data["name"],
            'parent_name' => $data["parent_name"],
            'gender' => $data["gender"],
            'from' => $from,
            'payment_id' => '0',
        ]
        ];
        if ($this->Candle->save($data)) {
            $result = array('saved' => 1);
            $this->Candle->received_email($this->Candle->id, $data["email"] ?? '');
        } else {
            $result = array('saved' => 0);
        }
        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }
}
