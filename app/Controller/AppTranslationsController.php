<?php
    class AppTranslationsController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'AppTranslations.id' => 'ASC'
        )
    );

    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("TranslationString.id = '$search' OR
                                TranslationString.string like '%$search%'");
        }
        $rows = $this->Paginator->paginate('TranslationString',$conditions);
        $this->set(compact('rows'));
//pr($rows);

//        $strings =  $this->AppTranslation->TranslationString->find("list",array('fields'=>array('id','string'),'order'=>'TranslationString.string ASC'));
//        $languages =  $this->AppTranslation->Language->find("list",
//            array('conditions'=>['Language.apply_app' => 1],
//                'fields'=>array('id','language'),'order'=>'Language.language ASC'));
//        $this->set(compact('strings','languages'));

    }

    public function admin_add() {
//        if ($this->request->is('post')) {
//            $this->AppTranslation->create();
//            if ($this->AppTranslation->save($this->request->data)) {
//                $this->Session->setFlash(__('The translation has been saved'),
//                    'default',
//                    array('class' => 'alert alert-success'));
//                return $this->redirect(array('action' => 'index'));
//            }
//            $this->Session->setFlash(
//                __('The translation could not be saved. Please, try again.'),
//                'default',
//                array('class' => 'alert alert-danger'));
//        }
//        $strings =  $this->AppTranslation->TranslationString->find("list",array('fields'=>array('id','string'),'order'=>'TranslationString.string ASC'));
//        $languages =  $this->AppTranslation->Language->find("list",
//                        array('conditions'=>['Language.apply_app' => 1],
//                        'fields'=>array('id','language'),'order'=>'Language.language ASC'));
//        $this->set(compact('strings','languages'));
    }
    public function admin_edit($string_id){

        $string = $this->AppTranslation->TranslationString->findById($string_id);
        $neighbors = $this->AppTranslation->TranslationString->find('neighbors',['field' => 'id', 'value' => $string_id]);
        $translated = [];
        if ($this->request->is('get')) {
            $translated = $this->AppTranslation->find('all',['conditions' =>['AppTranslation.translation_string_id'=>$string_id]]);
        } else {
            foreach ($this->request->data["AppTranslation"] as $translation) {
                $id = isset($translation["id"])?$translation["id"]:null;
                $translations = [
                    'AppTranslation' => [
                        'id' => $id,
                        'language_id' => $translation["language_id"],
                        'translation' => $translation["translation"],
                        'translation_string_id' => $this->request->data["TranslationString"]["id"],
                    ]
                ];
                if(!$this->AppTranslation->save($translations)){
                    $this->Session->setFlash(
                        __('The translation could not be saved. Please, try again.'),
                        'default',
                        array('class' => 'alert alert-danger'));
                }
            }
            $this->Session->setFlash(__('The translations has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
            if($this->request->data['option'] == 'save-continue' ?? null){
                $id_next = $neighbors['next']['TranslationString']['id'] ?? 1;
                return $this->redirect(array('action' => 'edit',$id_next));
            }else{
                return $this->redirect(array('action' => 'index'));
            }


        }
        $languages =  $this->AppTranslation->Language->find("list",
            array('conditions'=>['Language.apply_app' => 1],
                    'fields'=>array('id','language'),
                    'order'=>'Language.language ASC')
        );
        $this->set(compact('string','languages','translated','neighbors'));
    }

    public function admin_how_it_works() {
        $string = $this->AppTranslation->TranslationString->findByString("HOW_IT_WORKS_HTML");
        $translated = [];
        if ($this->request->is('get')) {
            $translated = $this->AppTranslation->find('all',['conditions' =>['AppTranslation.translation_string_id'=>$string["TranslationString"]["id"]]]);
        } else {
            foreach ($this->request->data["AppTranslation"] as $translation) {
                $id = isset($translation["id"])?$translation["id"]:null;
                $translations = [
                    'AppTranslation' => [
                        'id' => $id,
                        'language_id' => $translation["language_id"],
                        'translation' => $translation["translation"],
                        'translation_string_id' => $this->request->data["TranslationString"]["id"],
                    ]
                ];
                if(!$this->AppTranslation->save($translations)){
                    $this->Session->setFlash(
                        __('The translation could not be saved. Please, try again.'),
                        'default',
                        array('class' => 'alert alert-danger'));
                }
            }
            $this->Session->setFlash(__('The translations has been saved'),
                'default',
                array('class' => 'alert alert-success'));

            return $this->redirect(array('action' => 'admin_how_it_works'));

        }
        $languages =  $this->AppTranslation->Language->find("list",
            array('conditions'=>['Language.apply_app' => 1],
                'fields'=>array('id','language'),
                'order'=>'Language.language ASC')
        );
        $this->set(compact('string','languages','translated'));
    }

    public function api_get($lang = 'en') {
        $strings = Cache::read('translations_'.$lang, 'minute15');
        if (!$strings) {
            $strings = $this->AppTranslation->find("all",
                ['conditions' => [
                    'Language.abbreviation' => $lang
                ]
            ]
            );
            Cache::write('translations', $strings, 'minute15');
        }

//        $strings = $this->AppTranslation->find("all",
//            ['conditions' => [
//                    'Language.abbreviation' => $lang
//                ]
//            ]
//        );
        $this->set(array(
            'strings' => $strings
        ));
    }

}