<?php
    class UnityTextController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'UnityText.id' => 'ASC'
        )
    );

    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("UnityText.id = '$search'");
        }

        $rows = $this->Paginator->paginate('UnityText',$conditions);
        $this->set(compact('rows'));

    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->UnityText->create();
            if ($this->UnityText->save($this->request->data)) {
                $this->Session->setFlash(__('The Text has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The text could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }
    }

    public function admin_edit($id){

        $this->UnityText->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->UnityText->read();
        } else {
            $this->request->data["UnityText"]['id'] = $this->UnityText->id;
            if ($this->UnityText->save($this->request->data)) {
                $this->Session->setFlash(__("text saved!"),'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__("text not saved!"),'default', array('class' => 'alert alert-danger'));
            }
        }
    }

    function api_get_text(){
        $text = $this->request->query["text"];
        $text = $this->UnityText->findByAbbreviation($text);

        $this->set(array(
            'text' => $text,
            '_serialize' => array('text')
        ));
    }
}//class