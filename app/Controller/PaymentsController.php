<?php
class PaymentsController extends AppController {
    var $paginate = array(
        'limit' => 10,
        'order' => array(
            'Payment.created' => 'DESC'
        )
    );

    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("(Payment.id = '$search' OR
                                Payment.payment_id LIKE '%$search%' OR
                                User.name LIKE '%$search%' OR
                                User.lastname LIKE '%$search%' OR
                                Candle.id LIKE '%$search%' OR
                                User.email LIKE '%$search%')
                                ");
        }


        $rows = $this->Paginator->paginate('Payment',$conditions);
        $this->set(compact("rows"));
    }

    public function admin_content() {
        if ($this->request->is('post')) {

            if ($this->Payment->save_options($this->request->data)) {
                $this->Session->setFlash(__('The Payments values has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'content'));
            }
            $this->Session->setFlash(
                __('The Payments values could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }
        $this->Payment->setSource('payments_values');
        $this->Payment->recursive = -1;
        $payments = $this->Payment->find("all");
        $this->set(compact('payments'));
        //pr($payments);

    }

    function ipn(){
        if($this->request->is("post")){
            if(isset($this->request->data["txn_id"])){
                $payment = $this->Payment->findByPaymentId($this->request->data["txn_id"]);
                if(empty($payment)){
                    $data = array(
                        'Payment'=>array(
                            'user_id' => 0,
                            'event_id' => 0,
                            'payment_id' => $this->request->data["txn_id"],
                            'state' => $this->request->data["payment_status"],
                            'payment_time' => date("Y-m-d h:i:s",strtotime($this->request->data["payment_date"])),
                            'environment' => $this->request->data["txn_type"],
                            'platform' => "web",
                            'invoice' => $this->request->data["invoice"],
                        )
                    );
                    $result = $this->Payment->save($data);
                    if($this->request->data["payment_status"]=='Completed') {
                        if ($result) {
                            $id = $this->Payment->getLastInsertId();
                            $this->Payment->recursive = 0;
                            $payment = $this->Payment->findById($id);
                            $this->Payment->Candle->recursive = -1;
                            $c = $this->Payment->Candle->findByInvoice($payment["Payment"]["invoice"]);
                            $this->Payment->Candle->id = $c["Candle"]["id"];
                            $this->Payment->Candle->saveField("active", "1");
                            $this->Payment->Candle->saveField("payment_id", $payment["Payment"]["id"]);
                            $this->Payment->id = $payment["Payment"]["id"];
                            $this->Payment->saveField("user_id", $c["Candle"]["user_id"]);
                            $this->Payment->received_email($c["Candle"]["user_id"], $payment["Payment"]["id"]);
                            $this->log("payment saved\n", 'info');
                            $this->log(print_r($payment, true), 'info');

                        } else {
                            $this->log("payment not saved1\n", 'info');
                            $this->log(print_r($this->request->data, true), 'info');

                        }
                    }
                }
            }else{
                $this->log("payment not saved2\n",'info');
                $this->log(print_r($this->request->data,true),'info');
            }
        }else{
            $this->log("payment not saved3\n",'info');
            $this->log(print_r($this->request->data,true),'info');
        }
        $this->log(print_r($this->request->data,true),'info');
        die(1);
    }

    function ipn_members(){
        if($this->request->is("post")){
            if(isset($this->request->data["txn_id"])){
                $payment = $this->Payment->findByPaymentId($this->request->data["txn_id"]);
                if(empty($payment)){
                    $data = array(
                        'Payment'=>array(
                            'user_id' => 0,
                            'event_id' => 0,
                            'payment_id' => $this->request->data["txn_id"],
                            'state' => $this->request->data["payment_status"],
                            'payment_time' => date("Y-m-d h:i:s",strtotime($this->request->data["payment_date"])),
                            'environment' => $this->request->data["txn_type"],
                            'platform' => "web",
                            'invoice' => $this->request->data["invoice"],
                        )
                    );
                    $result = $this->Payment->save($data);
                    if($this->request->data["payment_status"]=='Completed') {
                        if ($result) {
                            $id = $this->Payment->getLastInsertId();
                            $this->Payment->recursive = 0;
                            $payment = $this->Payment->findById($id);
                            $this->Payment->User->recursive = -1;
                            $u = $this->Payment->User->findByInvoice($payment["Payment"]["invoice"]);
                            $this->Payment->id = $payment["Payment"]["id"];
                            $this->Payment->saveField("user_id", $u["User"]["id"]);

                            $this->Payment->User->id = $u["User"]["id"];
                            $this->Payment->User->saveField("membership_active", "1");

                            $this->Payment->received_email($u["User"]["id"], $payment["Payment"]["id"]);

                            $this->log("payment saved\n", 'info');
                            $this->log(print_r($payment, true), 'info');
                        } else {
                            $this->log("payment not saved1\n", 'info');
                            $this->log(print_r($this->request->data, true), 'info');

                        }
                    }
                }
            }else{
                $this->log("payment not saved2\n",'info');
                $this->log(print_r($this->request->data,true),'info');
            }
        }else{
            $this->log("payment not saved3\n",'info');
            $this->log(print_r($this->request->data,true),'info');
        }
        $this->log(print_r($this->request->data,true),'info');
        die(1);
    }


    function verify_invoice($invoice){
        $payment = $this->Payment->findByInvoice($invoice);
        if(!empty($payment)){
            echo json_encode(array('payment'=>$payment));
        }else{
            echo json_encode(array('payment'=>'0'));
        }
        exit;
    }

    function admin_get_paypal_data($payment_id){
        if($this->request->is('ajax')){
            $ag = $this->Paypal->get_payment($payment_id);
            $ag = $ag->toJSON(128);
            die($ag);
        }else{
            die("fuck off");
        }
    }
}//class