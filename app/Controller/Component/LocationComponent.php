<?php
App::uses('Component', 'Controller');
class LocationComponent extends Component {

    function get($ip = '127.0.0.1'){
        //$ip = "72.2.82.213";
        if($ip == '127.0.0.1' || $ip == '172.17.1.29'){
            return [];
        }
        $client = new GeoIp2\WebService\Client(Configure::read("MaxMind.id"), Configure::read("MaxMind.key"));
        $record = $client->city($ip);
        return [
            'ip_address'    => $ip,
            'lng'           => $record->location->longitude,
            'lat'           => $record->location->latitude,
            'country'       => $record->country->name,
            'country_iso'   => $record->country->isoCode,
            'state'          => $record->mostSpecificSubdivision->name,
            'city'          => $record->city->name,

        ];
    }


    static function partition( $list, $p ) {
        $listlen = count( $list );
        $partlen = floor( $listlen / $p );
        $partrem = $listlen % $p;
        $partition = array();
        $mark = 0;
        for ($px = 0; $px < $p; $px++) {
            $incr = ($px < $partrem) ? $partlen + 1 : $partlen;
            $partition[$px] = array_slice( $list, $mark, $incr );
            $mark += $incr;
        }
        return $partition;
    }
}
