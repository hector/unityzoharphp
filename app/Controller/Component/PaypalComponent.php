<?php
App::uses('Component', 'Controller');

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Currency;
use PayPal\Api\ChargeModel;
use PayPal\Api\Agreement;
use PayPal\Api\Payer;
use PayPal\Api\ShippingAddress;
use PayPal\Api\PayerInfo;
use PayPal\Api\CreditCard;
use PayPal\Api\FundingInstrument;
use PayPal\Api\PatchRequest;
use PayPal\Api\Patch;
use PayPal\Common\PayPalModel;
use PayPal\Api\AgreementStateDescriptor;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\Payment;


class PaypalComponent extends Component
{
    public $components = array('PayFlow');

    function getApiContext()
    {
        $clientId = Configure::read("PayPal.client_id");
        $clientSecret = Configure::read("PayPal.client_secret");
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $clientId,
                $clientSecret
            )
        );

        $apiContext->setConfig(
            array(
                'mode' => Configure::read("PayPal.mode"),
                'log.LogEnabled' => true,
                'log.FileName' => 'PayPal.log',
                'log.LogLevel' => 'DEBUG', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                'validation.level' => 'log',
                'cache.enabled' => true,
                // 'http.CURLOPT_CONNECTTIMEOUT' => 30
                // 'http.headers.PayPal-Partner-Attribution-Id' => '123123123'
            )
        );
        return $apiContext;
    }




    function create_plan($data = []){

        $apiContext = self::getApiContext();
        $plan = new Plan();
        $plan->setName('Unity Zohar Membership')
            ->setDescription($data["description"])
            ->setType('INFINITE');

        // # Payment definitions for this billing plan.
        $paymentDefinition = new PaymentDefinition();
        $paymentDefinition->setName('Unity Zohar payments')
            ->setType('REGULAR')
            ->setFrequency('Month')
            ->setFrequencyInterval("1")
            ->setAmount(new Currency(array('value' => $data["amount"], 'currency' => 'USD')));

        $merchantPreferences = new MerchantPreferences();
        $baseUrl = "http://unityzohar.com";

        $merchantPreferences
            ->setReturnUrl("$baseUrl")
            ->setCancelUrl("$baseUrl")
            ->setAutoBillAmount("yes")
            ->setInitialFailAmountAction("CONTINUE")
            ->setMaxFailAttempts("0");

        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        try {
            $plan->create($apiContext);

        } catch (Exception $ex) {
            $this->log("***************************Plan not created", "debug");
            $this->log(print_r($plan,true), "debug");
            return false;
        }

        return $plan;

    }

    function active_plan($plan){
        $apiContext = self::getApiContext();

        try {
            $patch = new Patch();

            $value = new PayPalModel('{
	            "state":"ACTIVE"
	        }');

            $patch->setOp('replace')
                ->setPath('/')
                ->setValue($value);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);
            $plan->update($patchRequest, $apiContext);
            $plan = Plan::get($plan->getId(), $apiContext);

        } catch (Exception $ex) {
            $this->log("***************************Plan not Activated", "debug");
            $this->log(print_r($plan,true), "debug");
            return false;
        }
        return $plan;

    }

    function get_plan($plan_id){
        $apiContext = self::getApiContext();
        try {
            $plan = Plan::get($plan_id, $apiContext);
        } catch (Exception $ex) {
            $this->log("***************************Cannot get plan", "debug");
            exit(1);
        }
        pr($plan);
        return $plan;
    }

    function create_billing($plan_id,$data = []){

        $apiContext = self::getApiContext();
        $agreement = new Agreement();
        $agreement->setName('Billing agreement for user: '.$data["first_name"].' '.$data["last_name"].'('.$data["email"].')')
            ->setDescription('Unity Zohar membership agreement')
            ->setStartDate(date('c',strtotime("+1 minute")));

        // Add Plan ID
        // Please note that the plan Id should be only set in this case.
        $plan = new Plan();
        $plan->setId($plan_id);
        $agreement->setPlan($plan);

        $payer = new Payer();
        $payer->setPaymentMethod('credit_card')
            ->setPayerInfo(new PayerInfo(array('email' => $data["email"],'first_name'=>$data["first_name"],"last_name"=>$data["last_name"])));

        // Add Credit Card to Funding Instruments
        $creditCard = new CreditCard();

        $creditCard->setType($data["cc"]["type"])
            ->setNumber($data["cc"]["number"])
            ->setExpireMonth($data["cc"]["expire_month"])
            ->setExpireYear($data["cc"]["expire_year"])
            ->setCvv2($data["cc"]["cvv2"]);

        $fundingInstrument = new FundingInstrument();
        $fundingInstrument->setCreditCard($creditCard);
        $payer->setFundingInstruments(array($fundingInstrument));
        //Add Payer to Agreement
        $agreement->setPayer($payer);
        try {
            // Please note that as the agreement has not yet activated, we wont be receiving the ID just yet.
            $agreement = $agreement->create($apiContext);
        } catch (Exception $ex) {
            $this->log("***************************Agreement not created", "debug");
            $this->log(print_r($agreement,true), "debug");
            return false;
        }
        return $agreement;
    }


    function get_agreement($agreement_id){
        $apiContext = self::getApiContext();

        try {
            $agreement = Agreement::get($agreement_id, $apiContext);
        } catch (Exception $ex) {
            return false;
        }
        return $agreement;
    }

    function suspend_agreement($agreement_id){
        $apiContext = self::getApiContext();
        $agreement = Agreement::get($agreement_id, $apiContext);
        $agreementStateDescriptor = new AgreementStateDescriptor();
        $agreementStateDescriptor->setNote("Suspending the agreement");
        try {
            $agreement->suspend($agreementStateDescriptor, $apiContext);
            // Lets get the updated Agreement Object
            $agreement = Agreement::get($agreement->getId(), $apiContext);

        } catch (Exception $ex) {
            $this->log("***************************Agreement not suspended", "debug");
            $this->log(print_r($agreement,true), "debug");
            return false;

        }
        return $agreement;
    }

    function get_plans($page = 0){

        $apiContext = self::getApiContext();

        try {
            // Get the list of all plans
            // You can modify different params to change the return list.
            // The explanation about each pagination information could be found here
            // at https://developer.paypal.com/webapps/developer/docs/api/#list-plans
            $params = array('page_size' => '20','page'=>$page,'status'=>'ACTIVE','total_required' => 'yes');
            $planList = Plan::all($params, $apiContext);
        } catch (Exception $ex) {
            pr($ex);
            exit(1);
        }

        pr($planList);

        return $planList;
    }

    function create_payment($data){
        $apiContext = self::getApiContext();
        $card = new CreditCard();
        $card->setType($data["type"])
            ->setNumber($data["number"])
            ->setExpireMonth($data["expire_month"])
            ->setExpireYear($data["expire_year"])
            ->setCvv2($data["cvv"])
            ->setFirstName($data["card_name"])
            ->setLastName("");

        $fi = new FundingInstrument();
        $fi->setCreditCard($card);

        $payer = new Payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));

        $item1 = new Item();
        $item1->setName('Unity Zohar Candle')
            ->setDescription($data["text"])
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setTax(0)
            ->setPrice($data["amount"]);
        $itemList = new ItemList();
        $itemList->setItems(array($item1));


        $amount = new Amount();
        $amount->setCurrency("USD")
            ->setTotal($data["amount"]);

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Unity Zohar Candle Payment")
            ->setInvoiceNumber(uniqid());

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));


        try {
            $payment->create($apiContext);
        } catch (Exception $ex) {
            $this->log("***************************Payment not created", "debug");
            $this->log(print_r($ex->getData(),true), "debug");
            return false;
        }
        return $payment;
    }


    function get_payment($payment_id){
        $apiContext = self::getApiContext();
        try {
            $payment = Payment::get($payment_id, $apiContext);
        } catch (Exception $ex) {
            return false;
        }
        return $payment;
    }


    function create_recurring_payment($data){


        $this->PayFlow->setEnvironment(Configure::read("PayFlow.environment"));
        $this->PayFlow->setTransactionType('R');
        $this->PayFlow->setPaymentMethod('C');
        $this->PayFlow->setPaymentCurrency('USD');

        $this->PayFlow->setProfileAction('A');
        $this->PayFlow->setProfileName('UNITY_MEMBERSHIP');
        $this->PayFlow->setProfileStartDate(date('mdY', strtotime("+1 days")));
        $this->PayFlow->setProfilePayPeriod('MONT');
        $this->PayFlow->setProfileTerm(0);
        $this->PayFlow->setAmount($data["amount"], FALSE);
        $this->PayFlow->setOption("A");
        $this->PayFlow->setCCNumber($data["cc"]["number"]);
        $this->PayFlow->setCVV($data["cc"]["cvv2"]);
        $this->PayFlow->setExpiration($data["cc"]["expire_month"].$data["cc"]["expire_year"]);
        $this->PayFlow->setCreditCardName($data["cc"]["card_name"]);
        $this->PayFlow->setCustomerFirstName($data["first_name"]);
        $this->PayFlow->setCustomerLastName($data["last_name"]);
        $this->PayFlow->setCustomerEmail($data["email"]);
        $this->PayFlow->setPaymentComment('Membership - '.$data["description"]);
        if($this->PayFlow->processTransaction()){
            return $this->PayFlow->getResponse();
        }else{
            //print_r($this->PayFlow->debugNVP('array'));
            //print_r($this->PayFlow->getResponse());
            return false;
        }
        unset($PayFlow);
    }


    function suspend_agreement_payflow($agreement_id){

        $this->PayFlow->setEnvironment(Configure::read("PayFlow.environment"));
        $this->PayFlow->setTransactionType('R');
        $this->PayFlow->setProfileAction('C');
        $this->PayFlow->setProfileId($agreement_id);
        if($this->PayFlow->processTransaction()){
            return $this->PayFlow->getResponse();
        }else{
            return false;
        }

        unset($PayFlow);

    }
}
