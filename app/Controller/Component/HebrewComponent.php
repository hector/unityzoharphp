<?php
App::uses('Component', 'Controller');
class HebrewComponent extends Component {

    static $jewishMonthNamesLeap = array(
            1 => 'Tishri',
            2 => 'Heshvan',
            3 => 'Kislev',
            4 => 'Tevet',
            5 => 'Shevat',
            6 => 'Adar',
            7 => 'Adar',
            8 => 'Nisan',
            9 => 'Iyyar',
            10 => 'Sivan',
            11 => 'Tammuz',
            12 => 'Av',
            13 => 'Elul',
    );
    static  $jewishMonthNamesNonLeap = array(

            1 => 'Tishri',
            2 => 'Heshvan',
            3 => 'Kislev',
            4 => 'Tevet',
            5 => 'Shevat',
            6 => 'Adar',
            7 => 'Adar',
            8 => 'Nisan',
            9 => 'Iyyar',
            10 => 'Sivan',
            11 => 'Tammuz',
            12 => 'Av',
            13 => 'Elul',
    );
    static $dbOrder = array(
            7 => 'Tishri',
            8 => 'Heshvan',
            9 => 'Kislev',
            10=> 'Tevet',
            11=> 'Shevat',
            12=> 'Adar',
            1 => 'Nisan',
            2 => 'Iyyar',
            3 => 'Sivan',
            4 => 'Tammuz',
            5 => 'Av',
            6 => 'Elul',
    );


    static function isJewishLeapYear($year) {
        if ($year % 19 == 0 || $year % 19 == 3 || $year % 19 == 6 ||
            $year % 19 == 8 || $year % 19 == 11 || $year % 19 == 14 ||
            $year % 19 == 17)
            return true;
        else
            return false;
    }

    static function getJewishMonthName($jewishMonth, $jewishYear) {
        if (self::isJewishLeapYear($jewishYear))
            return self::$jewishMonthNamesLeap[$jewishMonth];
        else
            return self::$jewishMonthNamesNonLeap[$jewishMonth];
    }

    static function gregorianToJewish($day,$month,$year){

        $jdNumber = gregoriantojd($month, $day, $year);
        $jewishDate = jdtojewish($jdNumber);
        list($jewishMonth, $jewishDay, $jewishYear) = explode('/', $jewishDate);
        $jewishMonthName = self::getJewishMonthName($jewishMonth, $jewishYear);
        return array($jewishDay,$jewishMonthName,$jewishYear);
    }
	
	
	static function jewishToGregorian($day,$month){
        $hd = self::gregorianToJewish(date('d'),date('m'),date("Y"));
        $year = date("Y");
        $month = self::$dbOrder[$month];
        if(self::isJewishLeapYear($year)){
            $month = array_search($month, self::$jewishMonthNamesLeap);
        }else{
            $month = array_search($month, self::$jewishMonthNamesonLeap);
        }

        $jdNumber = jewishtojd($month,$day,$hd[2]);
		$gregorianDate = jdtogregorian($jdNumber);
		return explode('/', $gregorianDate);
	
	}

    static function fromDBtoPhp($date = array()){
        if(empty($date)){
            $day = date('j');
            $month = date('n');
            $year = date('Y');
        }else{
            $day = $date[0];
            $month =$date[1];
            $year = $date[2];
        }

        $hd = self::gregorianToJewish($day,$month,$year);
        $month = array_search($hd[1], self::$dbOrder);
        return array($hd[0],$month);
    }
}