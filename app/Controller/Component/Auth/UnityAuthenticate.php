<?php
App::uses('BaseAuthenticate', 'Controller/Component/Auth');

class UnityAuthenticate extends BaseAuthenticate {

    public function authenticate(CakeRequest $request, CakeResponse $response) {

        if(isset($request->query["_access_token"])){
            $token = $request->query["_access_token"];
            if($token) {
                $this->Token = ClassRegistry::init("Token");
                $user_token = $this->Token->findByToken($token);
                return $user_token["User"];
            }
        }
        return false;
    }

    public function getUser(CakeRequest $request) {

        if(isset($request->query["_access_token"])){

            $token = $request->query["_access_token"];
            if($token){
                $this->Token = ClassRegistry::init("Token");
                $user_token = $this->Token->findByToken($token);
                if(!empty($user_token)){
                    return $this->_findUser($user_token["User"]["email"]);
                }
            }
        }
        return false;
    }

}