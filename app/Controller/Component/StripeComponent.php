<?php
App::uses('Component', 'Controller');



class StripeComponent extends Component
{
    var $_api_key;

    function __construct(){
        $this->_api_key = Configure::read("Stripe.api_key");
        \Stripe\Stripe::setApiKey($this->_api_key);
    }

    function getApiKey()
    {
        return Configure::read("Stripe.api_key");
    }

    function createToken($cc = null){
        $token = "";
        try {
            $token = \Stripe\Token::create(array(
                "card" => array(
                    "number" =>$cc["number"],
                    "exp_month" =>$cc["expire_month"],
                    "exp_year" =>$cc["expire_year"],
                    "cvc" =>$cc["cvv"],
                    //"number" => "4242424242424242",
//                    "exp_month" => 8,
//                    "exp_year" => 2018,
//                    "cvc" => "314"
                )
            ));

        } catch (Exception $ex) {
            $this->log("***************************Token not created", "debug");
            $this->log(print_r($ex->getMessage(),true), "debug");
            return false;
        }


        return $token;
    }

    function create_payment($data)
    {
        $token =  $this->createToken($data["cc"]);
        if(!$token){
            return false;
        }

        try{
            $payment = \Stripe\Charge::create(array(
                "amount" => floatval($data["cc"]["amount"]) * 100,
                "currency" => "usd",
                "source" => $token,
                "description" => $data["cc"]["text"]
            ));
        }catch (Exception $ex){
            $this->log("***************************Payment not created", "debug");
            $this->log(print_r($ex->getMessage(),true), "debug");
            return false;
        }

        return $payment;

    }

    function get_payment($payment_id){
    }



}
