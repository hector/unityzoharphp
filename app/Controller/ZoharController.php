<?php
    class ZoharController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Parasha.order' => 'ASC'
        )
    );

    public function admin_index() {

        if($this->request->is('post')){
            $conditions = [
                'parasha_id' => $this->data["Zohar"]["parasha_id"],
                'paragraph_num' => $this->data["Zohar"]["paragraph_num"],
            ];
            $this->Zohar->recursive = -1;
            $zohar = $this->Zohar->find("first",['conditions'=>$conditions]);
            if(empty($zohar)){
                $this->Session->setFlash(__('Zohar paragraph not found'),
                    'default',
                    array('class' => 'alert alert-danger'));
                return $this->redirect(array('action' => 'index'));
            }else{
                return $this->redirect(array('action' => 'edit',$zohar["Zohar"]['id']));
            }
        }

        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("Zohar.id = '$search' OR
                    Parasha.name like '%$search%'
            ");
        }

        $rows = $this->Paginator->paginate('Zohar',$conditions);
        $parashot = $this->Zohar->Parasha->find("list",array('fields'=>array('id','name'),'order'=>'Parasha.order ASC'));
        $this->set(compact('rows','parashot'));

    }
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Zohar->create();
            if ($this->Zohar->save($this->request->data)) {
                $this->Session->setFlash(__('The Zohar has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Zohar could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }
        $parashot = $this->Zohar->Parasha->find("list",array('fields'=>array('id','name'),'order'=>'Parasha.order ASC'));
        $this->set(compact('parashot'));
    }
    public function admin_edit($id){

        if($this->request->is('post') && isset($this->data["nav"])){
            $conditions = [
                'parasha_id' => $this->data["Zohar"]["parasha_id"],
                'paragraph_num' => $this->data["Zohar"]["paragraph_num"],
            ];
            $this->Zohar->recursive = -1;
            $zohar = $this->Zohar->find("first",['conditions'=>$conditions]);
            if(empty($zohar)){
                $this->Session->setFlash(__('Zohar paragraph not found'),
                    'default',
                    array('class' => 'alert alert-danger'));
                return $this->redirect(array('action' => 'index'));
            }else{
                return $this->redirect(array('action' => 'edit',$zohar["Zohar"]['id']));
            }
        }


        $this->Zohar->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->Zohar->read();
        } else {
            $this->request->data["Zohar"]['id'] = $this->Zohar->id;
            if ($this->Zohar->save($this->request->data)) {
                $this->Session->setFlash(__("Zohar saved!"),'default', array('class' => 'alert alert-success'));
                $this->request->data = $this->Zohar->read();
                //$this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__("Zohar not saved!"),'default', array('class' => 'alert alert-danger'));
            }
        }


        $parashot = $this->Zohar->Parasha->find("list",array('fields'=>array('id','name'),'order'=>'Parasha.order ASC'));
        $this->set(compact('parashot'));
    }

    function admin_next($parasha_id,$paragraph_num){
        $paragraph_num++;
        $this->Zohar->recursive = -1;
        $zohar = $this->Zohar->find('first',[
           'conditions' => [
               'Zohar.parasha_id' => $parasha_id,
               'Zohar.paragraph_num' => $paragraph_num,
           ]
        ]);

        if(empty($zohar)){
            $parashot = $this->Zohar->Parasha->find("list",array('fields'=>array('id','name'),'order'=>'Parasha.order ASC'));
            $parashot =  array_keys($parashot);
            $next = 0;
            foreach ($parashot as $p) {
                if($p == $parasha_id){
                    $next = current($parashot);
                    break;
                }
            }
            $this->Zohar->recursive = -1;
            $zohar = $this->Zohar->find('first',[
                'conditions' => [
                    'Zohar.parasha_id' => $next,
                ],
                'order' => 'Zohar.paragraph_num ASC'
            ]);
            if(empty($zohar)){
                $this->Session->setFlash(__("Zohar not found!"),'default', array('class' => 'alert alert-danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }
        return $this->redirect(array('action' => 'edit',$zohar["Zohar"]['id']));
    }

    function admin_prev($parasha_id,$paragraph_num){
        $paragraph_num--;
        $this->Zohar->recursive = -1;
        $zohar = $this->Zohar->find('first',[
            'conditions' => [
                'Zohar.parasha_id' => $parasha_id,
                'Zohar.paragraph_num' => $paragraph_num,
            ]
        ]);

        if(empty($zohar)){
            $parashot = $this->Zohar->Parasha->find("list",array('fields'=>array('id','name'),'order'=>'Parasha.order ASC'));
            $parashot =  array_keys($parashot);
            $next = 0;
            foreach ($parashot as $p) {
                if($p == $parasha_id){
                    $next = prev($parashot);
                    $next = prev($parashot);
                    if($next === false){
                        $next = array_pop($parashot);
                    }
                    break;
                }
            }
            $this->Zohar->recursive = -1;
            $zohar = $this->Zohar->find('first',[
                'conditions' => [
                    'Zohar.parasha_id' => $next,
                ],
                'order' => 'Zohar.paragraph_num DESC'
            ]);

            if(empty($zohar)){
                $this->Session->setFlash(__("Zohar not found!"),'default', array('class' => 'alert alert-danger'));
                return $this->redirect(array('action' => 'index'));
            }
        }


        return $this->redirect(array('action' => 'edit',$zohar["Zohar"]['id']));
    }


    function admin_translation($zohar_id,$languaje_id = 1){

        if ($this->request->is('post') || $this->request->is('put')) {
            $this->Zohar->Translation->create();

            $this->request->data["Translation"]["zohar_id"] = $zohar_id;
            $this->request->data["Translation"]["user_id"] = $this->Auth->user('id');
            //pr($this->request->data);exit;
            if ($this->Zohar->Translation->save($this->request->data)) {
                $this->Session->setFlash(__('The Translation has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Translation could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }elseif($this->request->is('get')){
            $data = $this->Zohar->Translation->findByZoharIdAndLanguageId($zohar_id,$languaje_id);
            if(empty($data)){
                $this->request->data["Translation"]['zohar_id'] = $zohar_id;
                $this->request->data["Translation"]['language_id'] = $languaje_id;
            }else{
                $this->request->data = $data;
            }


            //pr($this->request->data);
        }

        $this->Language = ClassRegistry::init('Language');
        $zohar = $this->Zohar->findById($zohar_id);
        $languages = $this->Language->find('list',[
            'fields' => ['id','language']
        ]);


        $this->set(compact('zohar','languages'));
    }

    public function api_get() {
        $texts = $this->Zohar->find_all($this->request->query);
        $this->set(array(
            'texts' => $texts,
            '_serialize' => array('texts')
        ));
    }


    function api_next(){
        $this->log(print_r($_SERVER,true), "debug");
        $limit = 2;
        if(isset($this->request->query["limit"])){
            $limit = $this->request->query["limit"];
        }

        if(isset($this->request->query["_access_token"])){
            $cycle = $this->Zohar->ZoharCycle->find_by_session(time());
        }else{
            $cycle = $this->Zohar->ZoharCycle->find_by_session($this->Session->id());
        }

        $this->log(print_r($this->Session->id(),true), "debug");

       // $cycle = $this->Zohar->ZoharCycle->find_by_session($this->Session->id(),$limit);
        if(empty($cycle['ZoharCycle']["ip_address"])){
            $this->log("empty cycle", "debug");
            $location = $this->Location->get($this->request->clientIp());
            if(!empty($location)){
                $this->Zohar->ZoharCycle->update_location($this->Session->id(),$location);
            }
        }
        $zohars2 = $this->Zohar->get_next_sync($limit);
        $zohars["Paragraphs"] = $zohars2["result"];
        $zohars["Percent"] = $this->Zohar->get_percent($cycle["ZoharCycle"]["parasha_id"],$cycle["ZoharCycle"]["paragraph"]);
        $zohars["Cycle"] = $cycle["ZoharCycle"]["cycle"];

        $this->Zohar->ZoharCycle->recursive = -1;
        $lv = $this->Zohar->ZoharCycle->find("first",[
            'conditions' => [
                'ip_address <>' =>NULL,
                'ip_address <>' =>"",
                'ip_address <>' =>"",
                'ip_address <>' =>$this->request->clientIp(),

            ],'order' => "id DESC"
        ]);
        $zohars["LastVisitor"] = [];
        if(!empty($lv)) {
            $zohars["LastVisitor"] = [
                'city' => $lv["ZoharCycle"]["city"],
                'country' => $lv["ZoharCycle"]["country"],
                'country_iso' => strtolower($lv["ZoharCycle"]["country_iso"]),
            ];
        }
        $zohars["Parasha"] = $zohars2["result"][0]["Parasha"];
        $zohars["Readers"] = $this->_getReaders();
        $this->set(array(
            'zohars' => $zohars,
            '_serialize' => array('zohars')
        ));
    }

    function api_next_pinchas(){
        $limit = 2;
        $this->PinchasCycle = ClassRegistry::init("PinchasCycle");
        if(isset($this->request->query["_access_token"])){
            $cycle = $this->PinchasCycle->find_by_session(time());
        }else{
            $cycle = $this->PinchasCycle->find_by_session($this->Session->id());
        }


        if(empty($cycle['PinchasCycle']["ip_address"])){
            $location = $this->Location->get($this->request->clientIp());
            if(!empty($location)){
                $this->PinchasCycle->update_location($this->Session->id(),$location);
            }
        }
        $zohars2 = $this->Zohar->get_next_pinchas_sync();
        $zohars["Paragraphs"] = $zohars2["result"];
        $zohars["Percent"] = $this->Zohar->get_pinchas_percent($cycle["PinchasCycle"]["zohar_id"],'pinchas');
        $zohars["Cycle"] = $cycle["PinchasCycle"]["cycle"];
        $this->PinchasCycle->recursive = -1;
        $lv = $this->PinchasCycle->find("first",[
            'conditions' => [
                'ip_address <>' =>NULL,
                'ip_address <>' =>"",
                'ip_address <>' =>$this->request->clientIp(),

            ],'order' => "id DESC"
        ]);
        $zohars["LastVisitor"] = [];
        if(!empty($lv)) {
            $zohars["LastVisitor"] = [
                'city' => $lv["PinchasCycle"]["city"],
                'country' => $lv["PinchasCycle"]["country"],
                'country_iso' => strtolower($lv["PinchasCycle"]["country_iso"]),
            ];
        }
        $zohars["Readers"] = $this->_getReaders();
        $zohars["Parasha"] = $zohars2["result"][0]["Parasha"];
        $this->set(array(
            'zohars' => $zohars,
            '_serialize' => array('zohars')
        ));
    }

    function api_next_tikunim(){
        $limit = 2;
        $this->TikunimCycle = ClassRegistry::init("TikunimCycle");
        //$cycle = $this->TikunimCycle->find_by_session($this->Session->id());
        if(isset($this->request->query["_access_token"])){
            $cycle = $this->TikunimCycle->find_by_session(time());
        }else{
            $cycle = $this->TikunimCycle->find_by_session($this->Session->id());
        }


        if(empty($cycle['TikunimCycle']["ip_address"])){
            $location = $this->Location->get($this->request->clientIp());
            if(!empty($location)){
                $this->TikunimCycle->update_location($this->Session->id(),$location);
            }
        }
        $zohars2 = $this->Zohar->get_next_tikunim_sync();
        $zohars["Paragraphs"] = $zohars2["result"];
        $zohars["Percent"] = $this->Zohar->get_tikunim_percent($cycle["TikunimCycle"]["zohar_id"]);
        $zohars["Cycle"] = $cycle["TikunimCycle"]["cycle"];
        $this->TikunimCycle->recursive = -1;
        $lv = $this->TikunimCycle->find("first",[
            'conditions' => [
                'ip_address <>' =>NULL,
                'ip_address <>' =>"",
                'ip_address <>' =>$this->request->clientIp(),

            ],'order' => "id DESC"
        ]);
        $zohars["LastVisitor"] = [];
        if(!empty($lv)) {
            $zohars["LastVisitor"] = [
                'city' => $lv["TikunimCycle"]["city"],
                'country' =>  $lv["TikunimCycle"]["country"],
                'country_iso' => strtolower($lv["TikunimCycle"]["country_iso"]),
            ];
        }
        $zohars["Parasha"] = $zohars2["result"][0]["Parasha"];
        $zohars["Readers"] = $this->_getReaders();
        $this->set(array(
            'zohars' => $zohars,
            '_serialize' => array('zohars')
        ));
    }

    function api_next_paragraphs($id){

        $this->Zohar->recursive = -1;
        $paragraph = $this->Zohar->findById($id);
        $params = [
            'Parasha.id' => $paragraph["Zohar"]["parasha_id"],
            'min_paragraph' => $paragraph["Zohar"]["paragraph_num"]
        ];
        if(isset($this->request->query["reverse"])){
            $params["reverse"] = true;
        }
        $paragraphs = $this->Zohar->get_paragraphs($params);
        $parashot = $this->Zohar->Parasha->get_one($paragraphs[0]["Zohar"]["parasha_id"]);
        $parashot["paragraphs"] = $paragraphs;
        $this->set(array(
            'parashot' => $parashot,
            '_serialize' => array('parashot')
        ));
    }

    function api_go_to_paragraph(){
        $limit = 3;
        if(isset($this->request->query["limit"])){
            $limit = $this->request->query["limit"];
        }
        $lang = $this->request->query["_userlang"] ?? 'en';

        $book = $this->request->query['book'];
        $par_num = $this->request->query['paragraph'];
        $parashot = $this->Zohar->Parasha->get_one($book);
        $this->Zohar->unbindModel(['hasMany'=> ['ZoharCycle']]);
        $paragraphs = $this->Zohar->find("all",[
            'conditions' => [
                "Parasha.id" => $book,
                "Zohar.paragraph_num >=" => $par_num,
            ],
            "limit" => $limit,
            "order" => "Zohar.paragraph_num ASC"
        ]);
        foreach ($paragraphs as &$result) {
            $result["Zohar"]["text_hebrew"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_hebrew"]);
            $result["Zohar"]["text_aramaic"] = preg_replace('#\s*\(.+\)\s*#U', ' ', $result["Zohar"]["text_aramaic"]);
            $result["Zohar"]['transliterated_hebrew'] = TransliterateComponent::transliterate(strip_tags($result["Zohar"]['text_hebrew']));
            $result["Zohar"]['transliterated_aramaic'] = TransliterateComponent::transliterate(strip_tags($result["Zohar"]['text_aramaic']));
            if (is_file(WWW_ROOT . "audio" . DS . $result["Parasha"]["audio_name"] . "-" . sprintf("%03d", $result['Zohar']["paragraph_num"]) . ".mp3")) {
                $result['Zohar']["audio"] = Router::url("/audio/", true) . $result["Parasha"]["audio_name"] . "-" . sprintf("%03d", $result['Zohar']["paragraph_num"]) . ".mp3";
                $result['Zohar']["audio"] = str_replace("http", "https", $result['Zohar']["audio"]);
            } else {
                $result['Zohar']["audio"] = 0;
            }
            if(!empty($result['ZoharCommentary'])){
                $result['ZoharCommentary'] = array_filter($result['ZoharCommentary'], function($commentary) use ($lang) {
                    return $commentary['lang'] == $lang;
                });
                $result['ZoharCommentary']  = array_values($result['ZoharCommentary']) ?? [];
            }

            // $result['ZoharCommentary'] =
        }

        $parashot["paragraphs"] = $paragraphs;

        $this->set(array(
            'parashot' => $parashot,
            '_serialize' => array('parashot')
        ));

    }

    function api_stats(){


        $stats = false; Cache::read('stats', 'minute15');
        if (!$stats) {

            $this->PinchasCycle = ClassRegistry::init("PinchasCycle");
            $this->TikunimCycle = ClassRegistry::init("TikunimCycle");
            $tot_ever_days = 10000;

            $stats = [
                'last_day_readers' => $this->Zohar->ZoharCycle->get_connected_users() +
                    $this->PinchasCycle->get_connected_users() + $this->TikunimCycle->get_connected_users()
                ,
                'last_week_readers' => $this->Zohar->ZoharCycle->get_connected_users(7/*a week*/) +
                    $this->PinchasCycle->get_connected_users(7) + $this->TikunimCycle->get_connected_users(7)
                ,
                'last_day_paragraphs' => $this->Zohar->ZoharCycle->get_paragraphs_read() +
                    $this->PinchasCycle->get_paragraphs_read() + $this->TikunimCycle->get_paragraphs_read()
                ,
                'last_week_paragraphs' => $this->Zohar->ZoharCycle->get_paragraphs_read(7/*a week*/) +
                    $this->PinchasCycle->get_paragraphs_read(7) + $this->TikunimCycle->get_paragraphs_read(7)
                ,
                'last_month_paragraphs' => $this->Zohar->ZoharCycle->get_paragraphs_read(30/*a month*/) +
                    $this->PinchasCycle->get_paragraphs_read(30) + $this->TikunimCycle->get_paragraphs_read(30)
                ,
                'total_paragraphs' => $this->Zohar->ZoharCycle->get_paragraphs_read($tot_ever_days/*a month*/) +
                    $this->PinchasCycle->get_paragraphs_read($tot_ever_days) + $this->TikunimCycle->get_paragraphs_read($tot_ever_days)
                ,
                'top_countries' => $this->Zohar->ZoharCycle->get_top_countries(1, 10) +
                    $this->PinchasCycle->get_top_countries(1, 10) + $this->TikunimCycle->get_top_countries(1, 10)
                ,
            ];


            $token = null;
            if (isset($this->request->query["_access_token"])) {
                $token = $this->request->query["_access_token"];
                $this->Token = ClassRegistry::init("Token");

                $email = $this->Token->field(
                    'email',
                    array('Token.token' => $token)

                );
               // pr($email);
                $tokens = $this->Token->find('list', [
                    'conditions' => ['Token.email' => $email],
                    'fields' => 'Token.token'
                ]);
               // pr($tokens);
                $tot_par = 0;
                $p_par = $this->PinchasCycle->get_paragraphs_read(1, $tokens);
                $z_par = $this->Zohar->ZoharCycle->get_paragraphs_read(1, $tokens);
                $t_par = $this->TikunimCycle->get_paragraphs_read(1, $tokens);
                //pr($p_par);

                $tot_par = $p_par + $z_par + $t_par;

                $tot_ever = $this->PinchasCycle->get_paragraphs_read($tot_ever_days, $tokens) + $this->Zohar->ZoharCycle->get_paragraphs_read($tot_ever_days, $tokens) + $this->TikunimCycle->get_paragraphs_read($tot_ever_days, $tokens);
                $stats['my_total_paragraphs'] = $tot_ever;
                $stats['my_day_paragraphs'] = $tot_par;
                $stats['my_week_paragraphs'] = $this->Zohar->ZoharCycle->get_paragraphs_read(7, $tokens) +
                    $this->PinchasCycle->get_paragraphs_read(7, $tokens) + $this->TikunimCycle->get_paragraphs_read(7, $tokens);

                $stats['my_month_paragraphs'] = $this->Zohar->ZoharCycle->get_paragraphs_read(30, $tokens) +
                    $this->PinchasCycle->get_paragraphs_read(30, $tokens) + $this->TikunimCycle->get_paragraphs_read(30, $tokens);;
            }

            Cache::write('stats', $stats, 'minute15');
        }
        $this->set(array(
            'stats' => $stats,
            '_serialize' => array('stats')
        ));
    }
}