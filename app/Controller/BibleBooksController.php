<?php
    class BibleBooksController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'BibleBook.id' => 'ASC'
        )
    );

    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("BibleBook.id = '$search' OR
                                 BibleBook.name like '%$search%'
            ");
        }

        $rows = $this->Paginator->paginate('BibleBook',$conditions);
        $this->set(compact('rows'));

    }

    public function api_get($id = null) {

        if($id){
            $books = $this->BibleBook->findBySlugOrId($id,$id);
        }else{
            $books = $this->BibleBook->find("all");
        }

        $this->set(array(
            'books' => $books,
            '_serialize' => array('books')
        ));
    }

}