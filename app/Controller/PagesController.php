<?php
App::uses('AppController', 'Controller');
class PagesController extends AppController {

    var $uses = array("ZoharCycle","UnitySession","Parasha","Zohar","PinchasCycle",'TikunimCycle','PsalmsCycle','Bible');
    //static $types = ['zohar'=>0,'pinchas'=>"1"];
    public $helpers = array('Cache');
    public $cacheAction = array(
        'admin_index'  => 600
    );
    function home(){
        //$zohar_time = $this->UnitySession->get_time();
        //$pinchas_time = $this->UnitySession->get_time('pinchas');
        $text = ClassRegistry::init('UnityText')->find("first",['conditions'=>['location'=>'home'],'order'=>'RAND()']);
        $this->set(compact("text"/*,'zohar_time','pinchas_time'*/));

    }

    function unity_zohar(){
        $limit = 2;
        $title_for_layout = "Unity Zohar";
        $cycle = $this->ZoharCycle->find_by_session($this->Session->id(),$limit);
        if(empty($cycle['ZoharCycle']["ip_address"])){
            $location = $this->Location->get($this->request->clientIp());
            if(!empty($location)){
                $this->ZoharCycle->update_location($this->Session->id(),$location);
            }
        }
        $cycle = $this->ZoharCycle->find("first",[//last zohar readed
            'order' => 'ZoharCycle.id DESC'
        ]);

        $zohars = $this->Zohar->get_next_sync();
//exit;
        //$zohars = $this->Zohar->get_next($cycle["ZoharCycle"]["parasha_id"],$cycle["ZoharCycle"]["paragraph"],2);
        if($zohars["last_paragraph"]){
            $this->ZoharCycle->id = $cycle['ZoharCycle']["id"];
            $cn = $cycle['ZoharCycle']["cycle"]+1;
            $this->ZoharCycle->saveField("cycle",$cn);
        }
        $zohars = $zohars["result"];
        $parasha["Parasha"] = $zohars[0]["Parasha"];
        $percent = $this->Zohar->get_percent($cycle["ZoharCycle"]["parasha_id"],$cycle["ZoharCycle"]["paragraph"]);
        $languages = $this->Zohar->Translation->Language->find('list',[
            'fields' => ['id','language']
        ]);


        $candles = ClassRegistry::init("Candle")->get_by_event();
        $first = $candles;
        $first = array_slice($first, 0, 3);
        $candles = Hash::combine($candles,'{n}.Candle.id', '{n}','{n}.Candle.sponsor_type');
        $this->set(compact('cycle','parasha','zohars','percent','visitors','candles','first','title_for_layout','languages'));


    }

    function unity_pinchas(){
        $title_for_layout = "Unity Pinchas";
        $cycle = $this->PinchasCycle->find_by_session($this->Session->id());
        if(empty($cycle['PinchasCycle']["ip_address"])){
            $location = $this->Location->get($this->request->clientIp());
            if(!empty($location)){
                $this->PinchasCycle->update_location($this->Session->id(),$location);
            }
        }
        $zohars = $this->Zohar->get_next_pinchas_sync();
        $zohars = $zohars["result"];
        $parasha["Parasha"] = $zohars[0]["Parasha"];
        $cycle = $this->PinchasCycle->find("first",[//last zohar readed
            'order' => 'PinchasCycle.id DESC'
        ]);
        $percent = $this->Zohar->get_pinchas_percent($cycle["PinchasCycle"]["zohar_id"],'pinchas');
        $this->Candle = ClassRegistry::init("Candle");
        $first = $this->Candle->get_by_event('refua',3);
        $candles = $this->Candle->get_by_event();
        $candles = Hash::combine($candles,'{n}.Candle.id', '{n}','{n}.Candle.sponsor_type');
        //$first = LocationComponent::partition($first,3);
        //$candles = LocationComponent::partition($candles,3);
        $this->set(compact('cycle','parasha','zohars','percent','visitors','candles','first','title_for_layout'));
    }

    function unity_tikunim(){
        $title_for_layout = "Unity Tikunim";
        $cycle = $this->TikunimCycle->find_by_session($this->Session->id());
        if(empty($cycle['TikunimCycle']["ip_address"])){
            $location = $this->Location->get($this->request->clientIp());
            if(!empty($location)){
                $this->TikunimCycle->update_location($this->Session->id(),$location);
            }
        }
        $zohars = $this->Zohar->get_next_tikunim_sync();
        $zohars = $zohars["result"];
        $cycle = $this->TikunimCycle->find("first",[//last zohar readed
            'order' => 'TikunimCycle.id DESC'
        ]);
        $parasha["Parasha"] = $zohars[0]["Parasha"];
        $percent = $this->Zohar->get_tikunim_percent($cycle["TikunimCycle"]["zohar_id"]);
        $this->Candle = ClassRegistry::init("Candle");
        $first = $this->Candle->get_by_event('all',3);
        $candles = $this->Candle->get_by_event();
        $candles = Hash::combine($candles,'{n}.Candle.id', '{n}','{n}.Candle.sponsor_type');
        //$first = LocationComponent::partition($first,3);
        //$candles = LocationComponent::partition($candles,3);
        $this->set(compact('cycle','parasha','zohars','percent','visitors','candles','first','title_for_layout'));
    }

    function unity_psalms(){
        $limit = 13;
        $title_for_layout = "Unity Psalms";
        $cycle = $this->PsalmsCycle->find_by_session($this->Session->id(),$limit);
        if(empty($cycle['PsalmsCycle']["ip_address"])){
            $location = $this->Location->get($this->request->clientIp());
            if(!empty($location)){
                $this->PsalmsCycle->update_location($this->Session->id(),$location);
            }
        }
        $zohars = $this->Bible->get_next_psalms_sync();
        $cycle = $this->PsalmsCycle->find("first",[
            'order' => 'PsalmsCycle.id DESC'
        ]);
        $book["BibleBook"] = $zohars[0]["BibleBook"];
        $percent = $this->Bible->get_psalms_percent($cycle["PsalmsCycle"]["bible_id"]);
        $this->Candle = ClassRegistry::init("Candle");
        $first = $this->Candle->get_by_event('all',3);
        $candles = $this->Candle->get_by_event();
        $candles = Hash::combine($candles,'{n}.Candle.id', '{n}','{n}.Candle.sponsor_type');
        $this->set(compact('cycle','book','zohars','percent','candles','first','title_for_layout'));
    }

    function unity_books($slug = 'prologue',$paragraph = null){
        $limit = 3;
        /*if(!$this->Auth->user()){
            $this->Session->setFlash(__("You must be logged in to access this area"),'default', array('class' => 'alert alert-danger'));
            $this->Session->write("redirect",Router::url("/",true)."zohar-books");
            return $this->redirect(['controller'=>'users','action' => 'login']);
        }

        if($this->Auth->user() && $this->Auth->user("membership_id")=='0'){
            $this->Session->setFlash(__("<em>You must be a member to access to this area</em>"),'default', array('class' => ' text-center alert alert-danger'),'nomembership');
            return $this->redirect(['controller'=>'users','action' => 'membership']);
        }*/

        $parasha = $this->Parasha->findBySlug($slug);
        if(empty($parasha)){
            $this->redirect(Router::url("/zohar-books/prologue/".$slug));
        }
        $parashot = $this->Parasha->find("list",[
            'conditions'=>[
                'Parasha.active' => 1,
            ],
            'fields' => ['slug','name'],
            'order' => "Parasha.order"
        ]);
        $keys = array_keys($parashot);
        $last = "prologue";
        if($paragraph === "0" && !is_null($paragraph) && $slug !='prologue'){
            while($last != null){
                $last = next($keys);
                if($last == $slug){
                    $last = prev($keys);
                    break;
                }
            }
            $parasha = $this->Parasha->findBySlug($last);
            $total_par = $this->Parasha->get_total_paragraph($parasha["Parasha"]["id"])-$limit;
            $this->redirect(Router::url("/zohar-books/".$last."/".$total_par));
        }
        $condition = true;
        if(!$paragraph){
            $condition = false;
            $paragraph = 1;
        }
        $zohars = $this->Zohar->get_next($parasha["Parasha"]["id"],$paragraph,$limit,$condition);
        $zohars = $zohars["result"];
        if($zohars[0]["Parasha"]["slug"] != $slug && $paragraph > 0){
            $this->redirect(Router::url("/zohar-books/".$zohars[0]["Parasha"]["slug"]));
        }
        $total_par = $this->Parasha->get_total_paragraph($parasha["Parasha"]["id"]);
        $languages = $this->Zohar->Translation->Language->find('list',[
            'fields' => ['id','language']
        ]);
        $this->set(compact('parashot','zohars','parasha','total_par','languages'));
    }

    function unity_search(){
        if(!isset($this->request->query["search"])){
            $this->redirect(Router::url("/zohar-books",true));
        }

        $search = $this->_search();
        $search = addslashes($search);

        $this->Paginator->settings = [
                        'limit' => 3,
                        'order' => [
                            'Parasha.order' => 'ASC'
                        ],
                        'joins' => [
                            [
                                'alias' => 'Translation',
                                'type' => 'LEFT',
                                'table' => 'translations',
                                'conditions' => '`Translation`.`zohar_id` = `Zohar`.`id`'
                            ]
                        ],

        ];
        $conditions = array();
        if(!empty($search)){
            $conditions = array("Zohar.id = '$search' OR
                    Parasha.name like '%$search%' OR
                    Zohar.text_aramaic like '%$search%' OR
                    Zohar.text_hebrew like '%$search%' OR
                    Translation.text like '%$search%'
            ");
        }
        //$this->Zohar->recursive = 1;

        $zohars = $this->Paginator->paginate('Zohar',$conditions);
        $results = [];
        foreach ($zohars as &$result) {
                $result = $this->Zohar->format_zohar($result);
                $results[] = $result;
        }
        //pr($results);
        $zohars = $results;
        $languages = $this->Zohar->Translation->Language->find('list',[
            'fields' => ['id','language']
        ]);
        $this->set(compact('zohars','languages'));
    }
    function unity_readers(){

    }

    function admin_index(){
        if(isset($this->request->query["clear"])){
            Cache::clear();
        }


        $cycle = $this->ZoharCycle->find('first',[
            'order' => 'ZoharCycle.modified DESC'
        ]);
        $percent = $this->Zohar->get_percent($cycle["ZoharCycle"]["parasha_id"],$cycle["ZoharCycle"]["paragraph"]);

        $cycle = $this->PinchasCycle->find('first',[
            'order' => 'PinchasCycle.modified DESC'
        ]);
        $cyclec = $cycle["PinchasCycle"]["cycle"];
        $percentc = $this->Zohar->get_pinchas_percent($cycle["PinchasCycle"]["zohar_id"]);

        $zr = count($this->ZoharCycle->get_last_visitors());
        $pr = count($this->PinchasCycle->get_last_visitors());
        $cr = count($this->ZoharCycle->get_country_readers());
        $this->set(compact('percent','percentc','cyclec','zr','pr','cr'));
        $this->render('statistics');
    }

    public function api_index() {
        $this->render("admin_index");
    }

    function get_visitors($type= 'zohar',$limit = 0){


        if (empty($this->request->params['requested']) && !$this->request->is('ajax')) {
            //throw new ForbiddenException();
        }

        $offset = isset($this->request->query["start"])?$this->request->query["start"]:0;

        if($type=="zohar"){
            $visitors = $this->ZoharCycle->get_last_visitors($limit,$offset);
        }else{
            $visitors = $this->PinchasCycle->get_last_visitors();
        }
        if(isset($this->request->params['requested'])) {
            return $visitors;
        }
        if($this->request->is('ajax') || $this->request->params["ext"]=='json'){
            $this->set(array(
                'visitors' => $visitors,
                '_serialize' => array('visitors')
            ));
        }

    }

    function get_candles(){
        if (empty($this->request->params['requested']) && !$this->request->is('ajax')) {
            throw new ForbiddenException();
        }
        $limit = 90;
        $offset = 0;
        if(isset($this->request->params["named"]['limit'])){
            $limit = $this->request->params["named"]['limit'];
        }
        if(isset($this->request->params["named"]['offset'])){
            $offset = $this->request->params["named"]['offset'];
        }

        $candles = ClassRegistry::init("Candle")->get_by_event(['limit'=>$limit,'offset'=>$offset]);
        $candles = LocationComponent::partition($candles,3);

        if(isset($this->request->params['requested'])) {
            return $candles;
        }
        if($this->request->is('ajax')){
            $this->set(array(
                'candles' => $candles,
                '_serialize' => array('candles')
            ));
        }
    }

    function get_text($abbr=''){

        if (empty($this->request->params['requested']) || $abbr=='') {
            throw new ForbiddenException();
        }
        $text = ClassRegistry::init('UnityText')->findByAbbreviation($abbr);
        return $text;
    }

    function about(){
        $text = ClassRegistry::init('UnityText')->findByAbbreviation("HTUSE");

        $patron = '/(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?([\w\-]{10,12})(?:&feature=related)?(?:[\w\-]{0})?/';
        $sustitucion = '<div class="video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/$1"></iframe></div>';
        $text["UnityText"]["text"] = preg_replace($patron, $sustitucion, $text["UnityText"]["text"]);
        //vimeo
        $patron = '/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(\d+)/';
        $sustitucion = '<div class=" video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/$1"></iframe></div>';

        $text["UnityText"]["text"] = preg_replace($patron, $sustitucion, $text["UnityText"]["text"]);

        if(isset($this->request->params["ext"])){

            $this->set(array(
                'text' => $text,
                '_serialize' => array('text')
            ));
        }

        $this->set('text',$text["UnityText"]['text']);
    }
    function projects(){
        $text = ClassRegistry::init('UnityText')->findByAbbreviation("APPPROJECTS");

        $patron = '/(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?([\w\-]{10,12})(?:&feature=related)?(?:[\w\-]{0})?/';
        $sustitucion = '<div class="video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/$1"></iframe></div>';
        $text["UnityText"]["text"] = preg_replace($patron, $sustitucion, $text["UnityText"]["text"]);
        //vimeo
        $patron = '/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(\d+)/';
        $sustitucion = '<div class=" video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/$1"></iframe></div>';
        $text["UnityText"]["text"] = preg_replace($patron, $sustitucion, $text["UnityText"]["text"]);
        if(isset($this->request->params["ext"])){

            $this->set(array(
                'text' => $text,
                '_serialize' => array('text')
            ));
        }

        $this->set('text',$text["UnityText"]['text']);
    }
    function terms(){
        $text = ClassRegistry::init('UnityText')->findByAbbreviation("TERMS");

        $patron = '/(?:https?:\/\/)?(?:www\.)?(?:youtube\.com|youtu\.be)\/(?:watch\?v=)?([\w\-]{10,12})(?:&feature=related)?(?:[\w\-]{0})?/';
        $sustitucion = '<div class="video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://www.youtube.com/embed/$1"></iframe></div>';
        $text["UnityText"]["text"] = preg_replace($patron, $sustitucion, $text["UnityText"]["text"]);
        //vimeo
        $patron = '/(?:http?s?:\/\/)?(?:www\.)?(?:vimeo\.com)\/?(\d+)/';
        $sustitucion = '<div class=" video-container embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" src="https://player.vimeo.com/video/$1"></iframe></div>';
        $text["UnityText"]["text"] = preg_replace($patron, $sustitucion, $text["UnityText"]["text"]);
        if(isset($this->request->params["ext"])){

            $this->set(array(
                'text' => $text,
                '_serialize' => array('text')
            ));
        }

        $this->set('text',$text["UnityText"]['text']);

    }


    function beforeFilter(){
        parent::beforeFilter();
        if($this->action=="unity_zohar" || $this->action=="unity_pinchas" || $this->action=="unity_books" || $this->action=="unity_tikunim" || $this->action=="unity_psalms"){
            $this->Session->write("zohar_time",time()+ MINUTE * 5);
        }
    }
    function beforeRender(){
        $readers = 0;
        $readers += $this->UnitySession->getReaders();
        $sessions = $this->UnitySession->findById($this->Session->id());
        if(count($sessions) == 0){
            $readers++;
        }
        $this->set(compact('readers'));
    }
}
