<?php
    class MembershipsController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Membership.id' => 'DESC'
        )
    );
    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("Membership.id = '$search' OR
                            Membership.name like '%$search%'");
        }

        $rows = $this->Paginator->paginate('Membership',$conditions);
        $this->set(compact('rows'));

    }
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Membership->create();
            if ($this->Membership->save($this->request->data)) {
                $this->Session->setFlash(__('The Membership has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Membership could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }
    }
    public function admin_edit($id) {
        if ($this->request->is('put')) {
            $this->request->data["Membership"]['id'] = $id;
            if ($this->Membership->save($this->request->data)) {
                $this->Session->setFlash(__('The Membership has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Membership could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }elseif($this->request->is('get')){
            $this->request->data = $this->Membership->findById($id);
            $this->request->data["Membership"]['id'] = $id;
        }
    }
}