<?php
    class TranslationStringsController  extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'TranslationString.id' => 'ASC'
        )
    );

    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("TranslationString.id = '$search' OR
                                TranslationString.string like '%$search%'");
        }
        $rows = $this->Paginator->paginate('TranslationString',$conditions);
        $this->set(compact('rows'));
    }

    public function admin_add() {
        if ($this->request->is('post')) {
            $this->TranslationString->create();
            if ($this->TranslationString->save($this->request->data)) {
                $this->Session->setFlash(__('The string has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The string could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }
    }

    public function admin_edit($id){

    $this->TranslationString->id = $id;
    if ($this->request->is('get')) {
        $this->request->data = $this->TranslationString->read();
    } else {
        $this->request->data["TranslationString"]['id'] = $this->TranslationString->id;
        if ($this->TranslationString->save($this->request->data)) {
            $this->Session->setFlash(__("String saved!"),'default', array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }else{
            $this->Session->setFlash(__("String saved!"),'default', array('class' => 'alert alert-danger'));
        }
    }
    }

    public function api_get() {
        $books = $this->Bible->find_all($this->request->query);
        $this->set(array(
            'books' => $books,
            '_serialize' => array('books')
        ));
    }

}