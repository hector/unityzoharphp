<?php
class MembershipAgreementsController extends AppController {
    var $paginate = array(
        'limit' => 10,
        'order' => array(
            'MembershipAgreement.created' => 'DESC'
        )
    );

    public function admin_index() {





        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("MembershipAgreement.id = '$search' OR
                                MembershipAgreement.agreement_id LIKE '%$search%' OR
                                MembershipAgreement.user_id LIKE '%$search%' OR
                                User.name LIKE '%$search%' OR
                                User.lastname LIKE '%$search%' OR
                                User.email LIKE '%$search%'");
        }


        $rows = $this->Paginator->paginate('MembershipAgreement',$conditions);
        $memberships = $this->MembershipAgreement->User->Membership->find("list",["fields"=>['id','name']]);
        $this->set(compact("rows",'memberships','a'));
    }

    function admin_cancel_membership($membership_agreement_id){

        $ag = $this->MembershipAgreement->findById($membership_agreement_id);
        $this->MembershipAgreement->User->id = $ag["User"]["id"];
        $this->MembershipAgreement->User->saveField("membership_id",0,false);
        $this->MembershipAgreement->_suspend_membership($ag["User"]["id"]);
        $this->MembershipAgreement->cancel_membership_mail($ag["User"]["id"]);
            $this->Session->setFlash(
                __('Membership canceled'),
                'default',
                array('class' => 'alert alert-success'));
        return  $this->redirect($this->referer());

    }

    function admin_get_paypal_data($agreement_id){
        if($this->request->is('ajax')){
            $ag = $this->Paypal->get_agreement($agreement_id);
            $ag = $ag->toJSON(128);
            die($ag);
        }else{
            die("fuck off");
        }
    }

}//class