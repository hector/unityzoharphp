<?php
/**
 *@property User $User
 */
App::uses('AppController', 'Controller');

/**
 * Users content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UsersController extends AppController {

    public $paginate = null;

    function beforeFilter(){
        parent::beforeFilter();
        $this->paginate = array(
            'limit' => 25,
            'order' => array(
                Inflector::classify($this->name).'.id' => 'DESC'
            )
        );
    }
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-error'));
        }
    }

    function admin_edit($id = null) {
        $this->User->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->User->read();
        } else {
            $this->request->data["User"]['id'] = $this->User->id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__("User saved!"),'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            }
        }
        $memberships = $this->User->Membership->find("list",['order'=>'name ASC','fields' =>['id','name']]);
        $this->set(compact('memberships'));
    }
    function admin_edit_profile() {
        $this->User->id = $this->Auth->user('id');
        if ($this->request->is('get')) {
            $this->request->data = $this->User->read();
        } else {
            $this->request->data["User"]['id'] = $this->User->id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__("User saved!"),'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'admin_index','controller'=>'pages'));
            }
        }
    }

    function admin_change_password() {
        $this->User->id =$this->Auth->user('id');
        if ($this->request->is('get')) {
            $this->request->data = $this->User->read();
        } else {
            $this->request->data["User"]['id'] = $this->User->id;
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__("Password saved!"),'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'admin_index','controller'=>'pages'));
            }else{
                $this->request->data = $this->User->read();
            }
        }


    }
    function admin_delete($id = null) {

        if($this->User->delete($id)){
            $this->Session->setFlash(__('The user has been deleted'),
                'default',
                array('class' => 'alert alert-success'));
            $this->redirect(array('action' => 'index'));
        }else{
            $this->Session->setFlash(__('The user cannot be deleted'),
                'default',
                array('class' => 'alert alert-error'));
            $this->redirect(array('action' => 'index'));
        }
    }

    public function admin_dashboard() {

    }
    public function admin_index() {

        $search = $this->_search();

        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("User.id = '$search' OR
								User.name LIKE '%$search%' OR
								User.username LIKE '%$search%' OR
								User.lastname LIKE '%$search%'");

        }

       // $conditions += ['User.role' => ['admin','editor']];

        $this->User->recursive = 0;

        $rows = $this->Paginator->paginate('User',$conditions);
        $this->set(compact('rows'));
    }

    function admin_login(){
        $this->set('page_title','Unity Zohar::Admin');
        $this->layout= 'login';
        if ($this->request->is('post')) {
            $user = $this->User->findByUsername($this->request->data["User"]['username']);
            if(in_array($user['User']['role'],['admin','editor'])){
                if ($this->Auth->login()) {
                    return $this->redirect($this->Auth->redirect());
                }
            }
            $this->Session->setFlash(__("Invalid credentials"),'default', array('class' => 'alert alert-danger'));
        }
    }
    function admin_logout(){
        $this->Auth->logout();
        $this->Session->destroy();
        $this->redirect(array('controller'=>'users', 'action'=>'login','admin'=>true));
    }

/*-------------------------------------------------------Web users---------------------------------------------------------------------*/
    function registerapp(){
        if($this->Auth->login() && $this->Auth->User("role")=='userapp'){
            $this->redirect(Router::url("/",true));
        }
        $this->set('title_for_layout','Unity Zohar:: Users :: Registration');
        if ($this->request->is('post')) {
            $this->request->data["User"]['role'] ='userapp';
            $this->request->data["User"]['username'] =$this->request->data["User"]['email'];
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->User->registration_email($this->User->id);
                $this->Session->setFlash(__('Your account has been created, please, check you email for activation link and click on it to activate your account If you do not receive the confirmation message within a few minutes of signing up, please check your Bulk/Spam Mail folder just in case the confirmation email got delivered there instead of your inbox.'),
                    'default',
                    array('class' => 'alert alert-success'),"middle");
                if(isset($this->data["membercheck"]) && isset($this->data["membership"])){
                        $this->User->set("membership_id",$this->data["membership"]);
                        $this->User->save();
                }

                return  $this->redirect(Router::url("/",true));
            }
            $this->Session->setFlash(
                __('The user could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }

        $memberships = $this->User->Membership->find("all",['order'=>'cost ASC']);
        $this->set(compact('memberships'));
    }

    function membership(){
        if(!$this->Auth->login() || $this->Auth->user("role") != 'userapp'){
            $this->Session->setFlash(
                __('Must be logged to access this area'),
                'default',
                array('class' => 'alert alert-danger'));
            $this->redirect(Router::url("/users/login",true));
        }
        $memberships = $this->User->Membership->find("all",['order'=>'cost ASC']);
        $this->set(compact('memberships'));
    }

    function cancel_membership(){
        if(!$this->Auth->login() || $this->Auth->user("role") != 'userapp'){
            $this->Session->setFlash(
                __('Must be logged to access this area'),
                'default',
                array('class' => 'alert alert-danger'));
            $this->redirect(Router::url("/users/login",true));
        }
        $this->User->id = $this->Auth->user('id');
        $this->User->set("membership_id","0");
        if($this->User->save(null,false)){
            $this->Session->write("Auth.User.membership_id","0");
            $this->User->MembershipAgreement->_suspend_membership($this->Auth->user('id'));
            $this->User->MembershipAgreement->cancel_membership_mail($this->Auth->user("id"));
            $this->Session->setFlash(
                __('Membership canceled'),
                'default',
                array('class' => 'alert alert-success'));
            return  $this->redirect(Router::url("/",true).'users/appaccount');
        }else{
            $this->Session->setFlash(
                __('The membership could not be canceled. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }



    }
    public function login() {
        if ($this->request->is('post')) {
            $this->User->recursive = -1;
            $user = $this->User->findByEmail($this->request->data["User"]['username']);
            if(empty($user)){
                $this->Session->setFlash(__("Invalid credentials"),'default', array('class' => 'alert alert-danger'));
                return $this->redirect(Router::url("/users/login",true));
            }
            if($user['User']['role']=='userapp' && $user['User']['active']=='1'){
                if ($this->Auth->login()) {
                    if($this->Session->check("redirect")){
                        $r = $this->Session->read("redirect");
                        $this->Session->delete("redirect");
                        return $this->redirect($r);
                    }
                    return $this->redirect(Router::url("/users/appaccount",true));
                }
            }
            $this->Session->setFlash(__("Invalid credentials"),'default', array('class' => 'alert alert-danger'));
        }
        $this->set("page_title",'Unity Zohar - Users');
        //$this->render(null,'login');
    }
    public function logout() {
        $this->Auth->logout();
        $this->Session->destroy();
        $this->redirect(Router::url("/",true));
    }

    function appaccount(){
        if(isset($this->request->query["invoice"])){
            $id = $this->request->query["invoice"];
            $u = $this->User->findByInvoice($id);
            if(!empty($u)){
                $p = $this->User->Payment->findByInvoice($id);
                if(!empty($p)){
                    if($p["Payment"]["state"]=='Completed') {
                        $this->Session->write("Auth.User.membership_active", "1");
                        $this->Session->write("Auth.User.membership_id", $u["User"]["membership_id"]);
                        $this->Session->setFlash(__("Your membership has been updated"),
                            'default',
                            array('class' => 'alert alert-success'), 'middle');
                        return $this->redirect(Router::url('/users/appaccount', true));
                    }else{
                        $this->Session->setFlash(__("Your membership cannot be updated, the PayPal response wasn't successful"),
                            'default',
                            array('class' => 'alert alert-danger'), 'middle');
                        return $this->redirect(Router::url('/users/appaccount', true));
                    }
                }
                $this->Session->setFlash(__("<i class='fa fa-spinner fa-spin fa-2x'></i> Please wait, Your membership will be modified once your payment is confirmed"),
                            'default',
                            array('class' => 'alert alert-success'),'middle');
            }
        }

        if($this->Session->read('Auth.User.role')!='userapp'){
            $this->redirect(Router::url('/users/login',true));
        }

        $id = $this->Session->read('Auth.User.id');
        $this->set('title_for_layout','Users - account');
        //$this->layout = 'api';
        $this->User->id = $id;
        $user = $this->User->findById($id);
        if ($this->request->is('get') && !empty($user) && $user["User"]["role"]=='userapp') {
            $this->request->data = $user;
        } elseif(!empty($user) && $user["User"]["role"]=='userapp' && $this->request->is('put')) {
            $this->request->data["User"]['id'] = $this->User->id;
            if ($this->User->save($this->request->data,false)) {
                $this->Session->setFlash(__("User saved!"),'default', array('class' => 'alert alert-success'));
                //return  $this->redirect(Router::url("/",true).'users/appaccount');
            }
        }

        $memberships = $this->User->Membership->find("list",['order'=>'cost ASC']);
        $this->set(compact('memberships'));
    }


    function update_profile()
    {
        if ($this->Session->read('Auth.User.role') != 'userapp') {
            $this->redirect(Router::url('/users/login', true));
        }
        $id = $this->Session->read('Auth.User.id');
        $this->set('title_for_layout', 'Users - profile');
        $this->User->id = $id;
        $user = $this->User->findById($id);
        if ($this->request->is('get') && !empty($user) && $user["User"]["role"] == 'userapp') {
            $this->request->data = $user;
        } elseif (!empty($user) && $user["User"]["role"] == 'userapp' && $this->request->is('put')) {
            $this->request->data["User"]['id'] = $this->User->id;
            if ($this->User->save($this->request->data, false)) {
                $this->Session->setFlash(__("Profile updated!"), 'default', array('class' => 'alert alert-success'));
                return  $this->redirect(Router::url("/",true).'users/appaccount');
            }
        }

        $memberships = $this->User->Membership->find("list", ['order' => 'cost ASC']);
        $this->set(compact('memberships'));

    }
        function verify(){
        if(isset($this->request->query["hash"]) && isset($this->request->query["email"])){
            $user = $this->User->findByEmail($this->request->query["email"]);
            $hash = md5($this->request->query["email"].$user["User"]["id"]);
            if(strcmp($hash,$this->request->query["hash"]) === 0){
                $this->User->id = $user["User"]["id"];
                $this->User->saveField('active',1);
                $this->Session->setFlash(__('Your account is now active, you may now log in'),
                    'default',
                    array('class' => 'alert alert-success'));
                return  $this->redirect(Router::url("/",true).'users/login');
            }
        }
        $this->Session->setFlash(__('Something went wrong'),
            'default',
            array('class' => 'alert alert-danger'));
        return  $this->redirect(Router::url("/",true));
    }
    function payments(){
        if($this->Session->read('Auth.User.role')!='userapp'){
            $this->redirect(Router::url('/users/login',true));
        }
        $payments = $this->User->Payment->find("all",array('conditions'=>array('Payment.user_id'=>$this->Auth->user("id"))));
        $this->set(compact('payments'));
    }

    function validate_user(){
        $this->User->set($this->request->data);
        if ($this->User->validates()) {
            die(json_encode(['result'=>1]));
        } else {
            $errors = $this->User->validationErrors;
            die(json_encode(['result'=>$errors]));
        }
    }


    public function forgot_password() {

        if($this->request->is("post") && !empty($this->request->data["User"]["email"])){
            $user = $this->User->findByEmail($this->request->data["User"]["email"]);
            if(empty($user)){
                $this->Session->setFlash(__('The user email doesn\'t exists'),
                    'default',
                    array('class' => 'alert alert-danger'));
            }else{
                $this->User->forgot_password_email($user["User"]["id"]);
                $this->Session->setFlash(__('A resetting password email has been send'),
                    'default',
                    array('class' => 'alert alert-success'));
                return  $this->redirect(Router::url("/",true));
            }
        }elseif( !empty($this->request->data["User"]["email"])){
            $this->Session->setFlash(__('The user email doesn\'t exists'),
                'default',
                array('class' => 'alert alert-danger'));
        }
    }

    function verify_hash(){
        //$this->log(print_r($this->request->data["email"],true),'debug');
        $response = array("");
        $this->User->recursive = 0;
        if(isset($this->request->query["hash"]) && isset($this->request->query["email"])){
            $user = $this->User->findByEmail($this->request->query["email"]);
            //$this->log(print_r($this->request->data["email"],true),'debug');
            if(empty($user) || $user["User"]["active"]==0){
                $this->Session->setFlash(__('Invalid user'),
                    'default',
                    array('class' => 'alert alert-danger'));
                return  $this->redirect(Router::url("/",true));
            }else{
                if($user['User']['role']=='userapp'){
                    $hash = md5($user["User"]["id"].$user["User"]["email"]."password-verification");
                    if(strcmp($hash,$this->request->query["hash"]) === 0){
                        $this->Session->write("resetting",true);
                        $this->Session->write("resetting_email",$user["User"]["email"]);
                        return  $this->redirect(Router::url("/users/reset_password",true));
                    }else{
                        $this->Session->setFlash(__('Invalid email data'),
                            'default',
                            array('class' => 'alert alert-danger'));
                        return  $this->redirect(Router::url("/",true));
                    }
                }else{
                    $this->Session->setFlash(__('Invalid user role'),
                        'default',
                        array('class' => 'alert alert-danger'));
                    return  $this->redirect(Router::url("/",true));
                }
            }
        }
        $this->set(array(
            'response' => $response,
            '_serialize' => array('response')
        ));
    }

    function reset_password(){

        if($this->Auth->login()){
            return  $this->redirect(Router::url("/users/appaccount",true));
        }

        if(!$this->Session->read("resetting") || !$this->Session->read("resetting_email")){
            $this->Session->setFlash(__('Invalid location'),
                'default',
                array('class' => 'alert alert-danger'));
            return  $this->redirect(Router::url("/",true));
        }

        if($this->request->is('post')){
            $this->User->recursive = 0;
            $user = $this->User->findByEmail($this->Session->read("resetting_email"));
            if(empty($user) || $user["User"]["active"]==0){
                $this->Session->setFlash(__('Invalid user'),
                    'default',
                    array('class' => 'alert alert-danger'));
                return  $this->redirect(Router::url("/",true));
            }else{
                $this->User->set($this->request->data);
                if($this->User->validates(array('fieldList' => array('password')))){
                    if(strcmp(Security::hash($this->request->data["User"]["password"]),Security::hash($this->request->data["User"]["password2"]))!=0){
                        $this->Session->setFlash(__('Passwords do not match'),
                            'default',
                            array('class' => 'alert alert-danger'));
                        return  $this->redirect($this->request->referer());
                    }else{
                        $this->User->id = $user["User"]["id"];
                        if($this->User->saveField('password',Security::hash($this->request->data["User"]["password"], NULL, true),array('callbacks'=>false))){
                            $this->Session->setFlash(__('Password has been reseated'),
                                'default',
                                array('class' => 'alert alert-success'));
                            return  $this->redirect(Router::url("/",true));
                        }
                    }
                }
            }

        }
    }

    public function admin_detail($id) {
        $this->User->recursive = 1;
        $user = $this->User->findByid($id);
        $memberships = $this->User->Membership->find("list");
        $this->set(compact('user','memberships'));
    }

    public function admin_deactivate_user($id) {
        $this->User->recursive = 0;
        $dev = $this->User->findById($id);
        $this->User->id = $dev["User"]["id"];
        if($dev["User"]['active'] == 1){
            $this->User->saveField('active',0);
            $this->Session->setFlash(__('The developer/user has been deactivate'),
                'default',
                array('class' => 'alert alert-success'));

        }else{
            $this->User->saveField('active',1);
            $this->Session->setFlash(__('The developer/user has been activate'),
                'default',
                array('class' => 'alert alert-success'));
        }

        return  $this->redirect($this->request->referer());

    }

    public function admin_update_membership($user_id,$id) {
        $this->User->id = $user_id;
        $this->User->saveField("membership_active","1");
        if($this->User->saveField("membership_id",$id)){
            $this->Session->setFlash(__('Membership updated'),
                'default',
                array('class' => 'alert alert-success'));

        }else{
            $this->Session->setFlash(__('Membership cannot be updated'),
                'default',
                array('class' => 'alert alert-danger'));
        }

        return  $this->redirect($this->request->referer());
    }

    function create_membership_agreement(){

        if(!$this->Auth->login() || $this->Auth->user("role") != 'userapp'){
            $this->Session->setFlash(
                __('Must be logged to access this area'),
                'default',
                array('class' => 'alert alert-danger'));
            $this->redirect(Router::url("/users/login",true));
        }

        $result = ["created" => 0];
        $user = $this->Auth->user();


        if($this->request->is("post") && $this->request->is("ajax") && $this->request->params["ext"] == 'json'){
            $data["description"] = $this->request->data["cc"]["text"];
            $data["amount"] = $this->request->data["cc"]["amount"];//"25";
            $data["email"] = $this->Auth->user("email");
            $data["first_name"] = $user["name"];
            $data["last_name"] = $user["lastname"];
            //$data["cc"]["type"] = $this->request->data["cc"]["type"];
            $data["cc"]["number"] = $this->request->data["cc"]["number"];//"4032033980539501";
            $data["cc"]["expire_month"] = $this->request->data["cc"]["expire_month"];//"08";
            $data["cc"]["expire_year"] = $this->request->data["cc"]["expire_year"];//"2019";
            $data["cc"]["cvv2"] = $this->request->data["cc"]["cvv"];
            $data["cc"]["card_name"] = $this->request->data["cc"]["card_name"];



            $agreement = $this->Paypal->create_recurring_payment($data);
            if($agreement){
                $ag = [
                    'MembershipAgreement' =>[
                        'user_id' => $user["id"],
                        'plan_id' => "0",
                        'agreement_id' => $agreement["PROFILEID"],
                        'paypal_state' => $agreement["RESPMSG"],
                        'start_date' => date("Y-m-d H:i:s",strtotime("+1 days")),
                        'value' => $data["amount"],
                        'membership_id' => $this->request->data["User"]["membership_id"],
                    ]
                ];

                if($this->User->MembershipAgreement->save($ag)){
                    $result = ["created" => 1,'payment_id' =>$this->User->MembershipAgreement->id];
                    $this->User->id = $user["id"];
                    $this->User->saveField("membership_id",$this->request->data["User"]["membership_id"],false);
                    $this->Session->write("Auth.User.membership_id",$this->request->data["User"]["membership_id"]);
                    $this->User->MembershipAgreement->received_email($this->Auth->user("id"),$this->User->MembershipAgreement->id);
                    $this->User->MembershipAgreement->_suspend_previous_membership($agreement["PROFILEID"]);
                }else{
                    $result = ["created" => 0,'error' =>"The Payment agreement has been created but there's a failure trying to
                    create / update your membership, please contact the administration"];
                }

            }else{
                $result = ["created" => 0,'error' => "There's a problem with your credit card info, please correct it"];
            }


            //$agreement2 = $this->Paypal->get_agreement($agreement->getId());
            //$agreement3 = $this->Paypal->suspend_agreement($agreement);
        }


        $this->set(array(
            'result' => $result,
            '_serialize' => array('result')
        ));
    }

    function api_activate_stats(){
        $result = ["activated" => 0];
        if(!isset($this->request->data["email"])){
            $this->set(array(
                'result' => $result,
                '_serialize' => array('result')
            ));
        }else{
            $email = $this->request->data["email"];
            //$email = "hectormotesinos@gmail.com";
            $this->Token = ClassRegistry::init("Token");
            $token = $this->Token->get($email);
            if($token){
                $result = ["token" => $token,'activated' => "1"];
            }

            $this->set(array(
                'result' => $result,
                '_serialize' => array('result')
            ));
        }
    }

}//class