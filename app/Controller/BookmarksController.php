<?php
    class BookmarksController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Bookmark.id' => 'ASC'
        )
    );

    public function api_save(){
        $bookmarks = [];
        if (isset($this->request->query["_access_token"])) {
            $access_token = $this->request->query["_access_token"];
            $this->Token = ClassRegistry::init("Token");
            $email = $this->Token->field(
                'email',
                array('Token.token' => $access_token)


            );
            $tokens = $this->Token->find('list', [
                'conditions' => ['Token.email' => $email],
                'fields' => 'Token.token'
            ]);


            $data = $this->request->data;
            $exists = $this->Bookmark->find('first',[
               'conditions' => [
                   'Bookmark.zohar_id' => $data['zohar_id'],
                   'Bookmark.token' => $tokens,
               ]
            ]);
            $dataSave = [
                'Bookmark' => [
                    'annotation' => $data['annotation'],
                    'token' => $access_token,
                    'zohar_id' => $data['zohar_id'],
                    'parasha_id' => $data['parasha_id']
                ]
            ];

            if(!empty($exists)) {
                $dataSave['Bookmark']['id'] = $exists['Bookmark']['id'];
            }
            $bookmarks= $this->Bookmark->save($dataSave);
            Cache::delete('bookmarks_'.$access_token, 'minute15');
        }
        $this->set(array(
            'bookmarks' => $bookmarks,
            '_serialize' => array('bookmarks')
        ));
    }

    public function api_get() {

        if(!empty($this->request->query["_access_token"])){
            $access_token = $this->request->query["_access_token"];
            $bookmarks = Cache::read('bookmarks_'.$access_token, 'minute15');
            $this->Token = ClassRegistry::init("Token");
            $email = $this->Token->field(
                'email',
                array('Token.token' => $access_token)


            );
            $tokens = $this->Token->find('list', [
                'conditions' => ['Token.email' => $email],
                'fields' => 'Token.token'
            ]);
            if (!$bookmarks) {
                $bookmarks = $this->Bookmark->find("all",
                    ['conditions' => [
                        'Bookmark.token' => $tokens
                    ]
                    ]);
                Cache::write('bookmarks', $bookmarks, 'minute15');

                $returned_bookmarks = [];
                foreach($bookmarks as $bookmark){
                    unset($bookmark["Bookmark"]['token']);
                    unset($bookmark["Bookmark"]['created']);
                    $bookmark["Bookmark"]["parasha"] = $bookmark["Parasha"]["name"];
                    $bookmark["Bookmark"]["parasha_hebrew"] = $bookmark["Parasha"]["name_hebrew"];
                    $bookmark["Bookmark"]["par_num"] = $bookmark["Zohar"]["paragraph_num"];
                    $bookmark["Bookmark"]["parasha_hebrew"] = $bookmark["Parasha"]["name_hebrew"];
                    $returned_bookmarks[] = $bookmark["Bookmark"];
                }
                $bookmarks = $returned_bookmarks;
            }
        }else{
            $bookmarks = [];
        }


        $this->set(array(
            'bookmarks' => $bookmarks,
            '_serialize' => array('bookmarks')
        ));
    }

        public function api_delete() {
            if(!empty($this->request->query["_access_token"])){
                $access_token = $this->request->query["_access_token"];
                $this->Token = ClassRegistry::init("Token");
                $email = $this->Token->field(
                    'email',
                    array('Token.token' => $access_token)


                );
                $tokens = $this->Token->find('list', [
                    'conditions' => ['Token.email' => $email],
                    'fields' => 'Token.token'
                ]);

                $this->Bookmark->deleteAll([
                        'Bookmark.token' => $tokens,
                        'Bookmark.zohar_id' => $this->request->data['zohar_id'],

                    ]
                );
                $bookmarks = ['deleted'];

            }else{
                $bookmarks = [];
            }


            $this->set(array(
                'bookmarks' => $bookmarks,
                '_serialize' => array('bookmarks')
            ));
        }

}