<?php
    class ParashotController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Parasha.id' => 'ASC'
        )
    );
    public $uses = array("Parasha");
    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("Parasha.id = '$search' OR
                            Parasha.name like '%$search%'");
        }

        $rows = $this->Paginator->paginate('Parasha',$conditions);
        $this->set(compact('rows'));

    }
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Parasha->create();
            if ($this->Parasha->save($this->request->data)) {
                $this->Session->setFlash(__('The Parasha has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Parasha could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }
    }
    public function admin_edit($id) {
        if ($this->request->is('put')) {
            $this->request->data["Parasha"]['id'] = $id;
            if ($this->Parasha->save($this->request->data)) {
                $this->Session->setFlash(__('The Parasha has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Parasha could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }elseif($this->request->is('get')){
            $this->request->data = $this->Parasha->findById($id);
            $this->request->data["Parasha"]['id'] = $id;
        }
    }
    public function admin_order() {
        $rows = $this->Parasha->find("all",[
                'conditions' => ['Parasha.active' => 1],
                'order'=>"Parasha.order ASC",
            ]
        );
        $this->set(compact('rows'));
    }
    function admin_save_order(){
        $this->autoRender = false;
        $this->response->type('json');
        $this->layout = 'ajax';
        if($this->request->is('post')){
            $order = 0;
            foreach($this->request->data as $faq){
                $data['Parasha']['id'] = $faq['id'];
                $data['Parasha']['order'] = ++$order;
                if($this->Parasha->save($data)){
                    $result = 'saved';
                }else{
                    $result = 'fail';
                }
            }
        }else{
            $result = 'only XHR for this method';
        }
        //$log = $this->Faq->getDataSource()->getLog(false, false);
        //pr($log);
        $json = json_encode(array('result'=>$result));
        $this->response->body($json);
    }


    public function api_get_all() {

        $parashot = $this->Parasha->find("all",[
            'conditions' => ['Parasha.active' => 1],
            'order' => 'Parasha.order ASC'
            ]);

        $this->set(array(
            'parashot' => $parashot,
            '_serialize' => array('parashot')
        ));
    }

    public function api_get($id) {

        $parashot = $this->Parasha->get_one($id);
        $paragraphs = $this->Parasha->Zohar->get_paragraphs(['Parasha.id' => $id]);
        $parashot["paragraphs"] = $paragraphs;
        $this->set(array(
            'parashot' => $parashot,
            '_serialize' => array('parashot')
        ));
    }

}