<?php
    class LanguagesController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Language.language' => 'ASC'
        )
    );
    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("Language.id = '$search' OR
                            Language.language like '%$search%'");
        }

        $rows = $this->Paginator->paginate('Language',$conditions);
        $this->set(compact('rows'));

    }
    public function admin_add() {
        if ($this->request->is('post')) {
            $this->Language->create();
            if ($this->Language->save($this->request->data)) {
                $this->Session->setFlash(__('The Language has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Language could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }
    }
    public function admin_edit($id) {
        if ($this->request->is('put')) {
            $this->request->data["Language"]['id'] = $id;
            if ($this->Language->save($this->request->data)) {
                $this->Session->setFlash(__('The Language has been saved'),
                    'default',
                    array('class' => 'alert alert-success'));
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->setFlash(
                __('The Language could not be saved. Please, try again.'),
                'default',
                array('class' => 'alert alert-danger'));
        }elseif($this->request->is('get')){
            $this->request->data = $this->Language->findById($id);
            $this->request->data["Language"]['id'] = $id;
        }
    }

    public function api_get() {
        $languages = $this->Language->find("all",
            [
                'conditions' => [
                    'Language.apply_app' => true
                ],
                'fields' => [
                    'abbreviation','language'
                ]
            ]
        );
        $this->set(array(
            'languages' => $languages,
            '_serialize' => array('languages')
        ));
    }
}