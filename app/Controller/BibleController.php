<?php
    class BibleController extends AppController {

    var $paginate = array(
        'limit' => 25,
        'order' => array(
            'Bible.id' => 'ASC'
        )
    );

    public function admin_index() {
        $search = $this->_search();
        $this->Paginator->settings = $this->paginate;
        $conditions = array();
        if(!empty($search)){
            $conditions = array("Bible.id = '$search' OR
                                BibleBook.name like '%$search%' OR
                                Bible.verse_text like '%$search%'

            ");
        }
        $rows = $this->Paginator->paginate('Bible',$conditions);
        $this->set(compact('rows'));
    }

    public function admin_edit($id){

        $this->Bible->id = $id;
        if ($this->request->is('get')) {
            $this->request->data = $this->Bible->read();
        } else {
            $this->request->data["Bible"]['id'] = $this->Bible->id;
            if ($this->Bible->save($this->request->data)) {
                $this->Session->setFlash(__("Bible saved!"),'default', array('class' => 'alert alert-success'));
                $this->redirect(array('action' => 'index'));
            }else{
                $this->Session->setFlash(__("Bible saved!"),'default', array('class' => 'alert alert-danger'));
            }
        }


        $books = $this->Bible->BibleBook->find("list",array('fields'=>array('id','name'),'order'=>'BibleBook.name ASC'));
        $this->set(compact('books'));
    }

    public function api_get() {
        $books = $this->Bible->find_all($this->request->query);
        $this->set(array(
            'books' => $books,
            '_serialize' => array('books')
        ));
    }

}