var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
//var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var uglify = require('gulp-uglify');
var csscomb = require('gulp-csscomb');
//var cdnify = require('gulp-cdnify');

gulp.task('min-styles', function () {
    gulp.src('./app/webroot/css/web/default.css')
        .pipe(minifyCss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./app/webroot/css/web/'));
});
//
//
//gulp.task('compress-js', function() {
//    gulp.src('./app/webroot/js/admin/admin.js')
//        .pipe(uglify())
//        .pipe(rename({suffix: '.min'}))
//        .pipe(gulp.dest('./app/webroot/js/admin/'))
//});
//
//gulp.task('comb', function () {
//    return gulp.src('./app/webroot/css/admin/default.css')
//        .pipe(csscomb())
//        .pipe(gulp.dest('./app/webroot/css/admin/'));
//});
gulp.task('compress-js', function() {
    gulp.src('./app/webroot/js/web/candles.js')
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./app/webroot/js/web/'))
});

gulp.task('membership-js', function() {
    gulp.src('./app/webroot/js/web/membership.js')
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./app/webroot/js/web/'))
});

gulp.task('compress-js-default', function() {
    gulp.src('./app/webroot/js/web/default.js')
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./app/webroot/js/web/'))
});

//gulp.task('cdn', function() {
//    gulp.src('./app/View/Layouts/login.ctp')
//        .pipe(cdnify(
//            {base: "http://pathto/your/cdn/"}
//        ))
//        .pipe(rename({suffix: '.new'}))
//        .pipe(gulp.dest('./app/View/Layouts/login2.ctp'))
//});

/*
*
* SELECT id, cycle, '-' as unity_session_id,`ipaddress` as ip_address, parnum as zohar_id, 1 as parasha_id, 1 as paragraph,`lng`,`lat`,`country`,1 as country_iso, `state`,`city`,`readingtime` as created, `readingtime` as modified FROM `ZoharCycles`
*
* */